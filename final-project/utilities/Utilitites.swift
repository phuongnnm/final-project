//
//  Utilitites.swift
//  final-project
//
//  Created by Phuong Nguyen on 28/09/2021.
//

import Foundation
import UIKit

class Utilitites {
    static func displayNoActionAlert(title: String, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        viewController.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    static func formatPrice(price: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: price as NSNumber) {
            if let dIndex = formattedTipAmount.lastIndex(of: ".") {
                let startIndex = formattedTipAmount.startIndex
                var a = String(formattedTipAmount[startIndex..<dIndex])
                a.removeFirst()
                return a
            }
        }
        return String(price)
    }
    
    static func formateDateFromString(dateString: String) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy, h:mm a"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
        guard let datee = dateFormatterGet.date(from: dateString) else { return "nil" }
        return  dateFormatterPrint.string(from: datee)
    }
    
    static func generateCode() -> String {
        let letters = "0123ABCDEF45678GHIJKLMNOPQRSTUVWXYZ9"
        let random = String((0..<4).map{ _ in letters.randomElement()! })
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMddssm"
        let b = formatter.string(from: date)
        return (b + random).uppercased()
    }
}
