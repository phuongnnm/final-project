//
//  ServiceCreateViewController.swift
//  final-project
//
//  Created by phuong nguyen on 7/18/21.
//

import UIKit
import Apollo
import RxCocoa
import RxSwift
import AWSS3
import PromiseKit

class ServiceCreateViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, DeliverableCellDeletgate {
    let rxbag = DisposeBag()
    var isCreateNew = true {
        didSet {
            if isCreateNew == false {
                saveBtn.isHidden = false
            } else {
                saveBtn.isHidden = true
            }
        }
    }
    let progressLayerView = Bundle.main.loadNibNamed("ProgressLayerView", owner: self, options: nil)?.first as! ProgressLayerView
    let saveBtn: UIButton = {
        let btn = UIButton(frame: CGRect.zero)
        btn.setTitle("Save", for: .normal)
        btn.layer.cornerRadius = 12.0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .orange
        btn.setTitleColor(.white, for: .normal)
        return btn
    } ()
    
    @IBOutlet weak var serviceDescriptionTf: UITextField!
    var productImgTableHeightAnchor: NSLayoutConstraint!
    let tapStandard = UITapGestureRecognizer()
    let tapPremium = UITapGestureRecognizer()
    var finishButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .orange
        button.setTitle("Finish", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    } ()
    var servicePackages: [ServicePackage] = []
    var serviceInput = ServiceInput()
    
    var items: [UIImage] = [] {
        didSet {
            productImgTableView.reloadData()
            if items.count > 0 {
                productImgTableHeightAnchor.constant = 280
            } else {
                productImgTableHeightAnchor.constant = 0
            }
            UIView.animate(withDuration: 0.5) {
                self.contentView.layoutIfNeeded()
            }
        }
    }
    
    var categories: [JobCategory] = [] {
        didSet {
            categoryCollectionView.reloadData()
            updateCollectionViewHeight()
        }
    }
    
    var deliverables1: [String] = [] {
        didSet {
            packageInfoView1.deliverTableView.reloadData()
            deliverTable1HeightAnchor.constant = CGFloat(30 * deliverables1.count)
            packageInfoView1.layoutIfNeeded()
        }
    }
    
    var deliverables2: [String] = [] {
        didSet{
            packageInfoView2.deliverTableView.reloadData()
            deliverTable2HeightAnchor.constant = CGFloat(30 * deliverables2.count)
            package2HeightAnchor.constant = 900 + deliverTable2HeightAnchor.constant
            packageInfoView2.layoutIfNeeded()
        }
    }
    
    var deliverables3: [String] = [] {
        didSet{
            packageInfoView3.deliverTableView.reloadData()
            deliverTable3HeightAnchor.constant = CGFloat(30 * deliverables3.count)
            package3HeightAnchor.constant = 900 + deliverTable3HeightAnchor.constant
            packageInfoView3.layoutIfNeeded()
        }
    }
    
    @IBOutlet weak var productImgTableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var pageScrollView: UIScrollView!
    @IBOutlet weak var categoryCollectionView:
        UICollectionView!
    
    let servicePackageXib = Bundle.main.loadNibNamed("ServicePackageView", owner: self, options: nil)?.first as! ServicePackageView
    let packageInfoView1 = Bundle.main.loadNibNamed("PackageInfoView", owner: self, options: nil)?.first as! PackageInfoView
    let packageInfoView2 = Bundle.main.loadNibNamed("PackageInfoView", owner: self, options: nil)?.first as! PackageInfoView
    let packageInfoView3 = Bundle.main.loadNibNamed("PackageInfoView", owner: self, options: nil)?.first as! PackageInfoView
    
    var package2HeightAnchor: NSLayoutConstraint!
    var package3HeightAnchor: NSLayoutConstraint!
    var deliverTable1HeightAnchor: NSLayoutConstraint!
    var deliverTable2HeightAnchor: NSLayoutConstraint!
    var deliverTable3HeightAnchor: NSLayoutConstraint!
    var contentViewHeightAnChor: NSLayoutConstraint!
    var collectionViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        productImgTableView.translatesAutoresizingMaskIntoConstraints = false
        productImgTableHeightAnchor = productImgTableView.heightAnchor.constraint(equalToConstant: 0)
        productImgTableHeightAnchor.isActive = true
        
        packageInfoView2.setDelegateDataSourceDeleverTableView(delegate: self, dataSource: self)
        
        packageInfoView1.setDelegateDataSourceDeleverTableView(delegate: self, dataSource: self)
        
        packageInfoView3.setDelegateDataSourceDeleverTableView(delegate: self, dataSource: self)
        
        packageInfoView1.deliverTf.delegate = self
        packageInfoView2.deliverTf.delegate = self
        packageInfoView3.deliverTf.delegate = self
        
        productImgTableView.delegate = self
        productImgTableView.dataSource = self
        productImgTableView.register(UINib(nibName: "PortfolioImageTableViewCell", bundle: nil), forCellReuseIdentifier: "PortfolioImageTableViewCell")
        view.backgroundColor =  .white
        categoryCollectionView.register(UINib(nibName: "PlacesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlacesCollectionViewCell")
        let layout = categoryCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let width = (contentView.frame.size.width / 2) - 40
        
        print(width)
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 5
        layout.itemSize = CGSize(width: width, height: 50)
        
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        
        collectionViewHeight = categoryCollectionView.heightAnchor.constraint(equalToConstant: 400)
        collectionViewHeight.isActive = true
        
        servicePackageXib.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(servicePackageXib)
        NSLayoutConstraint.activate([
            servicePackageXib.topAnchor.constraint(equalTo: productImgTableView.bottomAnchor, constant: 70),
            //HERE
            servicePackageXib.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -100),
            servicePackageXib.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            servicePackageXib.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
        saveBtn.addTarget(self, action: #selector(saveBtnTap), for: .touchUpInside)
        contentView.addSubview(saveBtn)
        NSLayoutConstraint.activate([
            saveBtn.topAnchor.constraint(equalTo: servicePackageXib.bottomAnchor, constant: 25),
            saveBtn.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
            saveBtn.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            saveBtn.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        
        if isCreateNew == false {
            saveBtn.isHidden = false
        } else {
            saveBtn.isHidden = true
        }
        
        addPackageInfoView()
        
        generateViewGestures()
        
        getAllBasicJobCategory()
        
        addProgressLayerView()
        
    }
    
    func getAllBasicJobCategory() {
        Network.shared.apollo.fetch(query: JobCategoryBasicQuery()) {
            res in
            switch (res) {
            case .success(let response):
                if let datas = response.data?.allJobCategories {
                    
                    self.categories = datas.compactMap { element in
                        let id = element!.resultMap["id"] as? String
                        let name = element!.resultMap["categoryName"] as? String
                        let desc = element!.resultMap["categoryDescription"] as? String
                        return JobCategory(id: id, categoryName: name, categoryDescription: desc)
                    }
                }
            case .failure(let err):
                print("Fetch loi ne ma \(err)")
            }
        }
    }
    
    func addProgressLayerView() {
        view.addSubview(progressLayerView)
        view.bringSubviewToFront(progressLayerView)
        progressLayerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            progressLayerView.topAnchor.constraint(equalTo: view.topAnchor),
            progressLayerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            progressLayerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            progressLayerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        ])
        self.progressLayerView.isHidden = true
    }
    func addPackageInfoView() {
        packageInfoView1.translatesAutoresizingMaskIntoConstraints = false
        
        servicePackageXib.addSubview(packageInfoView1)
        
        packageInfoView2.translatesAutoresizingMaskIntoConstraints = false
        
        servicePackageXib.addSubview(packageInfoView2)
        
        packageInfoView3.translatesAutoresizingMaskIntoConstraints = false
        
        servicePackageXib.addSubview(packageInfoView3)
        
        NSLayoutConstraint.activate([
            packageInfoView1.trailingAnchor.constraint(equalTo: servicePackageXib.trailingAnchor, constant: -5),
            packageInfoView1.leadingAnchor.constraint(equalTo: servicePackageXib.leadingAnchor, constant: 5),
            packageInfoView1.topAnchor.constraint(equalTo: servicePackageXib.regularCheckBox.bottomAnchor, constant: 20),
        ])
        deliverTable1HeightAnchor = packageInfoView1.deliverTableView.heightAnchor.constraint(equalToConstant: 0)
        deliverTable1HeightAnchor.isActive = true
        
        NSLayoutConstraint.activate([
            packageInfoView1.deliverTableView.leadingAnchor.constraint(equalTo: packageInfoView1.leadingAnchor, constant: 15),
            packageInfoView1.deliverTableView.trailingAnchor.constraint(equalTo: packageInfoView1.trailingAnchor, constant: -15),
            packageInfoView1.deliverTableView.topAnchor.constraint(equalTo: packageInfoView1.desc1Lbl.bottomAnchor, constant: 10),
            
            packageInfoView1.deliverTf.topAnchor.constraint(equalTo: packageInfoView1.deliverTableView.bottomAnchor, constant: 15),
            packageInfoView1.deliverTf.heightAnchor.constraint(equalToConstant: 36),
            packageInfoView1.deliverTf.leadingAnchor.constraint(equalTo: packageInfoView1.deliverTableView.leadingAnchor),
            packageInfoView1.deliverTf.trailingAnchor.constraint(equalTo: packageInfoView1.deliverTableView.trailingAnchor),
            //            contentView.bottomAnchor.constraint(equalTo: packageInfoView2.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            servicePackageXib.standardView.leadingAnchor.constraint(equalTo: servicePackageXib.leadingAnchor, constant: 15),
            servicePackageXib.standardView.topAnchor.constraint(equalTo: packageInfoView1.bottomAnchor, constant: 15),
            servicePackageXib.standardView.heightAnchor.constraint(equalToConstant: servicePackageXib.standardView.frame.height),
            servicePackageXib.standardView.trailingAnchor.constraint(equalTo: servicePackageXib.trailingAnchor),
        ])
        //MARK: VIEW2
        NSLayoutConstraint.activate([
            packageInfoView2.trailingAnchor.constraint(equalTo: servicePackageXib.trailingAnchor, constant: -5),
            packageInfoView2.leadingAnchor.constraint(equalTo: servicePackageXib.leadingAnchor, constant: 5),
            packageInfoView2.topAnchor.constraint(equalTo: servicePackageXib.standardView.bottomAnchor, constant: 20),
        ])
        package2HeightAnchor = packageInfoView2.heightAnchor.constraint(equalToConstant: 0)
        package2HeightAnchor.isActive = true
        
        packageInfoView2.isHidden = true
        deliverTable2HeightAnchor = packageInfoView2.deliverTableView.heightAnchor.constraint(equalToConstant: 0)
        deliverTable2HeightAnchor.isActive = true
        
        NSLayoutConstraint.activate([
            packageInfoView2.deliverTableView.leadingAnchor.constraint(equalTo: packageInfoView2.leadingAnchor, constant: 15),
            packageInfoView2.deliverTableView.trailingAnchor.constraint(equalTo: packageInfoView2.trailingAnchor, constant: -15),
            packageInfoView2.deliverTableView.topAnchor.constraint(equalTo: packageInfoView2.desc1Lbl.bottomAnchor, constant: 10),
            
            packageInfoView2.deliverTf.topAnchor.constraint(equalTo: packageInfoView2.deliverTableView.bottomAnchor, constant: 15),
            packageInfoView2.deliverTf.heightAnchor.constraint(equalToConstant: 36),
            packageInfoView2.deliverTf.leadingAnchor.constraint(equalTo: packageInfoView2.deliverTableView.leadingAnchor),
            packageInfoView2.deliverTf.trailingAnchor.constraint(equalTo: packageInfoView2.deliverTableView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            servicePackageXib.premiumView.leadingAnchor.constraint(equalTo: servicePackageXib.leadingAnchor, constant: 15),
            servicePackageXib.premiumView.topAnchor.constraint(equalTo: packageInfoView2.bottomAnchor, constant: 15),
            servicePackageXib.premiumView.heightAnchor.constraint(equalToConstant: servicePackageXib.premiumView.frame.height),
            servicePackageXib.premiumView.trailingAnchor.constraint(equalTo: servicePackageXib.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            packageInfoView3.trailingAnchor.constraint(equalTo: servicePackageXib.trailingAnchor, constant: -5),
            packageInfoView3.leadingAnchor.constraint(equalTo: servicePackageXib.leadingAnchor, constant: 5),
            packageInfoView3.topAnchor.constraint(equalTo: servicePackageXib.premiumView.bottomAnchor, constant: 20),
            packageInfoView3.bottomAnchor.constraint(equalTo: servicePackageXib.bottomAnchor)
        ])
        
        package3HeightAnchor = packageInfoView3.heightAnchor.constraint(equalToConstant: 0)
        packageInfoView3.isHidden = true
        package3HeightAnchor.isActive = true
        
        deliverTable3HeightAnchor = packageInfoView3.deliverTableView.heightAnchor.constraint(equalToConstant: 0)
        deliverTable3HeightAnchor.isActive = true
        
        NSLayoutConstraint.activate([
            packageInfoView3.deliverTableView.leadingAnchor.constraint(equalTo: packageInfoView3.leadingAnchor, constant: 15),
            packageInfoView3.deliverTableView.trailingAnchor.constraint(equalTo: packageInfoView3.trailingAnchor, constant: -15),
            packageInfoView3.deliverTableView.topAnchor.constraint(equalTo: packageInfoView3.desc1Lbl.bottomAnchor, constant: 10),
            
            packageInfoView3.deliverTf.topAnchor.constraint(equalTo: packageInfoView3.deliverTableView.bottomAnchor, constant: 15),
            packageInfoView3.deliverTf.heightAnchor.constraint(equalToConstant: 36),
            packageInfoView3.deliverTf.leadingAnchor.constraint(equalTo: packageInfoView3.deliverTableView.leadingAnchor),
            packageInfoView3.deliverTf.trailingAnchor.constraint(equalTo: packageInfoView3.deliverTableView.trailingAnchor)
        ])
        
        
    }
    
    func generateViewGestures() {
        tapPremium.isEnabled = false
        
        tapStandard.addTarget(self, action: #selector(tapStandardView))
        tapPremium.addTarget(self, action: #selector(tapPremiumView))
        
        servicePackageXib.standardView.addGestureRecognizer(tapStandard)
        servicePackageXib.premiumView.addGestureRecognizer(tapPremium)
    }
    
    func displayAlert(message: String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okBtnAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okBtnAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func mapServiceInfo() {
        if let unit = packageInfoView1.unitTf.text,
           let quantity = packageInfoView1.quantityTf.text,
           let price = packageInfoView1.priceTf.text {
            let quantityNum = Double(quantity)
            let intprice = Double(price)
            let regular = ServicePackage(unit: unit, quantity: quantityNum, deliverables: deliverables1, packageType: PackageType.regular, description: packageInfoView1.packageDescTextView.text, price: intprice)
            servicePackages.append(regular)
        } else {
            displayAlert(message: "Please fill the info of your Regular package", title: "Invalid input")
            return
        }
        
        if servicePackageXib.standardCheckBox.isChecked == true {
            if let unit = packageInfoView2.unitTf.text,
               let quantity = packageInfoView2.quantityTf.text,
               let price = packageInfoView2.priceTf.text{
                let quantityNum = Double(quantity)
                let intprice = Double(price)
                let standard = ServicePackage(unit: unit, quantity: quantityNum, deliverables: deliverables2, packageType: PackageType.standard, description: packageInfoView2.packageDescTextView.text, price: intprice)
                servicePackages.append(standard)
            } else {
                displayAlert(message: "Please fill the info of your Standard package", title: "Invalid input")
                return
            }
        } else {
            if let index = servicePackages.firstIndex(where: {$0.packageType == PackageType.standard}) {
                servicePackages.remove(at: index)
                
            }
        }
        
        if servicePackageXib.premiumCheckBox.isChecked == true {
            if let unit = packageInfoView3.unitTf.text,
               let quantity = packageInfoView3.quantityTf.text,
               let price = packageInfoView3.priceTf.text {
                let standard = ServicePackage(unit: unit, quantity: Double(quantity), deliverables: deliverables3, packageType: PackageType.premium, description: packageInfoView3.packageDescTextView.text, price: Double(price))
                servicePackages.append(standard)
            } else {
                displayAlert(message: "Please fill the info of your Premium package", title: "Invalid input")
                return
            }
        } else {
            if let index = servicePackages.firstIndex(where: {$0.packageType == PackageType.premium}) {
                servicePackages.remove(at: index)
            }
        }
    }
    @objc func tapStandardView() {
        servicePackageXib.standardCheckBox.isChecked = !servicePackageXib.standardCheckBox.isChecked
        tapPremium.isEnabled = servicePackageXib.standardCheckBox.isChecked
        print(tapPremium.isEnabled)
        if tapPremium.isEnabled == true {
            servicePackageXib.premiumCheckBox.backgroundColor = .white
            package2HeightAnchor.constant = 900
            packageInfoView2.isHidden = false
            contentView.layoutIfNeeded()
        } else {
            servicePackageXib.premiumCheckBox.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            package2HeightAnchor.constant = 0
            packageInfoView2.isHidden = true
            contentView.layoutIfNeeded()
        }
    }
    
    @objc func tapPremiumView() {
        tapStandard.isEnabled = servicePackageXib.premiumCheckBox.isChecked
        servicePackageXib.premiumCheckBox.isChecked = !servicePackageXib.premiumCheckBox.isChecked
        
        if servicePackageXib.premiumCheckBox.isChecked == true {
            servicePackageXib.premiumCheckBox.backgroundColor = .white
            package3HeightAnchor.constant = 900
            packageInfoView3.isHidden = false
            contentView.layoutIfNeeded()
        } else {
            package3HeightAnchor.constant = 0
            packageInfoView3.isHidden = true
            contentView.layoutIfNeeded()
        }
    }
    
    @objc func saveBtnTap() {
        var servicePackageInputArray: [ServicePackageInput] = []
        
        self.serviceInput.serviceDescription = self.serviceDescriptionTf.text

        self.mapServiceInfo()

        for element in self.servicePackages {
            let a = ServicePackageInput(deliverables: element.deliverables, price: element.price, unit: element.unit, quantity: element.quantity, description: element.description, packageType: element.packageType)
            servicePackageInputArray.append(a)
        }
        serviceInput.servicePackages = servicePackageInputArray
        print(serviceInput)
        
        uploadImagesToCloud(portfolios: items, completion: {
            [weak self] urls in
            
            guard let self = self else { return }
            
            var servicesUrls: [String] = []
            
            if let porfo = urls {
                for element in porfo {
                    servicesUrls.append(element.absoluteString)
                }
            }
            self.serviceInput.servicePhotoUrls = servicesUrls
            print(urls!.count)
            Network.shared.apollo.perform(mutation: CreateServiceMutation(sellerId: "a35018ca-ca80-4934-8e18-018e2432470f", service: self.serviceInput)) {
                res in
                print(res)
            }
        })
        
    }
    
    func uploadImagesToCloud(portfolios: [UIImage], completion: @escaping(_: [URL]?) -> Void) {
        guard let uId = SQLiteCommands.getCurrentUserInfo()?.id else {return}
        var retUrls: [URL] = []
        var uploadImages: [AWSImageData] = []
        uploadImages = portfolios.map { data in
                return AWSImageData(type: .portfolio, image: data) }
        
        RRAWSRxUpload.upload(dataList: uploadImages, userId: uId)
            .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                let url = AWSS3.default().configuration.endpoint.url
                for i in 0..<response.count {
                    let publicURL = url?.appendingPathComponent(AWSBucket.bucketName).appendingPathComponent(response[i].key)
                    switch response[i].type {
                    case .avatar:
                        break;
                    case .portfolio:
                        if let publicURL = publicURL {
                            retUrls.append(publicURL)
                        }
                        break;
                    case .service:
                        break;
                    }
                }
                completion(retUrls)
            }, onError: { error in
                print(error.localizedDescription)
            }).disposed(by: rxbag)
    }
    
    func deleteBtnClick(index: Int, tableView: UITableView) {
        if tableView == packageInfoView1.deliverTableView {
            deliverables1.remove(at: index)
        } else if tableView == packageInfoView2.deliverTableView {
            deliverables2.remove(at: index)
        } else if tableView == packageInfoView3.deliverTableView {
            deliverables3.remove(at: index)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateCollectionViewHeight()
    }
    
    func updateCollectionViewHeight() {
        let height = self.categoryCollectionView.collectionViewLayout.collectionViewContentSize.height;
        
        collectionViewHeight.constant = height
        contentView.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    @IBAction func addImgClick(_ sender: UIButton) {
        showImagePicker()
    }
    
    @objc internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImg = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            items.append(editedImg)
            productImgTableView.scrollToRow(at: IndexPath(item: items.count - 1, section: 0), at: .bottom, animated: false)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func appendDeliverable(packageType: PackageType, deliverableString: String) {
        switch packageType {
        case .regular:
            deliverables1.append(deliverableString)
        case .standard:
            deliverables2.append(deliverableString)
        case .premium:
            deliverables3.append(deliverableString)
        default:
            break;
        }
    }
}

extension ServiceCreateViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "PlacesCollectionViewCell" , for: indexPath) as! PlacesCollectionViewCell
        cell.placeLabel.text = categories[indexPath.row].categoryName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = categoryCollectionView.cellForItem(at: indexPath) as! PlacesCollectionViewCell
        cell.checkBox.image = nil
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = categoryCollectionView.cellForItem(at: indexPath) as! PlacesCollectionViewCell
        let config = UIImage.SymbolConfiguration(scale: .medium)
        cell.checkBox.image = UIImage(systemName: "checkmark", withConfiguration: config)
        let categoryInput = JobCategoryInput(id: categories[indexPath.row].id!)
        serviceInput.category = categoryInput
    }
}

// MARK: UITABLEVIEW DELEGATE
extension ServiceCreateViewController: UITableViewDelegate, UITableViewDataSource, ImageCellDelegate {
    func removeBtnClick(index: Int) {
        items.remove(at: index)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == packageInfoView1.deliverTableView {
            return deliverables1.count
        }
        if tableView == packageInfoView2.deliverTableView {
            return deliverables2.count
        }
        if tableView == packageInfoView3.deliverTableView {
            return deliverables3.count
        }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == productImgTableView {
            let cell = productImgTableView.dequeueReusableCell(withIdentifier: "PortfolioImageTableViewCell") as! PortfolioImageTableViewCell
            cell.portfolioImageView.image = items[indexPath.row]
            cell.delegate = self
            cell.index = indexPath.row
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeliberablesTableViewCell") as? DeliberablesTableViewCell else { return UITableViewCell() }
            if tableView == packageInfoView1.deliverTableView {
                cell.deliverLabel.text = "- \(deliverables1[indexPath.row])"
            } else if tableView == packageInfoView2.deliverTableView {
                cell.deliverLabel.text = "- \(deliverables2[indexPath.row])"
            } else if  tableView == packageInfoView3.deliverTableView {
                cell.deliverLabel.text = "- \(deliverables3[indexPath.row])"
            }
            
            cell.index = indexPath.row
            cell.tableView = tableView
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ([packageInfoView1.deliverTableView, packageInfoView2.deliverTableView, packageInfoView3.deliverTableView].contains(tableView)) {
            return 30
        }
        
        return 170
    }
}



extension ServiceCreateViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var type: PackageType!
        if textField == packageInfoView1.deliverTf {
            type = PackageType.regular
        }
        
        if textField == packageInfoView2.deliverTf {
            type = PackageType.standard
        }
        
        if textField == packageInfoView3.deliverTf {
            type = PackageType.premium
        }
        
        if let text = textField.text {
            if text.count > 3 {
                appendDeliverable(packageType: type, deliverableString: text)
                textField.text = ""
            }
            
        }
        
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        var type: PackageType!
        if textField == packageInfoView1.deliverTf {
            type = PackageType.regular
        }
        
        if textField == packageInfoView2.deliverTf {
            type = PackageType.standard
        }
        
        if textField == packageInfoView3.deliverTf {
            type = PackageType.premium
        }
        
        if let text = textField.text {
            if text.suffix(1) == "," {
                let withPartialRange = text[..<text.index(before: text.endIndex)]
                print(withPartialRange)
                if withPartialRange.count > 3 {
                    appendDeliverable(packageType: type, deliverableString: String(withPartialRange))
                }
                textField.text = ""
            }
        }
    }
}
