//
//  ServiceDetailViewController.swift
//  final-project
//
//  Created by phuong nguyen on 6/25/21.
//

import UIKit

protocol ServiceControllerDelegate {
    func displayInfoWhenTapped(row: Int, packages: [PackageModel])
}

class ServiceDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, ServiceOrderProtocol {
    func goToConFirm() {
        guard let vc = storyboard?.instantiateViewController(identifier: "BookingConfirmViewController") as? BookingConfirmViewController
        else { return }
        if totalPrice == nil {
            if let price = packagesInfo[selectedPackage].price {
                totalPrice = Double(price)
            }
        }
        if let seller = self.sellerInfo,
           let service = self.serviceInfo,
           let price = self.totalPrice,
           let type = packagesInfo[selectedPackage].packageType,
           let categoryName = categoryName {
            let quantity = Double(productInfoCel.quantityTf.text ?? "")
            let unit = packagesInfo[selectedPackage].unit
            let viewInfo = ConfirmViewModel(categoryName: categoryName, sellerName: seller.fullName!, sellerAva: seller.avatarUrl, totalPrice: price, quantity: quantity, packageType: type, description: service.serviceDescription!, unit: unit)
            vc.viewModel = viewInfo
            vc.packageId = packagesInfo[selectedPackage].id
            vc.userId = user?.id
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    let productInfoCel = Bundle.main.loadNibNamed("ServiceOrderTableViewCell", owner: self, options: nil)?.first as! ServiceOrderTableViewCell
    var totalPrice: Double?
    var delegate: ServiceControllerDelegate?
    let user = SQLiteCommands.getCurrentUserInfo()
    var serviceId: String!
    var categoryName: String?
    var serviceInfo: ServiceModel?
//    var orderQuantity: Double?
    var sUserId: String?
    var selectedPackage: Int = 0
    var sellerInfo: Seller?
    var packagesInfo: [PackageModel] = [] {
        didSet {
            pageTableView.reloadData()
        }
    }
    @IBOutlet weak var pageTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        pageTableView.delegate = self
        pageTableView.dataSource = self
//        pageTableView.rowHeight = UITableView.automaticDimension
//        pageTableView.estimatedRowHeight = 150
        view.backgroundColor = .white
        productInfoCel.delegate = self
        fetchServiceDetail()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func fetchServiceDetail() {
        Network.shared.apollo.fetch(query: ServiceQuery(id: self.serviceId)) {
            res in
            switch(res) {
            case .success(let response):
                if let fetchData = response.data?.service {
                    let id = fetchData.resultMap["id"] as? String
                    let desc = fetchData.resultMap["serviceDescription"] as? String
                    let photoUrls = fetchData.resultMap["servicePhotoUrls"] as? [String]
                    let category = fetchData.resultMap["category"] as? [String: Any]
                    if let categoryData = category {
                        self.categoryName = categoryData["categoryName"] as? String
                    }
                    
                    let sellerData = fetchData.resultMap["seller"] as? [String: Any]
                    
                    if let seller = sellerData {
                        let id = seller["id"] as? String
                        let fullName = seller["fullName"] as? String
                        let portfolioUrls = seller["portfolioPhotoUrls"] as? [String]
                        let avatarUrl = seller["avatarUrl"] as? String
                        
                        if let user = seller["user"] as? [String: Any] {
                            if let uId = user["id"] as? String {
                                self.sUserId = uId
                            }
                        }
                        
                        self.sellerInfo = Seller(id: id, portfolioUrls: portfolioUrls, avatarUrl: avatarUrl, description: nil, fullName: fullName, services: nil, userId: nil)
                    }
                    
                    let packages = fetchData.resultMap["packages"] as? [[String: Any]]
                    if let packages = packages {
                        for package in packages {
                            let unit = package["unit"] as? String
                            let description = package["description"] as? String
                            let deliverables = package["deliverables"] as? [String]
                            let quantity = package["quantity"] as? Double
                            if let price = package["price"] as? Double,
                            let pid = package["id"] as? String,
                            let type = package["packageType"] as? PackageType {
                                let packageData = PackageModel(id: pid, price: Float(price), type: type, unit: unit, quantity: quantity, description: description, deliverables: deliverables)
                                self.packagesInfo.append(packageData)
                            }
                        }
                    }
                    
                    self.serviceInfo = ServiceModel(id: id, serviceDescription: desc, servicePhotoUrls: photoUrls, packages: nil, seller: nil)
                }
            case .failure(let err):
                print(err.localizedDescription)
                break;
            }
        }
    }
    
    //MARK: TableView delegation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == pageTableView && indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
            vc.userId = self.sUserId!
            vc.isProfileOwner = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == productInfoCel.deliverablesTableView {
            return productInfoCel.deliverables.count
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == productInfoCel.deliverablesTableView {
            let cell = Bundle.main.loadNibNamed("DeliverableTableViewCell", owner: self, options: nil)?.first as! DeliverableTableViewCell
            cell.deliverableLabel.text = "- \(productInfoCel.deliverables[indexPath.row])"
            return cell
        }
        if indexPath.row == 0 {
            guard let cell = pageTableView.dequeueReusableCell(withIdentifier: "ServiceTopCell") as? ServiceTopCell else { return UITableViewCell() }
            if let seller = sellerInfo {
                cell.providerDisplayName.text = sellerInfo?.fullName
                if let avaUrl = seller.avatarUrl {
                    if let url = URL(string: avaUrl) {
                        NukeLoad.loadContent(url: url, imageView: cell.providerAva)
                    }
                }
            }
            
            if let desc = serviceInfo?.serviceDescription {
                cell.serviceDescription.text = desc
            }
            
            return cell
            
        }
        
        if indexPath.row == 1 {
            guard let cell = Bundle.main.loadNibNamed("ProductImgSlideTableViewCell", owner: self, options: nil)?.first as? ProductImgSlideTableViewCell else { return UITableViewCell() }
            cell.setDelegateAndDataSource(delegate: self, dataSource: self)
            return cell
        }
        
        if indexPath.row == 2 {
            self.delegate = productInfoCel
            productInfoCel.setDelegateAndDataSourceForDeliTableView(delegate: self, dataSource: self, tfDelegate: self)
           
            productInfoCel.packages = packagesInfo
            
            productInfoCel.setDelegationForCollectionView(delegate: self, dataSource: self)
            if packagesInfo.count > 0 {
                productInfoCel.displayPackageInfo(package: packagesInfo[0])
            }
            
            return productInfoCel
        }
        
        else {
            return UITableViewCell()
        }
    }
    
    //MARK: CollectionView delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 10 {
            return packagesInfo.count
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.tag == 10) {
            selectedPackage = indexPath.row
            if let price = packagesInfo[indexPath.row].price {
                totalPrice = Double(price)
            }
            delegate?.displayInfoWhenTapped(row: selectedPackage, packages: packagesInfo)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView.tag == 10) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabbarcell", for: indexPath) as! TabBarCollectionViewCell

            if indexPath.row == 0 {
                cell.tabLabel.text = "Regular"
                
            }

            else if indexPath.row == 1 {
                cell.tabLabel.text = "Standard"
                cell.isSelected = false
            }

            else if indexPath.row == 2 {
                cell.tabLabel.text = "Premium"
                cell.isSelected = false
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductImgCollectionViewCell", for: indexPath) as! ProductImgCollectionViewCell
            return cell
            
        }
    }
    
    //MARK: TEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let quantity = Double(textField.text ?? "") {
            let price = productInfoCel.calculatePrice(quantity: quantity, row: selectedPackage)
            self.totalPrice = price
            let dPrice = Utilitites.formatPrice(price: price)
            productInfoCel.servicePriceLabel.text = "\(dPrice) VND"
            productInfoCel.nextButton.setTitle("Continue (\(String(describing: productInfoCel.servicePriceLabel.text!)))", for: .normal)
        }
        return true
    }
}
