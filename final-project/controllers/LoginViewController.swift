//
//  ViewController.swift
//  final-project
//
//  Created by phuong nguyen on 2/19/21.
//

import UIKit

class LoginViewController: BaseViewController, LoginCellDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var swipeRight = UISwipeGestureRecognizer()
    @IBOutlet weak var loginTableView: UITableView!
    var tbCell: LoginTableViewCell?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginTableView.delegate = self
        self.loginTableView.dataSource = self
        swipeRight.direction = .right
        self.loginTableView.addGestureRecognizer(swipeRight)
        swipeRight.addTarget(self, action: #selector(swipeLeftAction(sender:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let user = SQLiteCommands.getCurrentUserInfo()
        if user != nil {
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @objc func swipeLeftAction(sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .right:
            //            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            //            let vc = sb.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            self.navigationController?.popViewController(animated: true)
        default:
            print("")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationBar(animated: animated)
    }
    
    //MARK: FORM RESET
    
    func resetForm(cell: LoginTableViewCell) {
        cell.errorMessLbl.text = ""
        cell.emailTf.layer.borderColor = .none
        cell.passwordTf.layer.borderColor = .none
        cell.passwordTf.layer.borderWidth = 0.5
        cell.emailTf.layer.borderWidth = 0.5
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let tbCell = tbCell {
            self.resetForm(cell: tbCell)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let tbCell = tbCell {
            tbCell.displayError()
        }
        return true
    }
    
    // MARK: Login CELL DELEGATION
    
    func loginBtnTap(cell: LoginTableViewCell) {
        if let email = cell.emailTf.text, let password = cell.passwordTf.text {
            UserManager.shared.login(email: email, password:password, completionBlock: {
                res in
                switch (res) {
                case true:
                    let alert = UIAlertController(title: "Success", message: "You are now logged in", preferredStyle: .alert)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when){
                        // your code with delay
                        alert.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                case false:
                    cell.displayError()
                }
            })
        } else {
            cell.displayError()
        }
    }
    
    func signInTap() {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)

        guard let vc = sb.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else { return }
        self.navigationController?.pushViewController(vc, animated: true)
        print("clicksignup")
    }
    
    // MARK: TABLE VIEW DELEGATION
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            tbCell = Bundle.main.loadNibNamed("LoginTableViewCell", owner: self, options: nil)?.first as? LoginTableViewCell
            
            if let tbCell = tbCell {
                tbCell.passwordTf.delegate = self
                tbCell.emailTf.delegate = self
                tbCell.delegate = self
                return tbCell
            }
            return tbCell!
            
        } else if (indexPath.row == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomTableViewCell", for: indexPath) as UITableViewCell
            return cell
        }
        
        else {
            let cell = Bundle.main.loadNibNamed("HomeTableViewCell", owner: self, options: nil)?.first as! HomeTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenSize: CGRect = UIScreen.main.bounds
        if (indexPath.row == 0) {
            return 295
        } else { return screenSize.height - 350 }
    }
}

