//
//  TabBarViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit

class TabBarViewController: UITabBarController {
    let arrayOfImageNameForSelectedState = ["house", "clock", "magnifyingglass", "person", "ellipsis.circle"]
    let itemNames = ["Home", "Bookings","Search", "Your Profile", "More"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let config = UIImage.SymbolConfiguration(weight: .light)
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                let imageNameForSelectedState   = arrayOfImageNameForSelectedState[i]
                let imageNameForUnselectedState = arrayOfImageNameForSelectedState[i]
                self.tabBar.items?[i].title = itemNames[i]
                
                
                self.tabBar.items?[i].selectedImage = UIImage(systemName: imageNameForSelectedState, withConfiguration: config)?.withTintColor(UIColor(red: 0.97, green: 0.64, blue: 0.30, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].image = UIImage(named: imageNameForUnselectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }   
    }
}
