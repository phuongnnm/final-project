//
//  TestViewController.swift
//  final-project
//
//  Created by phuong nguyen on 6/9/21.
//

import UIKit
import FSCalendar

protocol CalendarDelegate : class {
    func selectedDate(Value : Date)
}

class CalendarViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource {
    weak var delegate: CalendarDelegate?
    var selectedDate: Date? {
        didSet {
            print(self.selectedDate)
            if let selectedDate = self.selectedDate {
                delegate?.selectedDate(Value: selectedDate)
            }
        }
    }
    
    @IBOutlet weak var cal: FSCalendar!
    let formatter = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        cal.appearance.todayColor = nil
        cal.appearance.titleTodayColor = cal.appearance.titleDefaultColor
        cal.appearance.headerMinimumDissolvedAlpha = 0
        cal.appearance.headerTitleColor = .darkGray
        cal.appearance.weekdayTextColor = UIColor(red: 230/255, green: 174/255, blue: 70/255, alpha: 1.0)
        
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date { return Date() }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        formatter.dateFormat = "dd-MM-yyyy"
        guard let selectedDate = cal.selectedDate else {
            print("Calendar error")
            return
        }
        self.selectedDate = selectedDate
        
    }
}
