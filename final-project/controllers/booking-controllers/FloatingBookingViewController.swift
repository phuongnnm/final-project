//
//  FloatingBookingViewController.swift
//  final-project
//
//  Created by phuong nguyen on 6/11/21.
//

import UIKit
import FloatingPanel

class SellerPanelViewController: UIViewController{
    let formatter = DateFormatter()
    
    
    var currentStep = 0 {
        didSet {
            loadStep(step: currentStep)
            if currentStep > 0 {
                prevButton.isEnabled = true
                
            } else {
                prevButton.isEnabled = false
            }
        }
    }
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var prevButton: UIButton!
    
    @IBAction func prevButtonClick(_ sender: UIButton) {
        currentStep -= 1
    }
    @IBOutlet weak var stepContentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.isEnabled = false
        prevButton.isEnabled = false
        prevButton.isHidden = true
        nextButton.setTitleColor(.lightGray, for: .disabled)
        prevButton.setTitleColor(.lightGray, for: .disabled)
        
    }
    
    @IBAction func nextButtonClick(_ sender: UIButton) {
        currentStep += 1
        switch currentStep {
        case 1:
            break;
        default:
            break
        }
    }
    
    func loadStep(step: Int) {
        switch (step) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
        }
    }
}
