//
//  BookingPanelViewController.swift
//  final-project
//
//  Created by phuong nguyen on 6/12/21.
//

import UIKit
import FloatingPanel

class BookingPanelViewController: BaseViewController, FloatingPanelControllerDelegate, PanelDelegate {
    let floatingPanel = FloatingPanelController()
    
    @IBOutlet weak var guidingLabel: UILabel!
    func updateStep(val: Int) {
        switch val {
        case 0:
            guidingLabel.text = "Which date should we deliver our services or products?"
            break;
        case 1: guidingLabel.text = "Which service are you looking for?"
            break;
        default:
            break;
        }
    }
    
    func movePanelToAPosition(position: FloatingPanelState) {
        floatingPanel.move(to: position, animated: true)
    }
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var contentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureGradientLayer(view: contentView)
        floatingPanelConfig()
        showFloatingPanel()
    }
    
    func floatingPanelConfig() {
        floatingPanel.layout = BookingFloatingPanelLayout()
        let appearance = SurfaceAppearance()
        floatingPanel.delegate = self
        //        floatingPanel.isRemovalInteractionEnabled = true
        appearance.cornerRadius = 20.0
        appearance.backgroundColor = .clear
        floatingPanel.surfaceView.appearance = appearance
    }
    
    func configureGradientLayer(view: UIView)
    {
        view.backgroundColor = .clear
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red: 1, green: 0.9, blue: 0.5, alpha: 1).cgColor, UIColor(red: 1, green: 0.3, blue: 0.2, alpha: 0.7).cgColor]
        gradient.locations = [0, 1]
        gradient.frame = view.bounds
        contentView.layer.insertSublayer(gradient, at: 0)
    }
    
    func showFloatingPanel() {
        guard let vc = storyboard?.instantiateViewController(identifier: "PanelViewController") as? PanelViewController else { return }
        vc.delegate = self
        floatingPanel.set(contentViewController: vc)
        
        floatingPanel.addPanel(toParent: self)
    }
    
    func loadStep(step: Int) {
        switch (step) {
        case 0:
            break;
        case 1:
            
            break;
        default:
            break;
        }
    }
    
//    func floatingPanelWillEndDragging(_ vc: FloatingPanelController, withVelocity velocity: CGPoint, targetState: UnsafeMutablePointer<FloatingPanelState>) {
//        
//        floatingPanel.move(to: .half, animated: true)
//    }
    
}

class BookingFloatingPanelLayout: FloatingPanelLayout {
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return [
            .full: FloatingPanelLayoutAnchor(absoluteInset: 16.0, edge: .top, referenceGuide: .safeArea),
            .half: FloatingPanelLayoutAnchor(fractionalInset: 0.6, edge: .bottom, referenceGuide: .safeArea),
            .tip: FloatingPanelLayoutAnchor(absoluteInset: 400, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}
