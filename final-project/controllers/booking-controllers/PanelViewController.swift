//
//  PanelViewController.swift
//  final-project
//
//  Created by phuong nguyen on 6/13/21.
//

import UIKit
import FSCalendar
import FloatingPanel

protocol PanelDelegate: class {
    func movePanelToAPosition(position: FloatingPanelState)
    func updateStep(val: Int)
}

class PanelViewController: UIViewController, UIScrollViewDelegate , FSCalendarDelegate, UITableViewDelegate, UITableViewDataSource, FSCalendarDataSource {
    
    
    weak var delegate: PanelDelegate?
    
    @IBOutlet weak var prevBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        nextBtn.isEnabled = false
        prevBtn.isEnabled = false
        nextBtn.setTitleColor(.lightGray, for: .disabled)
        prevBtn.setTitleColor(.lightGray, for: .disabled)
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        scrollView.isScrollEnabled = false
        createCalendarUI()
        createJobsView()
        createServiceItemTableView(data: "non")
    }
    let formatter = DateFormatter()
    var currentStep = 0 {
        didSet {
            if (currentStep > 0) {
                prevBtn.isEnabled = true
            } else {
                prevBtn.isEnabled = false
            }

            if currentStep >= 4 {
                nextBtn.isEnabled = false
            }
        }
    }
    
    let images = ["flash", "cleaning", "plumbing"]
    let tb2Images = ["social-media", "graphic-design", "browser", "translation", "music-note", "document"]
    let titles = ["Electronics repair", "House cleaning", "Plumbing"]
    let tb2Titles = ["Marketing Contents", "Graphic Design", "Programming and Tech", "Translating", "Music Producing", "Writing"]
    
    var selectedDate: Date? {
        didSet {
            if selectedDate != nil {
                nextBtn.isEnabled = true
            }
        }
    }
    
    //MARK: JOBS TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 77 {
            let vc = storyboard?.instantiateViewController(identifier: "ServiceDetailViewController") as! ServiceDetailViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            goToNextSlide(panelPosition: .full)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return 6
        } else if tableView.tag == 77 {
            return 10
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            guard let cell = Bundle.main.loadNibNamed("LabourTableViewCell", owner: self, options: nil)?.first as? LabourTableViewCell else {
                return UITableViewCell()
            }
            cell.serviceImage.image = UIImage(named: tb2Images[indexPath.row])
            cell.serviceName.text = tb2Titles[indexPath.row]
            return cell
        } else if tableView.tag == 77 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesItemCell") as! ServicesItemCell
            cell.descriptionLabel.text = "I design logos for your business and help you with branding"
            return cell
        }
        
        else {
            guard let cell = Bundle.main.loadNibNamed("LabourTableViewCell", owner: self, options: nil)?.first as? LabourTableViewCell else {
                return UITableViewCell()
            }
            cell.serviceImage.image = UIImage(named: images[indexPath.row])
            cell.serviceName.text = titles[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 77 {
            return 280
        }
        return 64
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        if self.scrollView.contentOffset.x > 0 {
            delegate?.movePanelToAPosition(position: .half)
            let contentOffSet = self.scrollView.contentOffset.x - view.bounds.width
            self.scrollView.setContentOffset(CGPoint(x: contentOffSet, y: scrollView.contentOffset.y), animated: true)
        }
        
        currentStep = calculateCurrentStep() - 1
        delegate?.updateStep(val: currentStep)
        switch currentStep {
        case 0:
            if selectedDate != nil {
                nextBtn.isEnabled = true
            }
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        default:
            break;
        }
    }
    
    func calculateCurrentStep() -> Int {
        let offSet = Int(scrollView.contentOffset.x / view.frame.width)
        return offSet
    }
    
    func goToNextSlide(panelPosition: FloatingPanelState) {
        currentStep = calculateCurrentStep() + 1
        if scrollView.contentOffset.x < self.view.bounds.width * CGFloat(slides.count-1) {
            delegate?.movePanelToAPosition(position: panelPosition)
            let contentOffSet = scrollView.contentOffset.x + view.bounds.width
            scrollView.setContentOffset(CGPoint(x: contentOffSet, y: scrollView.contentOffset.y), animated: true)
            nextBtn.isEnabled = false
        }
        currentStep = calculateCurrentStep() + 1
        delegate?.updateStep(val: currentStep)
    }
    
    @IBAction func nextBtnClick(_ sender: UIButton) {
        goToNextSlide(panelPosition: .half)
    }
    
    var slides:[UIView] = [];
    
    func createCalendarUI() {
        let calView = Bundle.main.loadNibNamed("CalendarView", owner: self, options: nil)?.first as! CalendarView
        calView.translatesAutoresizingMaskIntoConstraints = false
        slides[0].addSubview(calView)
        NSLayoutConstraint.activate([
            calView.trailingAnchor.constraint(equalTo: slides[0].trailingAnchor),
            calView.bottomAnchor.constraint(equalTo: slides[0].bottomAnchor),
            calView.topAnchor.constraint(equalTo: slides[0].topAnchor),
            calView.leadingAnchor.constraint(equalTo: slides[0].leadingAnchor),
        ])
        calView.setDelegateAndDataSourceForCalendar(delegate: self, dataSource: self)
    }
    
    func createJobsView() {
        let jobsView = Bundle.main.loadNibNamed("JobCategoriesView", owner: self, options: nil)?.first as! JobCategoriesView
        jobsView.addDelegateAndDataSource(delegate: self, dataSource: self, scrollViewDelegate: self)
        jobsView.translatesAutoresizingMaskIntoConstraints = false

        slides[1].addSubview(jobsView)
        NSLayoutConstraint.activate([
            jobsView.trailingAnchor.constraint(equalTo: slides[1].trailingAnchor),
            jobsView.bottomAnchor.constraint(equalTo: slides[1].bottomAnchor),
            jobsView.topAnchor.constraint(equalTo: slides[1].topAnchor),
            jobsView.leadingAnchor.constraint(equalTo: slides[1].leadingAnchor),
        ])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom <= height {
            delegate?.movePanelToAPosition(position: .full)
        }
    }
    
    func createServiceItemTableView(data: String) {
        guard let service = Bundle.main.loadNibNamed("UserServiceView", owner: self, options: nil)?.first as? UserServiceView else { return }
        slides[2].addSubview(service)
        service.setDelegateAndDataSourceTableView(delegate: self, dataSource: self)
        service.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            service.trailingAnchor.constraint(equalTo: slides[2].trailingAnchor),
            service.bottomAnchor.constraint(equalTo: slides[2].bottomAnchor, constant: -155),
            service.topAnchor.constraint(equalTo: slides[2].topAnchor),
            service.leadingAnchor.constraint(equalTo: slides[2].leadingAnchor),
        ])
       
    }
    
    func createSlides() -> [UIView] {
        let s1 = UIView(frame: CGRect(x: 0, y: 70, width: view.frame.size.width, height: view.frame.size.height - 70))
        let s2 = UIView(frame: CGRect(x: 0, y: 70, width: view.frame.size.width, height: 1100))
        let s3 = UIView(frame: CGRect(x: 0, y: 70, width: view.frame.size.width, height: view.frame.size.height))
        let s4 = UIView(frame: CGRect(x: 0, y: 70, width: view.frame.size.width, height: view.frame.size.height - 70))
        return [s1, s2, s3, s4]
    }
    
    func setupSlideScrollView(slides : [UIView]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        let color: [UIColor] = [.red, .blue, .white, .cyan]
        for i in 0 ..< 4 {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height - 70)
            slides[i].backgroundColor = color[i]
            scrollView.addSubview(slides[i])
        }
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date { return Date() }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        formatter.dateFormat = "dd-MM-yyyy"
        guard let selectedDate = calendar.selectedDate else {
            print("Calendar error")
            return
        }
        self.selectedDate = selectedDate
        
    }
}
