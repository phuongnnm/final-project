//
//  BecomeSellerViewController.swift
//  final-project
//
//  Created by phuong nguyen on 7/11/21.
//

import UIKit
import FloatingPanel
import Apollo
import RxCocoa
import RxSwift
import AWSS3
import PromiseKit

class BecomeSellerViewController: BaseViewController, UIScrollViewDelegate, PersonalInfoDelegate, PortfolioViewDelegate, ImageCellDelegate, WorkingPlaceDelegate {
    var a = 0
    let rxbag = DisposeBag()
    var avaimg: Data?
    var jobCategories: [JobCategory] = [] {
        didSet {
            print("did set ne")
            serviceVC.categories = jobCategories
        }
    }
    var portfolioUrls: [URL] = []
    var serviceUrls: [URL] = []
    var avatarUrl: URL?
    let userInfo = SQLiteCommands.getCurrentUserInfo()
    var serviceVC: ServiceCreateViewController!
    var servicePackageInputArray: [ServicePackageInput] = []
    func animatePage() {
        //        a += 1
        //        print("dsdsa \(a)")
        //        scrollView2.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: workingPlaceView.contentView.frame.height)
        //
        //        scroll2constraint.constant = workingPlaceView.contentView.frame.height
        //
        //        UIView.animate(withDuration: 0.5) {
        //            self.scrollView2.layoutIfNeeded()
        //        }
    }
    
    let serviceView:UIView = {
        let sView = UIView(frame: CGRect.zero)
        sView.translatesAutoresizingMaskIntoConstraints = false
        return sView
    }()
    
    override func viewDidLayoutSubviews() {
        //        print(workingPlaceView.contentView.frame.height)
        //        if currentStep == 2 {
        //            animatePage()
        //        }
    }
    
    //MARK: Generate nib
    let portfolioView = Bundle.main.loadNibNamed("PortfolioUploadView", owner: self, options: nil)?.first as! PortfolioUploadView
    
    let personalInfoView = Bundle.main.loadNibNamed("PersonalInfoView", owner: self, options: nil)?.first as! PersonalInfoView
    
    let progressLayerView = Bundle.main.loadNibNamed("ProgressLayerView", owner: self, options: nil)?.first as! ProgressLayerView
    
    var scroll2constraint: NSLayoutConstraint!
    var stack = UIStackView()
    
    @IBOutlet weak var pageScrollView: UIScrollView!
    
    var currentStep = 0
    var portfolioItems: [PortfolioItem] = [] {
        didSet {
            portfolioView.imageTableView.reloadData()
        }
    }
    var scrollView2 = UIScrollView()
    let labelTwo: UILabel = {
        let label = UILabel()
        label.text = "Scroll Bottom"
        label.backgroundColor = .green
        label.frame = CGRect(x: 0, y: 0, width: 60, height: 25)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Create your seller profile"
        label.textAlignment = .center
        label.font = UIFont(name: "PingFang SC", size: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nextBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Next step >>", for: .normal)
        btn.setTitleColor(.darkGray, for: .disabled)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = .systemYellow
        btn.layer.cornerRadius = 12.0
        btn.clipsToBounds = true
        btn.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 14.5)
        return btn
    }()
    
    @objc let prevBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("<< Previous", for: .normal)
        btn.setTitleColor(.darkGray, for: .disabled)
        btn.setTitleColor(.black, for: .normal)
        btn.backgroundColor = .clear
        btn.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 14.5)
        return btn
    }()
    
    var steps: [UIView]! = []
    var slides: [UIView]! = []
    
    
    //MARK: VIEWDIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        pageScrollView.delegate = self
        portfolioView.delegate = self
        scrollView2.delegate = self
        scrollView2.showsVerticalScrollIndicator = false
        pageScrollView.showsVerticalScrollIndicator = false
        scrollView2.isScrollEnabled = false
        view.addSubview(nextBtn)
        view.addSubview(prevBtn)
        createPages()
        generateViews()
        createStepper()
        steps[0].tintColor = .orange
        nextBtn.addTarget(self, action: #selector(nextBtnClick), for: .touchUpInside)
        prevBtn.addTarget(self, action: #selector(prevBtnClick), for: .touchUpInside)
        generatePagesUI()
    
        view.addSubview(progressLayerView)
        view.bringSubviewToFront(progressLayerView)
        progressLayerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            progressLayerView.topAnchor.constraint(equalTo: view.topAnchor),
            progressLayerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            progressLayerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            progressLayerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        ])
        self.progressLayerView.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func generateViews() {
        titleLabel.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45)
        pageScrollView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: pageScrollView.topAnchor, constant: 3),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 0)
        ])
        nextBtn.translatesAutoresizingMaskIntoConstraints = false
        prevBtn.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            prevBtn.topAnchor.constraint(equalTo: pageScrollView.bottomAnchor, constant: 5),
            prevBtn.leadingAnchor.constraint(equalTo: pageScrollView.leadingAnchor, constant: 20),
            prevBtn.heightAnchor.constraint(equalToConstant: 40),
            
            nextBtn.topAnchor.constraint(equalTo: pageScrollView.bottomAnchor, constant: 5),
            nextBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            nextBtn.leadingAnchor.constraint(equalTo: prevBtn.trailingAnchor, constant: 15),
            nextBtn.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    //MARK: Upload image to AWS
    func uploadImagesToCloud(completion: @escaping(_: URL?, _: [URL]?, _: [URL]?) -> Void) {
        if let userId = userInfo?.id {
            var uploadImages: [AWSImageData] = []
            if portfolioItems.count > 0 {
                uploadImages = portfolioItems.map { data in
                    return AWSImageData(type: .portfolio, image: data.imageItem) }
            }
            
            if let avaData = personalInfoView.avaImgView.image {
                uploadImages.append(AWSImageData(type: .avatar, image: avaData))
            }
            
            if (serviceVC.items.count > 0) {
                for i in 0..<serviceVC.items.count {
                    uploadImages.append(AWSImageData(type: .service, image: serviceVC.items[i]))
                }
            }
            
            if !uploadImages.isEmpty {
                RRAWSRxUpload.upload(dataList: uploadImages, userId: userId)
                    .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
                    .observe(on: MainScheduler.instance)
                    .subscribe(onNext: { [self] response in
                        let url = AWSS3.default().configuration.endpoint.url
                        for i in 0..<response.count {
                            let publicURL = url?.appendingPathComponent(AWSBucket.bucketName).appendingPathComponent(response[i].key)
                            switch response[i].type {
                            case .avatar:
                                self.avatarUrl = publicURL
                                break;
                            case .portfolio:
                                self.portfolioUrls.append(publicURL!)
                                break;
                            case .service:
                                self.serviceUrls.append(publicURL!)
                            }
                        }
                        completion(avatarUrl, portfolioUrls, serviceUrls)
                    }, onError: { error in
                        print(error.localizedDescription)
                    }).disposed(by: rxbag)
            }
        }
    }
    
    func createSellerProfile(porfolioUrls: [String]?, avatarUrl: String?, description: String, serviceUrls: [String]?, serviceInfo: ServiceInput, firstName: String, lastName: String) {
        let seller = SellerInput(firstName: firstName, lastName: lastName, description: description, portfolioPhotoUrls: porfolioUrls, service: serviceInfo, avatarUrl: avatarUrl)
        progressLayerView.progressView.progress = 0.8
        if let id = userInfo?.id {
            Network.shared.apollo.perform(mutation: CreateSellerProfileMutation(seller: seller, userId: id)) {
                res in
                switch(res) {
                    case .success(let response):
                        guard let ret = response.data?.createSellerProfile else {
                            Network.shared.apollo.perform(mutation: DeleteExistedSellerFolderMutation(filepath: self.userInfo!.id)) {_ in
                                print("deleted")
                            }
                            self.displayAlert(ret: false)
                            return
                        }
                        print(ret)
                        self.progressLayerView.isHidden = true
                        self.displayAlert(ret: true)
                        break;
                case .failure(let err):
                    print(err.localizedDescription)
                }
            }
        }
       
    }
    
    func createStepper() {
        for i in 0..<3 {
            if i != 2 {
                let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                imgView.image = UIImage(systemName: "\(i + 1).circle")
                imgView.contentMode = .scaleAspectFit
                imgView.tintColor = .lightGray
                NSLayoutConstraint.activate([
                    imgView.heightAnchor.constraint(equalToConstant: 20),
                    imgView.widthAnchor.constraint(equalToConstant: 20),
                ])
                steps.append(imgView)
                
                let sview = UIView()
                sview.frame = CGRect(x: 0, y: 0, width: 70, height: 2)
                sview.backgroundColor = .lightGray
                
                sview.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([
                    sview.heightAnchor.constraint(equalToConstant: 2),
                    sview.widthAnchor.constraint(equalToConstant: sview.frame.width)
                ])
                steps.append(sview)
            } else {
                let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                imgView.image = UIImage(systemName: "\(i + 1).circle")
                imgView.contentMode = .scaleAspectFit
                imgView.tintColor = .lightGray
                steps.append(imgView)
            }   
        }
        
        stack = UIStackView(arrangedSubviews: steps)
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .equalSpacing
        stack.translatesAutoresizingMaskIntoConstraints = false
        pageScrollView.addSubview(stack)
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            stack.leadingAnchor.constraint(equalTo: pageScrollView.leadingAnchor, constant: 20),
            stack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
        ])
    }
    
    func generatePagesUI() {
        personalInfoView.delegate = self
        personalInfoView.translatesAutoresizingMaskIntoConstraints = false
        slides[0].addSubview(personalInfoView)
        NSLayoutConstraint.activate([
            personalInfoView.topAnchor.constraint(equalTo: slides[0].topAnchor),
            personalInfoView.trailingAnchor.constraint(equalTo: slides[0].trailingAnchor),
            personalInfoView.leadingAnchor.constraint(equalTo: slides[0].leadingAnchor),
            personalInfoView.bottomAnchor.constraint(equalTo: pageScrollView.bottomAnchor)
        ])
        
        portfolioView.setDelegateAndDataSouce(delegate: self, dataSource: self)
        portfolioView.translatesAutoresizingMaskIntoConstraints = false
        slides[1].addSubview(portfolioView)
        NSLayoutConstraint.activate([
            portfolioView.topAnchor.constraint(equalTo: slides[1].topAnchor),
            portfolioView.trailingAnchor.constraint(equalTo: slides[1].trailingAnchor),
            portfolioView.leadingAnchor.constraint(equalTo: slides[1].leadingAnchor),
            portfolioView.bottomAnchor.constraint(equalTo: nextBtn.topAnchor, constant: -5)
        ])
        let controller = storyboard!.instantiateViewController(withIdentifier: "ServiceCreateViewController") as! ServiceCreateViewController
        serviceVC = controller
        self.addChild(serviceVC)
        
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        controller.view.frame = view.frame
        serviceView.addSubview(controller.view)
        controller.didMove(toParent: self)
        slides[2].addSubview(serviceView)
        NSLayoutConstraint.activate([
            serviceView.topAnchor.constraint(equalTo: slides[2].topAnchor),
            serviceView.trailingAnchor.constraint(equalTo: slides[2].trailingAnchor),
            serviceView.leadingAnchor.constraint(equalTo: slides[2].leadingAnchor),
            serviceView.bottomAnchor.constraint(equalTo: nextBtn.topAnchor, constant:  -5),
            controller.view.topAnchor.constraint(equalTo: serviceView.topAnchor),
            controller.view.trailingAnchor.constraint(equalTo: serviceView.trailingAnchor),
            controller.view.leadingAnchor.constraint(equalTo: serviceView.leadingAnchor),
            controller.view.bottomAnchor.constraint(equalTo: serviceView.bottomAnchor)
        ])
    }
    
    static func deleteAWSFolder(userId: String) -> Promise<String> {
        return Promise<String> {
            ret in
            Network.shared.apollo.perform(mutation: DeleteExistedSellerFolderMutation(filepath: userId)) {
                res in
                switch (res) {
                case .success(let response):
                    ret.fulfill((response.data?.deleteExistedSellerFolder?.message)!)
                    
                case .failure(let err):
                    ret.reject(err)
                }
            }
        }
    }
    
    func displayAlert(ret: Bool) {
        switch ret {
        case true:
        let alert = UIAlertController(title: "Success", message: "Created your seller profile", preferredStyle: .alert)
        let goToProfile = UIAlertAction(title: "My profile", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        })
            
        alert.addAction(goToProfile)
            
        self.present(alert, animated: true, completion: nil)
        
        case false:
            let alert = UIAlertController(title: "Error", message: "Failed to create your seller profile", preferredStyle: .alert)
            let dismiss = UIAlertAction(title: "Cancel", style: .default, handler: {action in
                alert.dismiss(animated: true, completion: nil)
                
            })
            alert.addAction(dismiss)
            self.present(alert, animated: true, completion: nil)
        }
    }

    //MARK: Button click action
    @objc func nextBtnClick() {
        
        if currentStep == 2 {
            firstly {
                BecomeSellerViewController.deleteAWSFolder(userId: userInfo!.id)
            }.done {
                mess in
                self.progressLayerView.isHidden = false
                self.progressLayerView.progressView.progress = 0.4
                print(mess)
                self.uploadImagesToCloud(completion: {
                    avatarUrl, portfolioUrls, serviceUrls in
                    self.progressLayerView.progressView.progress = 0.6
                    self.serviceVC.serviceInput.serviceDescription = self.serviceVC.serviceDescriptionTf.text

                    self.serviceVC.mapServiceInfo()

                    for element in self.serviceVC.servicePackages {
                        let a = ServicePackageInput(deliverables: element.deliverables, price: element.price, unit: element.unit, quantity: element.quantity, description: element.description, packageType: element.packageType)
                        self.servicePackageInputArray.append(a)
                    }

                    self.serviceVC.serviceInput.servicePackages = self.servicePackageInputArray

                    var portfolioString: [String] = []
                    var services: [String] = []
                    var avatarString: String?

                    if let porfo = portfolioUrls {
                        for element in porfo {
                            portfolioString.append(element.absoluteString)
                        }
                    }

                    if let avatar = avatarUrl {
                        avatarString = avatar.absoluteString
                    }

                    if let service = serviceUrls {
                        for element in service {
                            services.append(element.absoluteString)
                        }
                    }
                    
                    self.serviceVC.serviceInput.servicePhotoUrls = services
                  
                    self.createSellerProfile(porfolioUrls: portfolioString, avatarUrl: avatarString, description: self.personalInfoView.descTextView.text, serviceUrls: services, serviceInfo: self.serviceVC.serviceInput, firstName: "Phuong", lastName: "Nguyen")
                    return
                })
                
            }
            
        }
        if currentStep < 2 {
            currentStep += 1
        }
        
        // Scroll to next page
        pageScrollView.setContentOffset(CGPoint(x: pageScrollView.contentOffset.x, y: 0), animated: true)
        if scrollView2.contentOffset.x < self.view.bounds.width * CGFloat(slides.count-1) {
            let contentOffSet = scrollView2.contentOffset.x + view.bounds.width
            scrollView2.setContentOffset(CGPoint(x: contentOffSet, y: 0), animated: true)
            
        }
        // Update stepper UI
        switch currentStep {
        case 1:
            steps[currentStep].backgroundColor = .orange
            steps[currentStep + 1].tintColor = .orange
        case 2:
            steps[currentStep + 1].backgroundColor = .orange
            steps[currentStep + 2].tintColor = .orange
            nextBtn.setTitle("Finish", for: .normal)
            
        default:
            break;
        }
    }
    
    @objc func prevBtnClick() {
        if currentStep == 0 { return }
        currentStep -= 1
        pageScrollView.setContentOffset(CGPoint(x: pageScrollView.contentOffset.x, y: 0), animated: true)
        
        if scrollView2.contentOffset.x > 0 {
            let contentOffSet = scrollView2.contentOffset.x - view.bounds.width
            scrollView2.setContentOffset(CGPoint(x: contentOffSet, y: 0), animated: true)
        }
        
        switch currentStep {
        
        case 0:
            steps[1].backgroundColor = .lightGray
            steps[2].tintColor = .lightGray
        case 1:
            steps[3].backgroundColor = .lightGray
            steps[4].tintColor = .lightGray
            nextBtn.setTitle("Next Step >>", for: .normal)
        default:
            break;
        }
    }
    
    func createPages() {
        let uislides = [personalInfoView, portfolioView, serviceView]
        for _ in 0..<uislides.count {
            let slide = UIView()
            
            slide.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.height - 170)
            slides.append(slide)
        }
        
        scrollView2.frame = CGRect.zero
        
        scrollView2.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        
        scrollView2.isPagingEnabled = true
        scrollView2.isDirectionalLockEnabled = true
        pageScrollView.addSubview(scrollView2)
        for i in 0 ..< uislides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: scrollView2.contentSize.height)
            scrollView2.addSubview(slides[i])
        }
        scrollView2.translatesAutoresizingMaskIntoConstraints = false
        scroll2constraint = scrollView2.heightAnchor.constraint(equalToConstant: scrollView2.contentSize.height)
        scroll2constraint.isActive = true
        
        NSLayoutConstraint.activate([
            scrollView2.topAnchor.constraint(equalTo: pageScrollView.topAnchor, constant: 60),
            scrollView2.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView2.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView2.heightAnchor.constraint(equalToConstant: scrollView2.contentSize.height)
        ])
    }
    
    //MARK: IMAGE PICKER
    func avatarClick() {
        showImagePicker()
    }
    
    @objc internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImg = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            if currentStep == 0 {
                personalInfoView.avaImgView.contentMode = .scaleAspectFill
                personalInfoView.avaImgView.image = editedImg
                avaimg = editedImg.jpegData(compressionQuality: 0.5)
            } else if currentStep == 1 {
                let newItem = PortfolioItem(imageItem: editedImg, description: nil)
                portfolioItems.append(newItem)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func removeBtnClick(index: Int) {
        portfolioItems.remove(at: index)
    }
    
    func addImage() {
        showImagePicker()
    }
    
}

//MARK: TABLEVIEW DELEGATION
extension BecomeSellerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return portfolioItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PortfolioImageTableViewCell") as! PortfolioImageTableViewCell
        cell.portfolioImageView?.image = portfolioItems[indexPath.row].imageItem
        cell.delegate = self
        cell.index = indexPath.row
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
}



