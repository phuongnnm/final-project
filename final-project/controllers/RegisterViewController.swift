//
//  RegisterViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/18/21.
//

import UIKit

class RegisterViewController: BaseViewController {

    
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var swipeRight = UISwipeGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.emailTf.layer.cornerRadius = 12.0
//        self.emailTf.layer.masksToBounds = true
        swipeRight.direction = .right
        footerView.backgroundColor = GlobalDesign.backgroundColor
        swipeRight.addTarget(self, action: #selector(swipeLeftAction(sender:)))
        scrollView.addGestureRecognizer(swipeRight)
        
        self.footerView = Bundle.main.loadNibNamed("FooterTableViewCell", owner: self, options: nil)?.first as! FooterTableViewCell
    }
    
    @objc func swipeLeftAction(sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .right:

            self.navigationController?.popViewController(animated: true)
            break
        default:
            break
        }
    }
}
