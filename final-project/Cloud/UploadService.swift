//
//  UploadService.swift
//  final-project
//
//  Created by Phuong Nguyen on 23/08/2021.
//

import Foundation
import UIKit
import AWSS3

typealias progressBlock = (_ progress: Double) -> Void //2
typealias completionBlock = (_ response: Any?, _ error: Error?) -> Void //3

class AWSS3Manager {
    static let shared = AWSS3Manager()
    private init () { }
    let bucketName = "phuong810"
    
    func deleteIfExisted(filePath: String) {
        let s3 = AWSS3.default()
        guard let deleteObjectRequest = AWSS3DeleteObjectRequest() else { return }
        deleteObjectRequest.bucket = bucketName
        deleteObjectRequest.key = filePath
        s3.deleteObject(deleteObjectRequest).continueWith { (task:AWSTask) -> AnyObject? in
            if let error = task.error {
                print("Error occurred: \(error)")
                return nil
            }
            print("Deleted successfully.")
            return nil
        }
    }
    
    func uploadImages(images: [UIImage], progress: progressBlock?, completion: completionBlock?) {
        for i in 0..<images.count {
            guard let imageData = images[i].jpegData(compressionQuality: 1.0) else {
                let error = NSError(domain:"", code:402, userInfo:[NSLocalizedDescriptionKey: "invalid image"])
                completion?(nil, error)
                return
            }
            
            let tmpPath = NSTemporaryDirectory() as String
            let fileName: String = ProcessInfo.processInfo.globallyUniqueString + (".jpeg")
            let filePath = tmpPath + fileName
            
            let fileUrl = URL(fileURLWithPath: filePath)
            
            do {
                try imageData.write(to: fileUrl)
                self.uploadfile(fileUrl: fileUrl, fileName: fileName, contentType: "image/jpeg", progress: progress, completion: completion)
            } catch {
                let error = NSError(domain:"", code:402, userInfo:[NSLocalizedDescriptionKey: "invalid image"])
                completion?(nil, error)
            }
            
        }
    }
    
    // Upload image using UIImage object
    func uploadImage(image: UIImage, progress: progressBlock?, completion: completionBlock?) {
        
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            let error = NSError(domain:"", code:402, userInfo:[NSLocalizedDescriptionKey: "invalid image"])
            completion?(nil, error)
            return
        }
        
        let tmpPath = NSTemporaryDirectory() as String
        let fileName: String = ProcessInfo.processInfo.globallyUniqueString + (".jpeg")
        let filePath = tmpPath + fileName
        let fileUrl = URL(fileURLWithPath: filePath)
        
        do {
            try imageData.write(to: fileUrl)
            self.uploadfile(fileUrl: fileUrl, fileName: fileName, contentType: "image/jpeg", progress: progress, completion: completion)
        } catch {
            let error = NSError(domain:"", code:402, userInfo:[NSLocalizedDescriptionKey: "invalid image"])
            completion?(nil, error)
        }
    }
    
    //MARK:- AWS file upload
    
    private func uploadfile(fileUrl: URL, fileName: String, contentType: String, progress: progressBlock?, completion: completionBlock?) {
        // Upload progress block
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.setValue("public-read", forRequestParameter: "x-amz-acl")
        expression.setValue("public-read", forRequestHeader: "x-amz-acl" )
        expression.progressBlock = {(task, awsProgress) in
            guard let uploadProgress = progress else { return }
            DispatchQueue.main.async {
                uploadProgress(awsProgress.fractionCompleted)
            }
        }
        // Completion block
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) in
            DispatchQueue.main.async(execute: {
                if error == nil {
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(self.bucketName).appendingPathComponent(fileName)
//                    print("Uploaded to:\(String(describing: publicURL))")
                    if let completionBlock = completion {
                        completionBlock(publicURL?.absoluteString, nil)
                    }
                } else {
                    if let completionBlock = completion {
                        completionBlock(nil, error)
                    }
                }
            })
        }
        
        let awsTransferUtility = AWSS3TransferUtility.default()
        awsTransferUtility.uploadFile(fileUrl, bucket: bucketName, key: fileName, contentType: contentType, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            if let error = task.error {
                print("error is: \(error.localizedDescription)")
            }
            if let _ = task.result {
            }
            return nil
        }
    }
}
