//
//  RRAWSRxUpload.swift
//  RRAWSRxUpload
//
//  Created by Rahul Mayani on 20/12/19.
//  Copyright © 2019 RR. All rights reserved.
//

import Foundation
import UIKit
import AWSS3
import RxCocoa
import RxSwift

public struct AWSBucket {
    static let bucketName   = "phuong810"
    static let contentType  = "image/jpeg"
    static let poolId       = "ap-southeast-1:7f5b7eae-c6f9-4c7c-bb0b-933a33e0b441"
}

public struct RRAWSRxUpload: ObservableType {

    public typealias Element = AWSImageKey
       
    var data: AWSImageData
    
    var index: Int = 0
    
    var userId: String!
    
    public func subscribe<Observer>(_ observer: Observer) -> Disposable where Observer : ObserverType, RRAWSRxUpload.Element == Observer.Element {
        
        if !AppNetworkReachability.isInternetAvailable() {
            let errorTemp = NSError(domain:"", code: StatusCode.noInternetConnection.rawValue, userInfo:nil)
            observer.onError(errorTemp)
        }
        
        let data = self.data.image.reduceImageFileSize()
        let code = Utilitites.generateCode()
        let key = self.userId + "/" + self.data.type.title + "/" + "\(code).jpeg"
        
        var uploadTask : AWSS3TransferUtilityUploadTask?
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        
        let completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                uploadTask = task
                if let error = error {
                    observer.onError(error)
                }else{
                    observer.onNext({
                        return AWSImageKey(type: self.data.type, key: key)
                    }())
                    observer.onCompleted()
                }
            })
        }
        
        AWSS3TransferUtility.default().uploadData(data, bucket: AWSBucket.bucketName, key: key, contentType: AWSBucket.contentType, expression: expression, completionHandler: completionHandler)
        
        return Disposables.create { uploadTask?.cancel() }
    }
    
    public static func upload(data: AWSImageData) -> Observable<Element> {
        return Observable.deferred {
            return RRAWSRxUpload.init(data: data).asObservable()
        }
    }

    public static func upload(dataList: [AWSImageData], userId: String) -> Observable<[Element]> {
        return Observable.deferred {
            return Observable.from(dataList).enumerated().flatMap { (index, data) -> Observable<Element> in
                return RRAWSRxUpload.init(data: data, index: index, userId: userId).asObservable()
            }
        }.toArray().asObservable()
    }
}

extension UIImage {
    
    func reduceImageFileSize() -> Data {
        
        var compression = 0.1
        let maxCompression = 0.1
        let maxFileSize = 100*924
        
        var data = self.jpegData(compressionQuality: CGFloat(compression))!
        
        while (data.count > maxFileSize) && (compression > maxCompression) {
            
            compression -= 0.1
            data = self.jpegData(compressionQuality: CGFloat(compression))!
        }
        
        return data
    }
}


