//
//  BookingViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/6/21.
//

import UIKit

class BookingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var tabCollectionView: UICollectionView!
    @IBOutlet weak var bookingTableView: UITableView!
    var displayInfo: [Booking] = [] {
        didSet {
            bookingTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        bookingTableView.delegate = self
        bookingTableView.dataSource = self
        bookingTableView.register(UINib(nibName: "BookingHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "BookingHistoryTableViewCell")
        let layout = tabCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: view.frame.width/2, height: 50)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        tabCollectionView.delegate = self
        tabCollectionView.dataSource = self
        tabCollectionView.selectItem(at: [0,0], animated: true, scrollPosition: [])
        fetchUsersBookings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userInfo = SQLiteCommands.getCurrentUserInfo()
        if userInfo == nil {
            hideNavigationBar(animated: animated)
            let vc = storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchUsersBookings()
    }
    
    func fetchUsersBookings() {
        let userInfo = SQLiteCommands.getCurrentUserInfo()
        if let userInfo = userInfo {
            Network.shared.apollo.fetch(query: GetUsersBookingsQuery(userId: userInfo.id), cachePolicy: .returnCacheDataAndFetch) {
                res in
                switch (res) {
                case .success(let response):
                    if let datas = response.data?.getUsersBookings {
                        self.displayInfo = datas.compactMap {
                            element in
                            let createdAt = element!.resultMap["createdAt"] as? String
                            let id = element!.resultMap["id"] as? String
                            let code = element!.resultMap["code"] as? String
                            let description = element!.resultMap["description"] as? String
                            let price = element!.resultMap["price"] as? Double
                            let seller = element!.resultMap["seller"] as? [String: Any]
                            let status = element!.resultMap["status"] as? Status
                            var sellerInfo: Seller?
                            if let seller = seller {
                                let sId = seller["id"] as? String
                                let sName = seller["fullName"] as? String
                                let avaUrl = seller["avatarUrl"] as? String
                                sellerInfo = Seller(id: sId, portfolioUrls: nil, avatarUrl: avaUrl, description: nil, fullName: sName, services: nil)
                            }
                            var cName: String?
                            if let package = element!.resultMap["package"] as? [String: Any] {
                                if let service = package["service"] as? [String: Any] {
                                    if let category = service["category"] as? [String: Any] {
                                        if let catName = category["categoryName"] as? String
                                        {
                                            cName = catName
                                        }
                                        
                                    }
                                }
                            }
                           
                            return Booking(id: id, code: code, price: price, description: description, seller: sellerInfo,  status: status, categoryName: cName, createdAt: createdAt)
                        }
                    }
                    break;
                case .failure(let err):
                    break;
                }
                
            }
        }
        
    }
}

// MARK: TABLE VIEW
extension BookingViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingHistoryTableViewCell") as! BookingHistoryTableViewCell
        
        if let price = displayInfo[indexPath.row].price, let status = displayInfo[indexPath.row].status, let catName = displayInfo[indexPath.row].catName,
           let createdAt = displayInfo[indexPath.row].createdAt {
            cell.displayStatus(status: status)
            let dprice = Utilitites.formatPrice(price: price)
            cell.priceLabel.text = "\(dprice) VND"
            cell.serviceDescLabel.text = "Job: \(catName)"
            cell.displayDate(dateString: createdAt)
        }
        
        if let ava = displayInfo[indexPath.row].seller?.avatarUrl {
            if let url = URL(string: ava) {
                NukeLoad.loadContent(url: url, imageView: cell.sellerAvaImgView)
            }
        }
        
        cell.sellerNameLabel.text = displayInfo[indexPath.row].seller?.fullName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "BookingInfoViewController") as! BookingInfoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: COLLECTION VIEW
extension BookingViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabbarcell", for: indexPath) as! TabBarCollectionViewCell
        if indexPath.row == 0 {
            cell.tabLabel.text = "Your Orders"
        } else {
            cell.tabLabel.text = "Your Clients"
        }
        
        return cell
    }
}
