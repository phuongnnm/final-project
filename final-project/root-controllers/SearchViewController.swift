//
//  SearchViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit



struct Model {
    
}

class SearchViewController: BaseViewController, UITabBarDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    var tabIndex = 0
    var searchKey: String?
    var data = [Bool]()
    var displayInfo:[Seller] = [] {
        didSet {
            resultTableView.reloadData()
        }
    }
    
    var serviceDatas: [ServiceModel] = [] {
        didSet {
            resultTableView.reloadData()
        }
    }
    
    @IBOutlet weak var resultTableView: UITableView!
    @IBAction func searchBtn(_ sender: UIButton) {
    }
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.delegate = self
        self.searchTextField.delegate = self
        self.resultTableView.delegate = self
        self.resultTableView.dataSource = self
        
        tabBar.selectedItem = tabBar.items?.first
        tabIndex = tabBar.items!.startIndex
        
        searchTextField.layer.cornerRadius = 12.0
        searchTextField.layer.masksToBounds = true
        searchTextField.layer.borderWidth = 0.7
        searchTextField.layer.borderColor = UIColor.darkGray.cgColor
        searchTextField.placeholder = "Try entering \"Designer\""
        searchKey = nil
    }
    
    //MARK: TEXTFIELD AND TABBAR DELEGATION
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchKey = searchTextField?.text ?? ""
        data = []
        switch tabIndex {
        case 0:
            if let searchKey = searchKey {
                searchServiceByKeyword(keyword: searchKey)
            }
            break;
        case 1:
            if let searchKey = searchKey {
                searchSellerByKeyWord(keyword: searchKey)
            }
            break;
        default:
            break;
        }
        
        resultTableView.reloadData()
        return true
    }
    
    func searchSellerByKeyWord(keyword: String) {
        Network.shared.apollo.fetch(query: SearchSellersByKeywordQuery(keyWord: keyword)) {
            res in
            switch res {
            case .success(let response):
                if let datas = response.data?.searchSellersByKeyword {
                    self.displayInfo = datas.compactMap {
                        element in
                        let id = element!.resultMap["id"] as? String
                        let avaUrl = element!.resultMap["avatarUrl"] as? String
                        let fullName = element!.resultMap["fullName"] as? String
                        var userId: String?
                        if let user = element!.resultMap["user"] as? [String: Any] {
                            userId = user["id"] as? String
                        }
                        
                        return Seller(id: id, portfolioUrls: nil, avatarUrl: avaUrl, description: nil, fullName: fullName, services: nil, userId: userId)
                    }
                }
                break
            case .failure(let err):
                print(err)
            }
        }
    }
    
    func searchServiceByKeyword(keyword: String) {
        Network.shared.apollo.fetch(query: SearchServiceByKeywordQuery(keyWord: keyword), cachePolicy: .returnCacheDataAndFetch) {
            res in
            switch res {
            case .success(let response):
                if let datas = response.data?.searchServiceByKeyword {
                    self.serviceDatas = datas.map { element in
                        let id = element!.resultMap["id"] as? String
                        let serviceDescription = element!.resultMap["serviceDescription"] as? String
                        let servicePhotoUrls = element!.resultMap["servicePhotoUrls"] as? [String]
                        let packages = element!.resultMap["packages"] as? [[String: Any]]
                        var servicePackages: [PackageModel] = []
                        
                        if let packages = packages {
                            for package in packages {
                                if let price = package["price"] as? Double,
                                let pid = package["id"] as? String,
                                let type = package["packageType"] as? PackageType {
                                    let packageData = PackageModel(id: pid, price: Float(price), type: type, unit: nil, quantity: nil, description: nil, deliverables: nil)
                                    servicePackages.append(packageData)
                                }
                            }
                        }
                        var sellerData: Seller?
                        
                        if let seller = element!.resultMap["seller"] as? [String: Any] {
                            let selId = seller["id"] as? String
                            let selName = seller["fullName"] as? String
                            let avaUrl = seller["avatarUrl"] as? String
                            sellerData = Seller(id: selId, portfolioUrls: nil, avatarUrl: avaUrl, description: nil, fullName: selName, services: nil, userId: nil)
                        }
                        
                        return ServiceModel(id: id, serviceDescription: serviceDescription, servicePhotoUrls: servicePhotoUrls, packages: servicePackages, seller: sellerData)
                    }
                }
            case .failure(let err):
                print("Loi ne \(err)")
            }
        }
    }
    
    //MARK: TABBAR DELEGATION
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let index = tabBar.items?.firstIndex(of: item) {
            tabIndex = index
            if tabIndex == 0 {
                if let kw = searchTextField.text {
                    searchServiceByKeyword(keyword: kw)
                }
            } else if tabIndex == 1 {
                if let kw = searchTextField.text {
                    searchSellerByKeyWord(keyword: kw)
                }
            }

//            resultTableView.reloadData()
        }
    }
    
    //MARK: TABLE VIEW DELEGATION
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if serviceDatas.count > 0 && tabIndex == 0 {
            return serviceDatas.count
        }
        if displayInfo.count > 0 && tabIndex == 1 {
            return displayInfo.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tabIndex == 0 && !serviceDatas.isEmpty) {
            let cell = resultTableView.dequeueReusableCell(withIdentifier: "jobcell") as! ServiceTableViewCell
            cell.serviceName.text = serviceDatas[indexPath.row].serviceDescription
            
            if let package = serviceDatas[indexPath.row].packages {
                if !package.isEmpty {
                    if let price = package[0].price {
                        let dPrice = Utilitites.formatPrice(price: Double(price))
                        cell.serviceMinPrice.text = dPrice + " VND"
                    }
                }
            }
            
            cell.userNameLabel.text = serviceDatas[indexPath.row].seller?.fullName
            if let url = URL(string: serviceDatas[indexPath.row].seller?.avatarUrl ?? "") {
                NukeLoad.loadContent(url: url, imageView: cell.userAvaImgView)
            }
            
            if let serviceUrl = serviceDatas[indexPath.row].servicePhotoUrls {
                if serviceUrl.count > 0 {
                    if let url = URL(string: serviceUrl[0]) {
                        NukeLoad.loadContent(url: url, imageView: cell.avaImage)
                    }
                } else {
                    cell.avaImage.image = UIImage(named: "loading2")
                }
            }
            
            return cell
        }
        
        if(tabIndex == 1 && !displayInfo.isEmpty) {
            let cell = resultTableView.dequeueReusableCell(withIdentifier: "providercell") as! ProviderTableViewCell
            cell.providerNameLabel.text = displayInfo[indexPath.row].fullName
            if let avaUrl = displayInfo[indexPath.row].avatarUrl {
                
                if let url = URL(string: avaUrl) {
                    NukeLoad.loadContent(url: url, imageView: cell.avaImage) 
                } else {
                    cell.avaImage.image = UIImage(named: "loading2")
                }
            }
            
            return cell
        }
        
        
        let cell = self.resultTableView.dequeueReusableCell(withIdentifier: "noresultcell") as! NoResultTableViewCell
        if let searchKey = self.searchKey {
            if tabIndex == 0 {
                cell.noResultLabel.text = "There is 0 service for keyword \"\(searchKey)\""
            } else if tabIndex == 1 {
                cell.noResultLabel.text = "There is 0 seller named \"\(searchKey)\""
            }
        } else {
            cell.noResultLabel.text = "Please enter a keyword to discover"
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tabIndex == 0 {
            guard let vc = storyboard?.instantiateViewController(identifier: "ServiceDetailViewController") as? ServiceDetailViewController else {return}
            if let sId = serviceDatas[indexPath.row].id {
                vc.serviceId = sId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let vc = storyboard?.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
            vc.userId = displayInfo[indexPath.row].userId!
            vc.isProfileOwner = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tabIndex == 0) {
            return 200
        } else {
            return 180
        }
    }
}
