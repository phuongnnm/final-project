//
//  ProfileViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit
import Nuke
import RxCocoa
import RxSwift
import AWSS3
import PromiseKit

protocol ProfileControllerDelegate {
    func resetProfileView(view: UserProfileView)
}

class ProfileViewController: BaseViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    
    //MARK: - UICollectionViewDelegateFlowLayout
    var type: AWSImageType!
    var userData: User?
    let rxbag = DisposeBag()
    let noSellerView = Bundle.main.loadNibNamed("NoSellerProfileView", owner: self, options: nil)?.first as! NoSellerProfileView
    var deletgate: ProfileControllerDelegate?
    let service = Bundle.main.loadNibNamed("UserServiceView", owner: self, options: nil)?.first as! UserServiceView
    var tabIndex = 0
    var isProfileOwner = true
    var userId: String = ""
    var currentPage = 0
    var slides:[UIView] = []
    var photoURLs: [URL] = [] {
        didSet {
            profile.portfolioCollection.reloadData()
        }
    }
    
    var sellerInfo: Seller! {
        didSet {
            service.serviceTableView.reloadData()
        }
    }
    let profile = Bundle.main.loadNibNamed("UserProfileView", owner: self, options: nil)?.first as! UserProfileView
    let addNewServiceBtn = UIButton(frame: CGRect.zero)
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tabBarCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        profile.delegate = self
        self.view.backgroundColor = .white
        scrollView.delegate = self
        tabBarCollectionView.delegate = self
        tabBarCollectionView.selectItem(at: [0,0], animated: true, scrollPosition: [])
        scrollView.isDirectionalLockEnabled = true
        slides = createViews()
        setupSlideScrollView(slides: slides)
        createView2()
        let width = view.frame.size.width / 2
        let layout = tabBarCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: 50)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewWillAppear(false)
    }
    
    func fetchSellerProfile(completion: @escaping ([ServiceModel]) -> Void) {
        var id = ""
        let userInfo = SQLiteCommands.getCurrentUserInfo()
        
        if isProfileOwner == true && userInfo == nil {
            let vc = storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
        
        userData = userInfo
        
        if isProfileOwner == false {
            id = self.userId
            profile.editBtnView.isHidden = true
            profile.portfolioBtnView.isHidden = true
            profile.avaBtnView.isHidden = true
            addNewServiceBtn.isHidden = true
        } else {
            id = userInfo!.id
        }
        
        Network.shared.apollo.fetch(query: GetSellerByUseridQuery(id: id), cachePolicy: .returnCacheDataAndFetch) {
            [weak self] ret in
            guard let self = self else {return}
            switch(ret) {
            case .success(let response):
                if let data = response.data?.getSellerByUserid {
                    self.noSellerView.isHidden = true
                    let id = data.resultMap["id"] as? String
                    let fullName = data.resultMap["fullName"] as? String
                    let description = data.resultMap["description"] as? String
                    let avatarUrl = data.resultMap["avatarUrl"] as? String
                    let portfolioUrls = data.resultMap["portfolioPhotoUrls"] as? [String]
                    let services = data.resultMap["services"] as! [[String: Any]]
                    
                    var serviceArr: [ServiceModel] = []
                    
                    for service in services {
                        let sId = service["id"] as? String
                        let sDesc = service["serviceDescription"] as? String
                        let sPhotoUrls = service["servicePhotoUrls"] as? [String]
                        let sPackages = service["packages"] as? [[String: Any]]
                        var servicePackages: [PackageModel] = []
                        
                        if let sPackages = sPackages {
                            for package in sPackages {
                                if let price = package["price"] as? Double {
                                    print(price)
                                    let packageData = PackageModel(id: nil, price: Float(price), type: nil, unit: nil, quantity: nil, description: nil, deliverables: nil)
                                    servicePackages.append(packageData)
                                }
                            }
                        }
                        
                        let serv = ServiceModel(id: sId, serviceDescription: sDesc, servicePhotoUrls: sPhotoUrls, packages: servicePackages, seller: nil)
                        
                        serviceArr.append(serv)
                    }
                    
                    if id != nil {
                        self.sellerInfo = Seller(id: id, portfolioUrls: portfolioUrls, avatarUrl: avatarUrl, description: description, fullName: fullName, services: serviceArr)
                        completion(serviceArr)
                    }
                    
                    if let avaUrl = avatarUrl {
                        guard let url = URL(string: avaUrl) else {return}
                        NukeLoad.loadContent(url: url, imageView: self.profile.avatarImgView)
                    }
                    
                } else {
                    print("LOADFAILNE")
                    self.view.addSubview(self.noSellerView)
                    self.noSellerView.isHidden = false
                    self.view.bringSubviewToFront(self.noSellerView)
                    
                }
            case .failure(let err):
                    print(err)
            }
        }
    }
    
    func createViews() -> [UIView] {
        let s1 = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.frame.height))
        
        let s2 = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.frame.height))
        
        return [s1, s2]
    }
    
    func createView2() {
        profile.translatesAutoresizingMaskIntoConstraints = false
        slides[0].addSubview(profile)
        profile.setDelegateAndDataSourceForCollectionView(delegate: self, dataSource: self)
        NSLayoutConstraint.activate([
            profile.topAnchor.constraint(equalTo: slides[0].topAnchor, constant: 20),
            profile.bottomAnchor.constraint(equalTo: slides[0].bottomAnchor),
            profile.trailingAnchor.constraint(equalTo: slides[0].trailingAnchor),
            profile.leadingAnchor.constraint(equalTo: slides[0].leadingAnchor),
        ])
        
        addNewServiceBtn.setTitle(" Add a new service", for: .normal)
        addNewServiceBtn.setImage(UIImage(systemName: "plus.circle"), for: .normal)
        addNewServiceBtn.contentHorizontalAlignment = .left
        addNewServiceBtn.titleLabel?.font = UIFont(name: "PingFang SC", size: 20)
        addNewServiceBtn.tintColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        addNewServiceBtn.setTitleColor(#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), for: .normal)
        addNewServiceBtn.addTarget(self, action: #selector(addNewServiceClick), for: .touchUpInside)
        addNewServiceBtn.translatesAutoresizingMaskIntoConstraints = false
        slides[1].addSubview(addNewServiceBtn)
        NSLayoutConstraint.activate([
            addNewServiceBtn.topAnchor.constraint(equalTo: slides[1].topAnchor),
            addNewServiceBtn.leadingAnchor.constraint(equalTo: slides[1].leadingAnchor, constant: 20),
            addNewServiceBtn.trailingAnchor.constraint(equalTo: slides[1].trailingAnchor, constant: -20),
            addNewServiceBtn.heightAnchor.constraint(equalToConstant: 35)
        ])
        
        
        service.setDelegateAndDataSourceTableView(delegate: self, dataSource: self)
        service.translatesAutoresizingMaskIntoConstraints = false
        slides[1].addSubview(service)
        
        NSLayoutConstraint.activate([
            service.topAnchor.constraint(equalTo: addNewServiceBtn.bottomAnchor, constant: 10),
            service.bottomAnchor.constraint(equalTo: slides[1].bottomAnchor),
            service.trailingAnchor.constraint(equalTo: slides[1].trailingAnchor),
            service.leadingAnchor.constraint(equalTo: slides[1].leadingAnchor),
        ])
    }
    
    func setupSlideScrollView(slides: [UIView]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: 1100)
        scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: 1100)
            scrollView.addSubview(slides[i])
        }
    }
    
    @objc func addNewServiceClick() {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ServiceCreateViewController") as! ServiceCreateViewController
        vc.isCreateNew = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 1) {
            return 2
        } else {
            return photoURLs.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            if(indexPath.row == 0) {
                scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y), animated: true)
            } else {
                scrollView.setContentOffset(CGPoint(x: view.bounds.width, y: scrollView.contentOffset.y), animated: true)
            }
        }
        
        if collectionView.tag == 0 {
            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "PortfolioDetailViewController") as! PortfolioDetailViewController
            vc.url = photoURLs[indexPath.row]
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            guard let cell = tabBarCollectionView.dequeueReusableCell(withReuseIdentifier: "tabbarcell", for: indexPath) as? TabBarCollectionViewCell else { return UICollectionViewCell()}
            if indexPath.row == 0 {
                cell.tabLabel.text = "Profile"
            } else {
                cell.tabLabel.text = "Services"
                cell.isSelected = false
            }
            return cell
        } else  {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PortfolioCollectionViewCell", for: indexPath) as? PortfolioCollectionViewCell else { return UICollectionViewCell() }
            
            var resizedImageProcessors: [ImageProcessing] {
              let imageSize = CGSize(width: 300, height: 300)
              return [ImageProcessors.Resize(size: imageSize, contentMode: .aspectFill)]
            }
            
            let url = photoURLs[indexPath.row]
            let options = ImageLoadingOptions(
                placeholder: UIImage(named: "loading2"),
                transition: .fadeIn(duration: 0.5)
            )
            
            let request = ImageRequest(
              url: url,
              processors: resizedImageProcessors)

            Nuke.loadImage(with: request, options: options, into: cell.portfolioImg)
            return cell
        }
        
    }
    
    // MARK: ScrollView delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.tag == 1) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            if (
                currentPage == 0 && scrollView.contentOffset.x/view.frame.width > 0.5 ||
                    currentPage == 1 && scrollView.contentOffset.x/view.frame.width < 0.5
            ) {
                tabBarCollectionView.selectItem(at: [0,Int(pageIndex)], animated: true, scrollPosition: [])
                currentPage = Int(pageIndex)
            }
        }
    }
    
    //MARK: TABLEVIEW delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let seller = sellerInfo {
            return seller.services!.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesItemCell") as! ServicesItemCell
        cell.descriptionLabel.text = sellerInfo.services?[indexPath.row].serviceDescription
        cell.sellerNameLabel.text = sellerInfo.fullName
        if let packages = sellerInfo.services![indexPath.row].packages {
            print(packages)
            if !packages.isEmpty {
                cell.serviceMinPriceLabel.text = "\(Utilitites.formatPrice(price: Double(packages[0].price!))) VND"
            }
        }
        if let avaUrl = sellerInfo.avatarUrl {
            if let url = URL(string: avaUrl) {
                NukeLoad.loadContent(url: url, imageView: cell.providerAva)
            }
        }
        
        if let service = sellerInfo.services?[indexPath.row] {
            if let urls = service.servicePhotoUrls {
                if !urls.isEmpty {
                    if let url = URL(string: urls[0]) {
                        NukeLoad.loadContent(url: url, imageView: cell.productImageView)
                    }
                }
            }
        }
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isProfileOwner == true {
            hideNavigationBar(animated: animated)
        } else {
            showNavigationBar(animated: animated)
        }
        
        fetchSellerProfile(completion: { [weak self] ret in
                            guard let self = self else {return}
                            if let urls = self.sellerInfo.portfolioUrls {
                                self.photoURLs = urls.compactMap{ URL(string: $0) }
                                self.profile.descriptionLabel.text = self.sellerInfo.description
                                self.profile.fullNameLabel.text = self.sellerInfo.fullName
                            }})
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationBar(animated: animated)
    }
}

enum UploadType {
    case avatar
    case back
    case left
    case right
}
extension ProfileViewController: SellerProfileProtocol, DescPopupDelegate {
     
    func updateDesc(description: String) {
        profile.descriptionLabel.text = description
    }
    
    func portfolioClick() {
        type = .portfolio
        showImagePicker()
    }
    
    func avatarClick() {
        type = .avatar
        showImagePicker()
    }
    
    func editBtnClick() {
        var popUpWindow: PopUpWindow!
        popUpWindow = PopUpWindow(title: "Description", buttontext: "Save", sellerId: sellerInfo.id!)
        popUpWindow.delegate = self
        self.present(popUpWindow, animated: true, completion: nil)
    }
    
    @objc internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImg = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            if (type == .avatar) {
                let data = AWSImageData(type: .avatar, image: editedImg)
                updateAvatar(avaData: data)
            } else if type == .portfolio {
                print("PORTFOLIO CLICK")
                let data = AWSImageData(type: .portfolio, image: editedImg)
                addNewPortfolioImage(image: data)
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func addNewPortfolioImage(image: AWSImageData) {
        self.uploadImagesToCloud(avaData: image, completion: {
                                [weak self] avatarUrl in
            guard let self = self else {return}
            
            if let publicUrl = avatarUrl {
                self.photoURLs.append(publicUrl)
                let pStrings = self.photoURLs.compactMap {$0.absoluteString}
                Network.shared.apollo.perform(mutation: UpdateSellerProfileMutation(id: self.sellerInfo.id!, portfolioPhotoUrls: pStrings)) {
                    res in
                    switch(res) {
                    case .success:
                        Utilitites.displayNoActionAlert(title: "Success", message: "Uploaded your portfolio image", viewController: self)
                        break;
                    case .failure:
                        Utilitites.displayNoActionAlert(title: "Error", message: "Cannot upload image", viewController: self)
                        break;
                    }
                }
            } else {
                Utilitites.displayNoActionAlert(title: "Error", message: "Cannot upload image", viewController: self)
            }
            
        })
    }
    
    
    
    func uploadImagesToCloud(avaData: AWSImageData, completion: @escaping(_: URL?) -> Void) {
        
        var avatarUrl: URL?
        guard let uId = userData?.id else {return}
        
        RRAWSRxUpload.upload(dataList: [avaData], userId: uId)
            .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                let url = AWSS3.default().configuration.endpoint.url
                for i in 0..<response.count {
                    let publicURL = url?.appendingPathComponent(AWSBucket.bucketName).appendingPathComponent(response[i].key)
                    switch response[i].type {
                    case .avatar:
                        avatarUrl = publicURL
                        break;
                    case .portfolio:
                        avatarUrl = publicURL
                        break;
                    case .service:
                        break;
                    }
                }
                completion(avatarUrl)
            }, onError: { error in
                print(error.localizedDescription)
            }).disposed(by: rxbag)
    }
    
    func updateAvatar(avaData: AWSImageData) {
        firstly {
            BecomeSellerViewController.deleteAWSFolder(userId: userData!.id+"/avatar")
        }.done {
            _ in
            self.uploadImagesToCloud(avaData: avaData, completion: {
                                    [weak self] avatarUrl in
                guard let self = self else {return}
                
                if let avatarUrl = avatarUrl {
                    print(avatarUrl)
                    Network.shared.apollo.perform(mutation: UpdateSellerProfileMutation(id: self.sellerInfo.id!, avatarUrl: avatarUrl.absoluteString)) {
                        res in
                        switch(res) {
                        case .success(let response):
                            
                            if let data = response.data?.updateSellerProfile {
                                let avaUrl1 = data.resultMap["avatarUrl"] as? String
                                print(avaUrl1)
                                if let avaUrl = data.resultMap["avatarUrl"] as? String {
                                    let url1 = URL(string: avaUrl)
                                    print(url1)
                                    guard let url = URL(string: avaUrl) else {return}
                                    
                                    NukeLoad.loadContent(url: url, imageView: self.profile.avatarImgView)
                                }
                            }
                        case .failure:
                            break;
                        }
                    }
                } else {
                    let alert = UIAlertController(title: "Error", message: "Cannot upload image", preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                    
                    let when = DispatchTime.now() + 2.0
                    DispatchQueue.main.asyncAfter(deadline: when){
                        alert.dismiss(animated: true, completion: nil)
                    }
                }
                
            })
        }
    }
    
}
