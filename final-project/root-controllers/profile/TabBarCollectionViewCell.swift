//
//  TabBarCollectionViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/19/21.
//

import UIKit

class TabBarCollectionViewCell: UICollectionViewCell {
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                underlineView.backgroundColor = .orange
            } else {
                underlineView.backgroundColor = .white
            }
        }
    }
    
    var tabLabel = UILabel()
    var underlineView = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.isUserInteractionEnabled = true
    }
    
    func setUpView() {
        contentView.backgroundColor = .white
        underlineView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: CGFloat(4))
        tabLabel.textColor = .darkGray
        contentView.addSubview(tabLabel)
        contentView.addSubview(underlineView)
        tabLabel.translatesAutoresizingMaskIntoConstraints = false
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tabLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            tabLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            tabLabel.heightAnchor.constraint(equalToConstant: CGFloat(25)),
            
            underlineView.heightAnchor.constraint(equalToConstant: CGFloat(4)),
            underlineView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            underlineView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            underlineView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
