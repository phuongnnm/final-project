//
//  HeaderTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit

protocol HeaderCellDelegate: class {
    func tabDiscoverBtn(_ button: UIButton)
}

class HeaderCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    weak var delegate: HeaderCellDelegate?
    

    @IBAction func tapDiscover(_ sender: UIButton) {
        delegate?.tabDiscoverBtn(sender);
    }
    
}
