//
//  TestingTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/11/21.
//

import UIKit

protocol TestCellDelegate {
    func showFloatingPanel()
}

class TestingTableViewCell: UITableViewCell {
    var delegate: TestCellDelegate?
    @IBAction func button(_ sender: UIButton) {
        delegate?.showFloatingPanel()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
