//
//  SettingViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit

class SettingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var topContainerView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    func config() {
        self.avatarImage.makeRounded();
        self.topContainerView?.backgroundColor = UIColor(red: 0.99, green: 0.80, blue: 0.43, alpha: 1.00)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = GlobalDesign.backgroundColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 8) {
            let cell = Bundle.main.loadNibNamed("FooterTableViewCell", owner: self, options: nil)?.first as! FooterTableViewCell
            
            return cell
        } else {
            let cell = Bundle.main.loadNibNamed("HomeTableViewCell", owner: self, options: nil)?.first as! HomeTableViewCell
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 8) {
            return 150
        } else {
            return 100
        }
    }
}
