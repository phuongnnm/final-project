//
//  HomeViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit
import FloatingPanel

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, HeaderCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource, TestCellDelegate, FloatingPanelControllerDelegate, UITabBarControllerDelegate {
    
    let floatingPanel = FloatingPanelController()
    var isHidden: Bool = false
    let userInfo = SQLiteCommands.getCurrentUserInfo()
    let token = UserDefaults.standard.value(forKey: "accessToken")
    
    @IBOutlet weak var homeTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SQLiteCommands.createTable()
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        self.homeTableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        self.homeTableView.backgroundColor = GlobalDesign.backgroundColor
        self.addBecomePartnerButton(color: UIColor.white)
        
        //floating panel
        let appearance = SurfaceAppearance()
        floatingPanel.delegate = self
        floatingPanel.isRemovalInteractionEnabled = true
        appearance.cornerRadius = 20.0
        appearance.backgroundColor = .clear
        floatingPanel.surfaceView.appearance = appearance
        self.tabBarController?.delegate = self
    }
    
    
    @IBAction func logoutBtnClick(_ sender: UIButton) {
        UserManager.shared.logout()
        
        let alert = UIAlertController(title: "Sign out", message: "You are now logged out", preferredStyle: .alert)
        
        self.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
//            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: FLOATING PANEL
    
    func showFloatingPanel() {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "BookingPanelViewController") as! BookingPanelViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tabDiscoverBtn(_ button: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
    //MARK: TABLE VIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = Bundle.main.loadNibNamed("HeaderCell", owner: self, options: nil)?.first as! HeaderCell
            cell.cellView?.backgroundColor = GlobalDesign.backgroundColor
            cell.delegate = self
            return cell
        }
        else if (indexPath.row == 1) {
            guard let cell = homeTableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as? HomeTableViewCell
            else {
                return UITableViewCell()
            }
            return cell
        }
        else if (indexPath.row == 2) {
            guard let cell = homeTableView.dequeueReusableCell(withIdentifier: "TestingTableViewCell") as? TestingTableViewCell else { return UITableViewCell()}
            cell.delegate = self
            
            return cell
        } else {
            let cell = Bundle.main.loadNibNamed("FooterTableViewCell", owner: self, options: nil)?.first as! FooterTableViewCell
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            return 190
        }
        if (indexPath.row == 1) {
            return 170
        }
        if (indexPath.row == 2) {
            return 400
        }
        
        else { return 100 }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? HomeTableViewCell else { return }
        cell.setDelegateAndDataSourceCollectionView(delegate: self, dataSource: self, forRow: indexPath.row)
    }
    
    //MARK: COLLECTION VIEW
    let jobImages = ["designer", "plumber2", "cleaner", "translator", "uxui"]
    let jobTitles = ["Graphic Designer", "Plumber", "House Cleaner", "Translator", "UX/UI Designer"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as? ServiceCollectionViewCell else { return UICollectionViewCell() }
        cell.jobImageView.image = UIImage(named: jobImages[indexPath.row])
        cell.jobTitleLabel.text = jobTitles[indexPath.row]
        return cell
    }
    
    
    
    @objc func becomePartnerClick(sender : UIButton) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        if let userInfo = userInfo, let token = token {
            let vc = sb.instantiateViewController(withIdentifier: "BecomeSellerViewController") as! BecomeSellerViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = sb.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    let becomePartnerBtn: UIButton = UIButton()
    
    //MARK: switch between become partner and view your orders
    
    func addBecomePartnerButton(color: UIColor) {
        becomePartnerBtn.setTitleColor(color, for: .normal)
        let image = UIImage(systemName: "globe");
        becomePartnerBtn.setImage(image, for: .normal)
        becomePartnerBtn.setTitle(" Become a seller", for: .normal);
        becomePartnerBtn.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        becomePartnerBtn.sizeToFit()
        becomePartnerBtn.addTarget(self, action: #selector (becomePartnerClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: becomePartnerBtn)
        self.navigationItem.rightBarButtonItem = barButton
    }
}


