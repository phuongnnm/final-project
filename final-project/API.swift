// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct UserPayloadInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - email
  ///   - password
  public init(email: Swift.Optional<String?> = nil, password: Swift.Optional<String?> = nil) {
    graphQLMap = ["email": email, "password": password]
  }

  public var email: Swift.Optional<String?> {
    get {
      return graphQLMap["email"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var password: Swift.Optional<String?> {
    get {
      return graphQLMap["password"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "password")
    }
  }
}

public enum Role: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case user
  case seller
  case admin
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "USER": self = .user
      case "SELLER": self = .seller
      case "ADMIN": self = .admin
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .user: return "USER"
      case .seller: return "SELLER"
      case .admin: return "ADMIN"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: Role, rhs: Role) -> Bool {
    switch (lhs, rhs) {
      case (.user, .user): return true
      case (.seller, .seller): return true
      case (.admin, .admin): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [Role] {
    return [
      .user,
      .seller,
      .admin,
    ]
  }
}

public struct SellerInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - firstName
  ///   - lastName
  ///   - description
  ///   - portfolioPhotoUrls
  ///   - service
  ///   - avatarUrl
  public init(firstName: Swift.Optional<String?> = nil, lastName: Swift.Optional<String?> = nil, description: Swift.Optional<String?> = nil, portfolioPhotoUrls: Swift.Optional<[String?]?> = nil, service: Swift.Optional<ServiceInput?> = nil, avatarUrl: Swift.Optional<String?> = nil) {
    graphQLMap = ["firstName": firstName, "lastName": lastName, "description": description, "portfolioPhotoUrls": portfolioPhotoUrls, "service": service, "avatarUrl": avatarUrl]
  }

  public var firstName: Swift.Optional<String?> {
    get {
      return graphQLMap["firstName"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "firstName")
    }
  }

  public var lastName: Swift.Optional<String?> {
    get {
      return graphQLMap["lastName"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lastName")
    }
  }

  public var description: Swift.Optional<String?> {
    get {
      return graphQLMap["description"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var portfolioPhotoUrls: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["portfolioPhotoUrls"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "portfolioPhotoUrls")
    }
  }

  public var service: Swift.Optional<ServiceInput?> {
    get {
      return graphQLMap["service"] as? Swift.Optional<ServiceInput?> ?? Swift.Optional<ServiceInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "service")
    }
  }

  public var avatarUrl: Swift.Optional<String?> {
    get {
      return graphQLMap["avatarUrl"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "avatarUrl")
    }
  }
}

public struct ServiceInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - serviceDescription
  ///   - servicePackages
  ///   - category
  ///   - servicePhotoUrls
  ///   - serviceInfo
  public init(serviceDescription: Swift.Optional<String?> = nil, servicePackages: Swift.Optional<[ServicePackageInput?]?> = nil, category: Swift.Optional<JobCategoryInput?> = nil, servicePhotoUrls: Swift.Optional<[String?]?> = nil, serviceInfo: Swift.Optional<String?> = nil) {
    graphQLMap = ["serviceDescription": serviceDescription, "servicePackages": servicePackages, "category": category, "servicePhotoUrls": servicePhotoUrls, "serviceInfo": serviceInfo]
  }

  public var serviceDescription: Swift.Optional<String?> {
    get {
      return graphQLMap["serviceDescription"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "serviceDescription")
    }
  }

  public var servicePackages: Swift.Optional<[ServicePackageInput?]?> {
    get {
      return graphQLMap["servicePackages"] as? Swift.Optional<[ServicePackageInput?]?> ?? Swift.Optional<[ServicePackageInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "servicePackages")
    }
  }

  public var category: Swift.Optional<JobCategoryInput?> {
    get {
      return graphQLMap["category"] as? Swift.Optional<JobCategoryInput?> ?? Swift.Optional<JobCategoryInput?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "category")
    }
  }

  public var servicePhotoUrls: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["servicePhotoUrls"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "servicePhotoUrls")
    }
  }

  public var serviceInfo: Swift.Optional<String?> {
    get {
      return graphQLMap["serviceInfo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "serviceInfo")
    }
  }
}

public struct ServicePackageInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - deliverables
  ///   - price
  ///   - unit
  ///   - quantity
  ///   - description
  ///   - packageType
  public init(deliverables: Swift.Optional<[String?]?> = nil, price: Swift.Optional<Double?> = nil, unit: Swift.Optional<String?> = nil, quantity: Swift.Optional<Double?> = nil, description: Swift.Optional<String?> = nil, packageType: Swift.Optional<PackageType?> = nil) {
    graphQLMap = ["deliverables": deliverables, "price": price, "unit": unit, "quantity": quantity, "description": description, "packageType": packageType]
  }

  public var deliverables: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["deliverables"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "deliverables")
    }
  }

  public var price: Swift.Optional<Double?> {
    get {
      return graphQLMap["price"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "price")
    }
  }

  public var unit: Swift.Optional<String?> {
    get {
      return graphQLMap["unit"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "unit")
    }
  }

  public var quantity: Swift.Optional<Double?> {
    get {
      return graphQLMap["quantity"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "quantity")
    }
  }

  public var description: Swift.Optional<String?> {
    get {
      return graphQLMap["description"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var packageType: Swift.Optional<PackageType?> {
    get {
      return graphQLMap["packageType"] as? Swift.Optional<PackageType?> ?? Swift.Optional<PackageType?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "packageType")
    }
  }
}

public enum PackageType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case regular
  case standard
  case premium
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "REGULAR": self = .regular
      case "STANDARD": self = .standard
      case "PREMIUM": self = .premium
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .regular: return "REGULAR"
      case .standard: return "STANDARD"
      case .premium: return "PREMIUM"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: PackageType, rhs: PackageType) -> Bool {
    switch (lhs, rhs) {
      case (.regular, .regular): return true
      case (.standard, .standard): return true
      case (.premium, .premium): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [PackageType] {
    return [
      .regular,
      .standard,
      .premium,
    ]
  }
}

public struct JobCategoryInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  public init(id: GraphQLID) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct BookingInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - note
  ///   - code
  ///   - customerQuantity
  public init(note: Swift.Optional<String?> = nil, code: Swift.Optional<String?> = nil, customerQuantity: Swift.Optional<Double?> = nil) {
    graphQLMap = ["note": note, "code": code, "customerQuantity": customerQuantity]
  }

  public var note: Swift.Optional<String?> {
    get {
      return graphQLMap["note"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "note")
    }
  }

  public var code: Swift.Optional<String?> {
    get {
      return graphQLMap["code"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "code")
    }
  }

  public var customerQuantity: Swift.Optional<Double?> {
    get {
      return graphQLMap["customerQuantity"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "customerQuantity")
    }
  }
}

public enum Status: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case inProgress
  case cancelled
  case finished
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "IN_PROGRESS": self = .inProgress
      case "CANCELLED": self = .cancelled
      case "FINISHED": self = .finished
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .inProgress: return "IN_PROGRESS"
      case .cancelled: return "CANCELLED"
      case .finished: return "FINISHED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: Status, rhs: Status) -> Bool {
    switch (lhs, rhs) {
      case (.inProgress, .inProgress): return true
      case (.cancelled, .cancelled): return true
      case (.finished, .finished): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [Status] {
    return [
      .inProgress,
      .cancelled,
      .finished,
    ]
  }
}

public final class LoginMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation login($user: UserPayloadInput) {
      login(user: $user) {
        __typename
        token
        user {
          __typename
          id
          role
          email
        }
      }
    }
    """

  public let operationName: String = "login"

  public var user: UserPayloadInput?

  public init(user: UserPayloadInput? = nil) {
    self.user = user
  }

  public var variables: GraphQLMap? {
    return ["user": user]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("login", arguments: ["user": GraphQLVariable("user")], type: .object(Login.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(login: Login? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "login": login.flatMap { (value: Login) -> ResultMap in value.resultMap }])
    }

    public var login: Login? {
      get {
        return (resultMap["login"] as? ResultMap).flatMap { Login(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "login")
      }
    }

    public struct Login: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserPayloadResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("token", type: .scalar(String.self)),
          GraphQLField("user", type: .object(User.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(token: String? = nil, user: User? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPayloadResponse", "token": token, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var token: String? {
        get {
          return resultMap["token"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "token")
        }
      }

      public var user: User? {
        get {
          return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "user")
        }
      }

      public struct User: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["User"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("role", type: .scalar(Role.self)),
            GraphQLField("email", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, role: Role? = nil, email: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "User", "id": id, "role": role, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var role: Role? {
          get {
            return resultMap["role"] as? Role
          }
          set {
            resultMap.updateValue(newValue, forKey: "role")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }
      }
    }
  }
}

public final class DeleteExistedSellerFolderMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteExistedSellerFolder($filepath: String) {
      deleteExistedSellerFolder(filepath: $filepath) {
        __typename
        message
      }
    }
    """

  public let operationName: String = "deleteExistedSellerFolder"

  public var filepath: String?

  public init(filepath: String? = nil) {
    self.filepath = filepath
  }

  public var variables: GraphQLMap? {
    return ["filepath": filepath]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("deleteExistedSellerFolder", arguments: ["filepath": GraphQLVariable("filepath")], type: .object(DeleteExistedSellerFolder.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteExistedSellerFolder: DeleteExistedSellerFolder? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteExistedSellerFolder": deleteExistedSellerFolder.flatMap { (value: DeleteExistedSellerFolder) -> ResultMap in value.resultMap }])
    }

    public var deleteExistedSellerFolder: DeleteExistedSellerFolder? {
      get {
        return (resultMap["deleteExistedSellerFolder"] as? ResultMap).flatMap { DeleteExistedSellerFolder(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "deleteExistedSellerFolder")
      }
    }

    public struct DeleteExistedSellerFolder: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AWSResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "AWSResponse", "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class CreateSellerProfileMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createSellerProfile($seller: SellerInput, $userId: ID!) {
      createSellerProfile(seller: $seller, userId: $userId) {
        __typename
        id
        firstName
        lastName
        description
        services {
          __typename
          id
          serviceDescription
          packages {
            __typename
            id
            unit
            quantity
            price
            deliverables
          }
        }
      }
    }
    """

  public let operationName: String = "createSellerProfile"

  public var seller: SellerInput?
  public var userId: GraphQLID

  public init(seller: SellerInput? = nil, userId: GraphQLID) {
    self.seller = seller
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["seller": seller, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("createSellerProfile", arguments: ["seller": GraphQLVariable("seller"), "userId": GraphQLVariable("userId")], type: .object(CreateSellerProfile.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createSellerProfile: CreateSellerProfile? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createSellerProfile": createSellerProfile.flatMap { (value: CreateSellerProfile) -> ResultMap in value.resultMap }])
    }

    public var createSellerProfile: CreateSellerProfile? {
      get {
        return (resultMap["createSellerProfile"] as? ResultMap).flatMap { CreateSellerProfile(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createSellerProfile")
      }
    }

    public struct CreateSellerProfile: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Seller"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("services", type: .list(.object(Service.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, firstName: String? = nil, lastName: String? = nil, description: String? = nil, services: [Service?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Seller", "id": id, "firstName": firstName, "lastName": lastName, "description": description, "services": services.flatMap { (value: [Service?]) -> [ResultMap?] in value.map { (value: Service?) -> ResultMap? in value.flatMap { (value: Service) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var firstName: String? {
        get {
          return resultMap["firstName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return resultMap["lastName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastName")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var services: [Service?]? {
        get {
          return (resultMap["services"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Service?] in value.map { (value: ResultMap?) -> Service? in value.flatMap { (value: ResultMap) -> Service in Service(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Service?]) -> [ResultMap?] in value.map { (value: Service?) -> ResultMap? in value.flatMap { (value: Service) -> ResultMap in value.resultMap } } }, forKey: "services")
        }
      }

      public struct Service: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Service"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("serviceDescription", type: .scalar(String.self)),
            GraphQLField("packages", type: .list(.object(Package.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, serviceDescription: String? = nil, packages: [Package?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Service", "id": id, "serviceDescription": serviceDescription, "packages": packages.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var serviceDescription: String? {
          get {
            return resultMap["serviceDescription"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "serviceDescription")
          }
        }

        public var packages: [Package?]? {
          get {
            return (resultMap["packages"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Package?] in value.map { (value: ResultMap?) -> Package? in value.flatMap { (value: ResultMap) -> Package in Package(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }, forKey: "packages")
          }
        }

        public struct Package: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["ServicePackage"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("unit", type: .scalar(String.self)),
              GraphQLField("quantity", type: .scalar(Double.self)),
              GraphQLField("price", type: .scalar(Double.self)),
              GraphQLField("deliverables", type: .list(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, unit: String? = nil, quantity: Double? = nil, price: Double? = nil, deliverables: [String?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "ServicePackage", "id": id, "unit": unit, "quantity": quantity, "price": price, "deliverables": deliverables])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var unit: String? {
            get {
              return resultMap["unit"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "unit")
            }
          }

          public var quantity: Double? {
            get {
              return resultMap["quantity"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "quantity")
            }
          }

          public var price: Double? {
            get {
              return resultMap["price"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "price")
            }
          }

          public var deliverables: [String?]? {
            get {
              return resultMap["deliverables"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "deliverables")
            }
          }
        }
      }
    }
  }
}

public final class CreateBookingMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createBooking($booking: BookingInput, $packageId: ID!, $userId: ID!) {
      createBooking(booking: $booking, userId: $userId, packageId: $packageId) {
        __typename
        data {
          __typename
          id
          price
          customerQuantity
          code
        }
        message
        status
      }
    }
    """

  public let operationName: String = "createBooking"

  public var booking: BookingInput?
  public var packageId: GraphQLID
  public var userId: GraphQLID

  public init(booking: BookingInput? = nil, packageId: GraphQLID, userId: GraphQLID) {
    self.booking = booking
    self.packageId = packageId
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["booking": booking, "packageId": packageId, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("createBooking", arguments: ["booking": GraphQLVariable("booking"), "userId": GraphQLVariable("userId"), "packageId": GraphQLVariable("packageId")], type: .object(CreateBooking.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createBooking: CreateBooking? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createBooking": createBooking.flatMap { (value: CreateBooking) -> ResultMap in value.resultMap }])
    }

    public var createBooking: CreateBooking? {
      get {
        return (resultMap["createBooking"] as? ResultMap).flatMap { CreateBooking(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createBooking")
      }
    }

    public struct CreateBooking: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BookingResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(Int.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(data: Datum? = nil, message: String? = nil, status: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "BookingResponse", "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "message": message, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var status: Int? {
        get {
          return resultMap["status"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Booking"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("price", type: .scalar(Double.self)),
            GraphQLField("customerQuantity", type: .scalar(Double.self)),
            GraphQLField("code", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, price: Double? = nil, customerQuantity: Double? = nil, code: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Booking", "id": id, "price": price, "customerQuantity": customerQuantity, "code": code])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var price: Double? {
          get {
            return resultMap["price"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "price")
          }
        }

        public var customerQuantity: Double? {
          get {
            return resultMap["customerQuantity"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "customerQuantity")
          }
        }

        public var code: String? {
          get {
            return resultMap["code"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "code")
          }
        }
      }
    }
  }
}

public final class UpdateSellerProfileMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateSellerProfile($id: ID!, $avatarUrl: String, $description: String, $portfolioPhotoUrls: [String]) {
      updateSellerProfile(
        id: $id
        avatarUrl: $avatarUrl
        description: $description
        portfolioPhotoUrls: $portfolioPhotoUrls
      ) {
        __typename
        id
        avatarUrl
        portfolioPhotoUrls
        description
      }
    }
    """

  public let operationName: String = "updateSellerProfile"

  public var id: GraphQLID
  public var avatarUrl: String?
  public var description: String?
  public var portfolioPhotoUrls: [String?]?

  public init(id: GraphQLID, avatarUrl: String? = nil, description: String? = nil, portfolioPhotoUrls: [String?]? = nil) {
    self.id = id
    self.avatarUrl = avatarUrl
    self.description = description
    self.portfolioPhotoUrls = portfolioPhotoUrls
  }

  public var variables: GraphQLMap? {
    return ["id": id, "avatarUrl": avatarUrl, "description": description, "portfolioPhotoUrls": portfolioPhotoUrls]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("updateSellerProfile", arguments: ["id": GraphQLVariable("id"), "avatarUrl": GraphQLVariable("avatarUrl"), "description": GraphQLVariable("description"), "portfolioPhotoUrls": GraphQLVariable("portfolioPhotoUrls")], type: .object(UpdateSellerProfile.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateSellerProfile: UpdateSellerProfile? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateSellerProfile": updateSellerProfile.flatMap { (value: UpdateSellerProfile) -> ResultMap in value.resultMap }])
    }

    public var updateSellerProfile: UpdateSellerProfile? {
      get {
        return (resultMap["updateSellerProfile"] as? ResultMap).flatMap { UpdateSellerProfile(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateSellerProfile")
      }
    }

    public struct UpdateSellerProfile: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Seller"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("avatarUrl", type: .scalar(String.self)),
          GraphQLField("portfolioPhotoUrls", type: .list(.scalar(String.self))),
          GraphQLField("description", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, avatarUrl: String? = nil, portfolioPhotoUrls: [String?]? = nil, description: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Seller", "id": id, "avatarUrl": avatarUrl, "portfolioPhotoUrls": portfolioPhotoUrls, "description": description])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var avatarUrl: String? {
        get {
          return resultMap["avatarUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avatarUrl")
        }
      }

      public var portfolioPhotoUrls: [String?]? {
        get {
          return resultMap["portfolioPhotoUrls"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "portfolioPhotoUrls")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }
    }
  }
}

public final class CreateServiceMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createService($sellerId: ID!, $service: ServiceInput) {
      createService(sellerId: $sellerId, service: $service) {
        __typename
        id
      }
    }
    """

  public let operationName: String = "createService"

  public var sellerId: GraphQLID
  public var service: ServiceInput?

  public init(sellerId: GraphQLID, service: ServiceInput? = nil) {
    self.sellerId = sellerId
    self.service = service
  }

  public var variables: GraphQLMap? {
    return ["sellerId": sellerId, "service": service]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("createService", arguments: ["sellerId": GraphQLVariable("sellerId"), "service": GraphQLVariable("service")], type: .object(CreateService.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createService: CreateService? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createService": createService.flatMap { (value: CreateService) -> ResultMap in value.resultMap }])
    }

    public var createService: CreateService? {
      get {
        return (resultMap["createService"] as? ResultMap).flatMap { CreateService(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createService")
      }
    }

    public struct CreateService: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Service"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil) {
        self.init(unsafeResultMap: ["__typename": "Service", "id": id])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }
    }
  }
}

public final class AllUsersQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query allUsers {
      allUsers {
        __typename
        id
        email
      }
    }
    """

  public let operationName: String = "allUsers"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("allUsers", type: .list(.object(AllUser.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(allUsers: [AllUser?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "allUsers": allUsers.flatMap { (value: [AllUser?]) -> [ResultMap?] in value.map { (value: AllUser?) -> ResultMap? in value.flatMap { (value: AllUser) -> ResultMap in value.resultMap } } }])
    }

    public var allUsers: [AllUser?]? {
      get {
        return (resultMap["allUsers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AllUser?] in value.map { (value: ResultMap?) -> AllUser? in value.flatMap { (value: ResultMap) -> AllUser in AllUser(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [AllUser?]) -> [ResultMap?] in value.map { (value: AllUser?) -> ResultMap? in value.flatMap { (value: AllUser) -> ResultMap in value.resultMap } } }, forKey: "allUsers")
      }
    }

    public struct AllUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["User"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("email", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, email: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "User", "id": id, "email": email])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return resultMap["email"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }
    }
  }
}

public final class AllJobCategoriesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query allJobCategories {
      allJobCategories {
        __typename
        id
        categoryDescription
        categoryName
      }
    }
    """

  public let operationName: String = "allJobCategories"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("allJobCategories", type: .list(.object(AllJobCategory.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(allJobCategories: [AllJobCategory?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "allJobCategories": allJobCategories.flatMap { (value: [AllJobCategory?]) -> [ResultMap?] in value.map { (value: AllJobCategory?) -> ResultMap? in value.flatMap { (value: AllJobCategory) -> ResultMap in value.resultMap } } }])
    }

    public var allJobCategories: [AllJobCategory?]? {
      get {
        return (resultMap["allJobCategories"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AllJobCategory?] in value.map { (value: ResultMap?) -> AllJobCategory? in value.flatMap { (value: ResultMap) -> AllJobCategory in AllJobCategory(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [AllJobCategory?]) -> [ResultMap?] in value.map { (value: AllJobCategory?) -> ResultMap? in value.flatMap { (value: AllJobCategory) -> ResultMap in value.resultMap } } }, forKey: "allJobCategories")
      }
    }

    public struct AllJobCategory: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["JobCategory"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("categoryDescription", type: .scalar(String.self)),
          GraphQLField("categoryName", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, categoryDescription: String? = nil, categoryName: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "JobCategory", "id": id, "categoryDescription": categoryDescription, "categoryName": categoryName])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var categoryDescription: String? {
        get {
          return resultMap["categoryDescription"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "categoryDescription")
        }
      }

      public var categoryName: String? {
        get {
          return resultMap["categoryName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "categoryName")
        }
      }
    }
  }
}

public final class JobCategoryBasicQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query jobCategoryBasic {
      allJobCategories {
        __typename
        ...JobCategoryBasic
      }
    }
    """

  public let operationName: String = "jobCategoryBasic"

  public var queryDocument: String {
    var document: String = operationDefinition
    document.append("\n" + JobCategoryBasic.fragmentDefinition)
    return document
  }

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("allJobCategories", type: .list(.object(AllJobCategory.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(allJobCategories: [AllJobCategory?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "allJobCategories": allJobCategories.flatMap { (value: [AllJobCategory?]) -> [ResultMap?] in value.map { (value: AllJobCategory?) -> ResultMap? in value.flatMap { (value: AllJobCategory) -> ResultMap in value.resultMap } } }])
    }

    public var allJobCategories: [AllJobCategory?]? {
      get {
        return (resultMap["allJobCategories"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AllJobCategory?] in value.map { (value: ResultMap?) -> AllJobCategory? in value.flatMap { (value: ResultMap) -> AllJobCategory in AllJobCategory(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [AllJobCategory?]) -> [ResultMap?] in value.map { (value: AllJobCategory?) -> ResultMap? in value.flatMap { (value: AllJobCategory) -> ResultMap in value.resultMap } } }, forKey: "allJobCategories")
      }
    }

    public struct AllJobCategory: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["JobCategory"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLFragmentSpread(JobCategoryBasic.self),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, categoryName: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "JobCategory", "id": id, "categoryName": categoryName])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(unsafeResultMap: resultMap)
        }
        set {
          resultMap += newValue.resultMap
        }
      }

      public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public var jobCategoryBasic: JobCategoryBasic {
          get {
            return JobCategoryBasic(unsafeResultMap: resultMap)
          }
          set {
            resultMap += newValue.resultMap
          }
        }
      }
    }
  }
}

public final class SearchServiceByKeywordQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query searchServiceByKeyword($keyWord: String) {
      searchServiceByKeyword(keyWord: $keyWord) {
        __typename
        id
        category {
          __typename
          categoryName
        }
        seller {
          __typename
          fullName
          avatarUrl
        }
        packages {
          __typename
          price
          id
          packageType
        }
        servicePhotoUrls
        serviceDescription
      }
    }
    """

  public let operationName: String = "searchServiceByKeyword"

  public var keyWord: String?

  public init(keyWord: String? = nil) {
    self.keyWord = keyWord
  }

  public var variables: GraphQLMap? {
    return ["keyWord": keyWord]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("searchServiceByKeyword", arguments: ["keyWord": GraphQLVariable("keyWord")], type: .list(.object(SearchServiceByKeyword.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(searchServiceByKeyword: [SearchServiceByKeyword?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "searchServiceByKeyword": searchServiceByKeyword.flatMap { (value: [SearchServiceByKeyword?]) -> [ResultMap?] in value.map { (value: SearchServiceByKeyword?) -> ResultMap? in value.flatMap { (value: SearchServiceByKeyword) -> ResultMap in value.resultMap } } }])
    }

    public var searchServiceByKeyword: [SearchServiceByKeyword?]? {
      get {
        return (resultMap["searchServiceByKeyword"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SearchServiceByKeyword?] in value.map { (value: ResultMap?) -> SearchServiceByKeyword? in value.flatMap { (value: ResultMap) -> SearchServiceByKeyword in SearchServiceByKeyword(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [SearchServiceByKeyword?]) -> [ResultMap?] in value.map { (value: SearchServiceByKeyword?) -> ResultMap? in value.flatMap { (value: SearchServiceByKeyword) -> ResultMap in value.resultMap } } }, forKey: "searchServiceByKeyword")
      }
    }

    public struct SearchServiceByKeyword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Service"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("category", type: .object(Category.selections)),
          GraphQLField("seller", type: .object(Seller.selections)),
          GraphQLField("packages", type: .list(.object(Package.selections))),
          GraphQLField("servicePhotoUrls", type: .list(.scalar(String.self))),
          GraphQLField("serviceDescription", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, category: Category? = nil, seller: Seller? = nil, packages: [Package?]? = nil, servicePhotoUrls: [String?]? = nil, serviceDescription: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Service", "id": id, "category": category.flatMap { (value: Category) -> ResultMap in value.resultMap }, "seller": seller.flatMap { (value: Seller) -> ResultMap in value.resultMap }, "packages": packages.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }, "servicePhotoUrls": servicePhotoUrls, "serviceDescription": serviceDescription])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var category: Category? {
        get {
          return (resultMap["category"] as? ResultMap).flatMap { Category(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "category")
        }
      }

      public var seller: Seller? {
        get {
          return (resultMap["seller"] as? ResultMap).flatMap { Seller(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "seller")
        }
      }

      public var packages: [Package?]? {
        get {
          return (resultMap["packages"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Package?] in value.map { (value: ResultMap?) -> Package? in value.flatMap { (value: ResultMap) -> Package in Package(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }, forKey: "packages")
        }
      }

      public var servicePhotoUrls: [String?]? {
        get {
          return resultMap["servicePhotoUrls"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "servicePhotoUrls")
        }
      }

      public var serviceDescription: String? {
        get {
          return resultMap["serviceDescription"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "serviceDescription")
        }
      }

      public struct Category: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["JobCategory"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("categoryName", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(categoryName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "JobCategory", "categoryName": categoryName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var categoryName: String? {
          get {
            return resultMap["categoryName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "categoryName")
          }
        }
      }

      public struct Seller: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Seller"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("fullName", type: .scalar(String.self)),
            GraphQLField("avatarUrl", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(fullName: String? = nil, avatarUrl: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Seller", "fullName": fullName, "avatarUrl": avatarUrl])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var fullName: String? {
          get {
            return resultMap["fullName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "fullName")
          }
        }

        public var avatarUrl: String? {
          get {
            return resultMap["avatarUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatarUrl")
          }
        }
      }

      public struct Package: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ServicePackage"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("price", type: .scalar(Double.self)),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("packageType", type: .scalar(PackageType.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(price: Double? = nil, id: GraphQLID? = nil, packageType: PackageType? = nil) {
          self.init(unsafeResultMap: ["__typename": "ServicePackage", "price": price, "id": id, "packageType": packageType])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var price: Double? {
          get {
            return resultMap["price"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "price")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var packageType: PackageType? {
          get {
            return resultMap["packageType"] as? PackageType
          }
          set {
            resultMap.updateValue(newValue, forKey: "packageType")
          }
        }
      }
    }
  }
}

public final class SearchSellersByKeywordQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query searchSellersByKeyword($keyWord: String) {
      searchSellersByKeyword(keyWord: $keyWord) {
        __typename
        id
        firstName
        lastName
        avatarUrl
        fullName
        user {
          __typename
          id
        }
      }
    }
    """

  public let operationName: String = "searchSellersByKeyword"

  public var keyWord: String?

  public init(keyWord: String? = nil) {
    self.keyWord = keyWord
  }

  public var variables: GraphQLMap? {
    return ["keyWord": keyWord]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("searchSellersByKeyword", arguments: ["keyWord": GraphQLVariable("keyWord")], type: .list(.object(SearchSellersByKeyword.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(searchSellersByKeyword: [SearchSellersByKeyword?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "searchSellersByKeyword": searchSellersByKeyword.flatMap { (value: [SearchSellersByKeyword?]) -> [ResultMap?] in value.map { (value: SearchSellersByKeyword?) -> ResultMap? in value.flatMap { (value: SearchSellersByKeyword) -> ResultMap in value.resultMap } } }])
    }

    public var searchSellersByKeyword: [SearchSellersByKeyword?]? {
      get {
        return (resultMap["searchSellersByKeyword"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SearchSellersByKeyword?] in value.map { (value: ResultMap?) -> SearchSellersByKeyword? in value.flatMap { (value: ResultMap) -> SearchSellersByKeyword in SearchSellersByKeyword(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [SearchSellersByKeyword?]) -> [ResultMap?] in value.map { (value: SearchSellersByKeyword?) -> ResultMap? in value.flatMap { (value: SearchSellersByKeyword) -> ResultMap in value.resultMap } } }, forKey: "searchSellersByKeyword")
      }
    }

    public struct SearchSellersByKeyword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Seller"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("avatarUrl", type: .scalar(String.self)),
          GraphQLField("fullName", type: .scalar(String.self)),
          GraphQLField("user", type: .object(User.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, firstName: String? = nil, lastName: String? = nil, avatarUrl: String? = nil, fullName: String? = nil, user: User? = nil) {
        self.init(unsafeResultMap: ["__typename": "Seller", "id": id, "firstName": firstName, "lastName": lastName, "avatarUrl": avatarUrl, "fullName": fullName, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var firstName: String? {
        get {
          return resultMap["firstName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return resultMap["lastName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastName")
        }
      }

      public var avatarUrl: String? {
        get {
          return resultMap["avatarUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avatarUrl")
        }
      }

      public var fullName: String? {
        get {
          return resultMap["fullName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "fullName")
        }
      }

      public var user: User? {
        get {
          return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "user")
        }
      }

      public struct User: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["User"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil) {
          self.init(unsafeResultMap: ["__typename": "User", "id": id])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }
      }
    }
  }
}

public final class GetSellerByUseridQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSellerByUserid($id: ID!) {
      getSellerByUserid(id: $id) {
        __typename
        id
        portfolioPhotoUrls
        description
        createdAt
        fullName
        avatarUrl
        services {
          __typename
          id
          serviceDescription
          servicePhotoUrls
          packages {
            __typename
            price
          }
        }
      }
    }
    """

  public let operationName: String = "getSellerByUserid"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getSellerByUserid", arguments: ["id": GraphQLVariable("id")], type: .object(GetSellerByUserid.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSellerByUserid: GetSellerByUserid? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSellerByUserid": getSellerByUserid.flatMap { (value: GetSellerByUserid) -> ResultMap in value.resultMap }])
    }

    public var getSellerByUserid: GetSellerByUserid? {
      get {
        return (resultMap["getSellerByUserid"] as? ResultMap).flatMap { GetSellerByUserid(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getSellerByUserid")
      }
    }

    public struct GetSellerByUserid: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Seller"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("portfolioPhotoUrls", type: .list(.scalar(String.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("createdAt", type: .scalar(String.self)),
          GraphQLField("fullName", type: .scalar(String.self)),
          GraphQLField("avatarUrl", type: .scalar(String.self)),
          GraphQLField("services", type: .list(.object(Service.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, portfolioPhotoUrls: [String?]? = nil, description: String? = nil, createdAt: String? = nil, fullName: String? = nil, avatarUrl: String? = nil, services: [Service?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Seller", "id": id, "portfolioPhotoUrls": portfolioPhotoUrls, "description": description, "createdAt": createdAt, "fullName": fullName, "avatarUrl": avatarUrl, "services": services.flatMap { (value: [Service?]) -> [ResultMap?] in value.map { (value: Service?) -> ResultMap? in value.flatMap { (value: Service) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var portfolioPhotoUrls: [String?]? {
        get {
          return resultMap["portfolioPhotoUrls"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "portfolioPhotoUrls")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var createdAt: String? {
        get {
          return resultMap["createdAt"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var fullName: String? {
        get {
          return resultMap["fullName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "fullName")
        }
      }

      public var avatarUrl: String? {
        get {
          return resultMap["avatarUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avatarUrl")
        }
      }

      public var services: [Service?]? {
        get {
          return (resultMap["services"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Service?] in value.map { (value: ResultMap?) -> Service? in value.flatMap { (value: ResultMap) -> Service in Service(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Service?]) -> [ResultMap?] in value.map { (value: Service?) -> ResultMap? in value.flatMap { (value: Service) -> ResultMap in value.resultMap } } }, forKey: "services")
        }
      }

      public struct Service: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Service"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("serviceDescription", type: .scalar(String.self)),
            GraphQLField("servicePhotoUrls", type: .list(.scalar(String.self))),
            GraphQLField("packages", type: .list(.object(Package.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, serviceDescription: String? = nil, servicePhotoUrls: [String?]? = nil, packages: [Package?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Service", "id": id, "serviceDescription": serviceDescription, "servicePhotoUrls": servicePhotoUrls, "packages": packages.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var serviceDescription: String? {
          get {
            return resultMap["serviceDescription"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "serviceDescription")
          }
        }

        public var servicePhotoUrls: [String?]? {
          get {
            return resultMap["servicePhotoUrls"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "servicePhotoUrls")
          }
        }

        public var packages: [Package?]? {
          get {
            return (resultMap["packages"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Package?] in value.map { (value: ResultMap?) -> Package? in value.flatMap { (value: ResultMap) -> Package in Package(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }, forKey: "packages")
          }
        }

        public struct Package: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["ServicePackage"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("price", type: .scalar(Double.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(price: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "ServicePackage", "price": price])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var price: Double? {
            get {
              return resultMap["price"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "price")
            }
          }
        }
      }
    }
  }
}

public final class ServiceQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Service($id: ID!) {
      Service(id: $id) {
        __typename
        id
        serviceDescription
        servicePhotoUrls
        seller {
          __typename
          id
          fullName
          avatarUrl
          portfolioPhotoUrls
          user {
            __typename
            id
          }
        }
        packages {
          __typename
          description
          packageType
          quantity
          unit
          id
          price
          deliverables
        }
        category {
          __typename
          categoryName
        }
      }
    }
    """

  public let operationName: String = "Service"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("Service", arguments: ["id": GraphQLVariable("id")], type: .object(Service.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(service: Service? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Service": service.flatMap { (value: Service) -> ResultMap in value.resultMap }])
    }

    public var service: Service? {
      get {
        return (resultMap["Service"] as? ResultMap).flatMap { Service(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Service")
      }
    }

    public struct Service: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Service"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("serviceDescription", type: .scalar(String.self)),
          GraphQLField("servicePhotoUrls", type: .list(.scalar(String.self))),
          GraphQLField("seller", type: .object(Seller.selections)),
          GraphQLField("packages", type: .list(.object(Package.selections))),
          GraphQLField("category", type: .object(Category.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, serviceDescription: String? = nil, servicePhotoUrls: [String?]? = nil, seller: Seller? = nil, packages: [Package?]? = nil, category: Category? = nil) {
        self.init(unsafeResultMap: ["__typename": "Service", "id": id, "serviceDescription": serviceDescription, "servicePhotoUrls": servicePhotoUrls, "seller": seller.flatMap { (value: Seller) -> ResultMap in value.resultMap }, "packages": packages.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }, "category": category.flatMap { (value: Category) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var serviceDescription: String? {
        get {
          return resultMap["serviceDescription"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "serviceDescription")
        }
      }

      public var servicePhotoUrls: [String?]? {
        get {
          return resultMap["servicePhotoUrls"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "servicePhotoUrls")
        }
      }

      public var seller: Seller? {
        get {
          return (resultMap["seller"] as? ResultMap).flatMap { Seller(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "seller")
        }
      }

      public var packages: [Package?]? {
        get {
          return (resultMap["packages"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Package?] in value.map { (value: ResultMap?) -> Package? in value.flatMap { (value: ResultMap) -> Package in Package(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Package?]) -> [ResultMap?] in value.map { (value: Package?) -> ResultMap? in value.flatMap { (value: Package) -> ResultMap in value.resultMap } } }, forKey: "packages")
        }
      }

      public var category: Category? {
        get {
          return (resultMap["category"] as? ResultMap).flatMap { Category(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "category")
        }
      }

      public struct Seller: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Seller"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("fullName", type: .scalar(String.self)),
            GraphQLField("avatarUrl", type: .scalar(String.self)),
            GraphQLField("portfolioPhotoUrls", type: .list(.scalar(String.self))),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, fullName: String? = nil, avatarUrl: String? = nil, portfolioPhotoUrls: [String?]? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "Seller", "id": id, "fullName": fullName, "avatarUrl": avatarUrl, "portfolioPhotoUrls": portfolioPhotoUrls, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var fullName: String? {
          get {
            return resultMap["fullName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "fullName")
          }
        }

        public var avatarUrl: String? {
          get {
            return resultMap["avatarUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatarUrl")
          }
        }

        public var portfolioPhotoUrls: [String?]? {
          get {
            return resultMap["portfolioPhotoUrls"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "portfolioPhotoUrls")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }
        }
      }

      public struct Package: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ServicePackage"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("packageType", type: .scalar(PackageType.self)),
            GraphQLField("quantity", type: .scalar(Double.self)),
            GraphQLField("unit", type: .scalar(String.self)),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("price", type: .scalar(Double.self)),
            GraphQLField("deliverables", type: .list(.scalar(String.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(description: String? = nil, packageType: PackageType? = nil, quantity: Double? = nil, unit: String? = nil, id: GraphQLID? = nil, price: Double? = nil, deliverables: [String?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "ServicePackage", "description": description, "packageType": packageType, "quantity": quantity, "unit": unit, "id": id, "price": price, "deliverables": deliverables])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }

        public var packageType: PackageType? {
          get {
            return resultMap["packageType"] as? PackageType
          }
          set {
            resultMap.updateValue(newValue, forKey: "packageType")
          }
        }

        public var quantity: Double? {
          get {
            return resultMap["quantity"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "quantity")
          }
        }

        public var unit: String? {
          get {
            return resultMap["unit"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "unit")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var price: Double? {
          get {
            return resultMap["price"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "price")
          }
        }

        public var deliverables: [String?]? {
          get {
            return resultMap["deliverables"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "deliverables")
          }
        }
      }

      public struct Category: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["JobCategory"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("categoryName", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(categoryName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "JobCategory", "categoryName": categoryName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var categoryName: String? {
          get {
            return resultMap["categoryName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "categoryName")
          }
        }
      }
    }
  }
}

public final class GetUsersBookingsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getUsersBookings($userId: ID!) {
      getUsersBookings(userId: $userId) {
        __typename
        id
        createdAt
        status
        code
        price
        customerQuantity
        seller {
          __typename
          id
          fullName
          avatarUrl
        }
        package {
          __typename
          service {
            __typename
            category {
              __typename
              categoryName
            }
          }
        }
      }
    }
    """

  public let operationName: String = "getUsersBookings"

  public var userId: GraphQLID

  public init(userId: GraphQLID) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getUsersBookings", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetUsersBooking.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getUsersBookings: [GetUsersBooking?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getUsersBookings": getUsersBookings.flatMap { (value: [GetUsersBooking?]) -> [ResultMap?] in value.map { (value: GetUsersBooking?) -> ResultMap? in value.flatMap { (value: GetUsersBooking) -> ResultMap in value.resultMap } } }])
    }

    public var getUsersBookings: [GetUsersBooking?]? {
      get {
        return (resultMap["getUsersBookings"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetUsersBooking?] in value.map { (value: ResultMap?) -> GetUsersBooking? in value.flatMap { (value: ResultMap) -> GetUsersBooking in GetUsersBooking(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetUsersBooking?]) -> [ResultMap?] in value.map { (value: GetUsersBooking?) -> ResultMap? in value.flatMap { (value: GetUsersBooking) -> ResultMap in value.resultMap } } }, forKey: "getUsersBookings")
      }
    }

    public struct GetUsersBooking: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Booking"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("createdAt", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(Status.self)),
          GraphQLField("code", type: .scalar(String.self)),
          GraphQLField("price", type: .scalar(Double.self)),
          GraphQLField("customerQuantity", type: .scalar(Double.self)),
          GraphQLField("seller", type: .object(Seller.selections)),
          GraphQLField("package", type: .object(Package.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, createdAt: String? = nil, status: Status? = nil, code: String? = nil, price: Double? = nil, customerQuantity: Double? = nil, seller: Seller? = nil, package: Package? = nil) {
        self.init(unsafeResultMap: ["__typename": "Booking", "id": id, "createdAt": createdAt, "status": status, "code": code, "price": price, "customerQuantity": customerQuantity, "seller": seller.flatMap { (value: Seller) -> ResultMap in value.resultMap }, "package": package.flatMap { (value: Package) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: String? {
        get {
          return resultMap["createdAt"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var status: Status? {
        get {
          return resultMap["status"] as? Status
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var code: String? {
        get {
          return resultMap["code"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "code")
        }
      }

      public var price: Double? {
        get {
          return resultMap["price"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "price")
        }
      }

      public var customerQuantity: Double? {
        get {
          return resultMap["customerQuantity"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "customerQuantity")
        }
      }

      public var seller: Seller? {
        get {
          return (resultMap["seller"] as? ResultMap).flatMap { Seller(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "seller")
        }
      }

      public var package: Package? {
        get {
          return (resultMap["package"] as? ResultMap).flatMap { Package(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "package")
        }
      }

      public struct Seller: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Seller"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("fullName", type: .scalar(String.self)),
            GraphQLField("avatarUrl", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, fullName: String? = nil, avatarUrl: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Seller", "id": id, "fullName": fullName, "avatarUrl": avatarUrl])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var fullName: String? {
          get {
            return resultMap["fullName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "fullName")
          }
        }

        public var avatarUrl: String? {
          get {
            return resultMap["avatarUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatarUrl")
          }
        }
      }

      public struct Package: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ServicePackage"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("service", type: .object(Service.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(service: Service? = nil) {
          self.init(unsafeResultMap: ["__typename": "ServicePackage", "service": service.flatMap { (value: Service) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var service: Service? {
          get {
            return (resultMap["service"] as? ResultMap).flatMap { Service(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "service")
          }
        }

        public struct Service: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Service"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("category", type: .object(Category.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(category: Category? = nil) {
            self.init(unsafeResultMap: ["__typename": "Service", "category": category.flatMap { (value: Category) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var category: Category? {
            get {
              return (resultMap["category"] as? ResultMap).flatMap { Category(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "category")
            }
          }

          public struct Category: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["JobCategory"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("categoryName", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(categoryName: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "JobCategory", "categoryName": categoryName])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var categoryName: String? {
              get {
                return resultMap["categoryName"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "categoryName")
              }
            }
          }
        }
      }
    }
  }
}

public struct JobCategoryBasic: GraphQLFragment {
  /// The raw GraphQL definition of this fragment.
  public static let fragmentDefinition: String =
    """
    fragment JobCategoryBasic on JobCategory {
      __typename
      id
      categoryName
    }
    """

  public static let possibleTypes: [String] = ["JobCategory"]

  public static var selections: [GraphQLSelection] {
    return [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("id", type: .scalar(GraphQLID.self)),
      GraphQLField("categoryName", type: .scalar(String.self)),
    ]
  }

  public private(set) var resultMap: ResultMap

  public init(unsafeResultMap: ResultMap) {
    self.resultMap = unsafeResultMap
  }

  public init(id: GraphQLID? = nil, categoryName: String? = nil) {
    self.init(unsafeResultMap: ["__typename": "JobCategory", "id": id, "categoryName": categoryName])
  }

  public var __typename: String {
    get {
      return resultMap["__typename"]! as! String
    }
    set {
      resultMap.updateValue(newValue, forKey: "__typename")
    }
  }

  public var id: GraphQLID? {
    get {
      return resultMap["id"] as? GraphQLID
    }
    set {
      resultMap.updateValue(newValue, forKey: "id")
    }
  }

  public var categoryName: String? {
    get {
      return resultMap["categoryName"] as? String
    }
    set {
      resultMap.updateValue(newValue, forKey: "categoryName")
    }
  }
}
