//
//  SQLiteCommand.swift
//  final-project
//
//  Created by Phuong Nguyen on 28/08/2021.
//

import Foundation
import SQLite

class SQLiteCommands {
    static var table = Table("users")
    
    // Expressions
    static let users = Table("users")
    static let id = Expression<String>("id")
    static let email = Expression<String>("email")
    static let role = Expression<String>("role")
    
    
    
    // Creating Table
    static func createTable() {
        guard let database = SQLiteDatabase.shared.db else {
            print("Datastore connection error")
            return
        }
        
        do {
            // ifNotExists: true - Will NOT create a table if it already exists
            try database.run(table.create(ifNotExists: true) { table in
                table.column(id, primaryKey: true)
                table.column(email)
                table.column(role)
            })
        } catch {
            print("Table already exists: \(error)")
        }
    }
    
    static func insertRowUser(_ user: User) -> Bool? {
        guard let database = SQLiteDatabase.shared.db  else {
            print("Datastore connection error")
            return nil
        }
        
        do {
            try database.run(table.insert(id <- user.id, email <- user.email, role <- user.role))
            return true
        } catch let Result.error(message, code, statement) where code == SQLITE_CONSTRAINT {
            print("Insert row failed: \(message), in \(String(describing: statement))")
            return false
        } catch let error {
            print("Insertion failed: \(error)")
            return false
        }
    }
    
    static func getCurrentUserInfo() -> User? {
        guard let database = SQLiteDatabase.shared.db  else {
            print("Datastore connection error")
            return nil
        }
        
        var user: User?
        
        do {
            for users in try database.prepare(table) {
                
                let idValue = users[id]
                let roleValue = users[role]
                let emailValue = users[email]
                // Create object
                let userObject = User(id: idValue, email: emailValue, role: roleValue)
                user = userObject
            }
        } catch {
            print("Present row error: \(error)")
        }
        return user
    }
    
    static func deleteUser(at rowId: String) -> Bool? {
        guard let database = SQLiteDatabase.shared.db  else {
            print("Datastore connection error")
            return nil
        }
        let alice = users.filter(id == rowId)
        do {
            try database.run(alice.delete())
            return true
        }
        catch {
            print("Delete failed: \(error)")
            return false
        }
    }
}
