//
//  SQLiteManager.swift
//  final-project
//
//  Created by Phuong Nguyen on 28/08/2021.
//

import Foundation
import SQLite

class SQLiteDatabase {
    static var shared = SQLiteDatabase()
    var db: Connection?
    
    init() {
        do {
            let path = NSSearchPathForDirectoriesInDomains(
                .documentDirectory, .userDomainMask, true
            ).first!
            print(path)
            db = try Connection("\(path)/db.sqlite3")
            
        } catch {
            print(error)
        }
    }
}
