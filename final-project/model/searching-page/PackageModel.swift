//
//  PackageModel.swift
//  final-project
//
//  Created by Phuong Nguyen on 11/09/2021.
//

import Foundation

class PackageModel {
    let id: String?
    let price: Float?
    let packageType: PackageType?
    let unit: String?
    let quantity: Double?
    let description: String?
    let deliverables: [String]?
    
    init(id: String?, price: Float?, type: PackageType?, unit: String?, quantity: Double?, description: String?, deliverables: [String]?) {
        self.id = id
        self.price = price
        self.packageType = type
        self.unit = unit
        self.quantity = quantity
        self.description = description
        self.deliverables = deliverables
    }
}
