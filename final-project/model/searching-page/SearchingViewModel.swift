//
//  SearchingViewModel.swift
//  final-project
//
//  Created by Phuong Nguyen on 11/09/2021.
//

import Foundation

class ServiceModel {
    let id: String?
    let serviceDescription: String?
    let servicePhotoUrls: [String]?
    let packages: [PackageModel]?
    let seller: Seller?
    
    init(id: String?, serviceDescription: String?, servicePhotoUrls: [String]?, packages: [PackageModel]?, seller: Seller?) {
        self.id = id
        self.serviceDescription = serviceDescription
        self.servicePhotoUrls = servicePhotoUrls
        self.packages = packages
        self.seller = seller
    }
}
