//
//  Seller.swift
//  final-project
//
//  Created by Phuong Nguyen on 13/09/2021.
//

import Foundation

class Seller {
    let id: String?
    var portfolioUrls: [String]?
    let avatarUrl: String?
    let description: String?
    let fullName: String?
    let services: [ServiceModel]?
    let userId: String?
    
    init(id: String?, portfolioUrls: [String]?, avatarUrl: String?, description: String?, fullName: String?, services: [ServiceModel]?, userId: String? = nil) {
        self.fullName = fullName
        self.description = description
        self.avatarUrl = avatarUrl
        self.portfolioUrls = portfolioUrls
        self.id = id
        self.services = services
        self.userId = userId
    }
}
