//
//  historyData.swift
//  final-project
//
//  Created by Phuong Nguyen on 14/10/2021.
//

import Foundation

class Booking {
    let id: String?
    let code: String?
    let seller: Seller?
    let price: Double?
    let description: String?
    let status: Status?
    let catName: String?
    let createdAt: String?
    
    init(id: String?, code: String?, price: Double?, description: String?, seller: Seller?, status: Status?, categoryName: String?, createdAt: String?) {
        self.createdAt = createdAt
        self.id = id
        self.code = code
        self.price = price
        self.seller = seller
        self.description = description
        self.status = status
        self.catName = categoryName
    }
}
