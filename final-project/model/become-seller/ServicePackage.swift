//
//  ServicePackage.swift
//  final-project
//
//  Created by Phuong Nguyen on 28/08/2021.
//

import Foundation

class ServicePackage {
//    var let
    var unit: String?
    var quantity: Double?
    var price: Double?
    var deliverables: [String] = []
    var description: String?
    var packageType: PackageType
    
    init(unit: String?, quantity: Double?, deliverables: [String]?, packageType: PackageType, description: String, price: Double?) {
        self.unit = unit
        self.deliverables = deliverables ?? []
        self.packageType = packageType
        self.description = description
        self.quantity = quantity
        self.price = price
    }
    
}
