//
//  PortfolioItem.swift
//  final-project
//
//  Created by Phuong Nguyen on 22/08/2021.
//

import Foundation
import UIKit

class PortfolioItem {
    var description: String?
    var imageItem: UIImage!
    
    init(imageItem: UIImage, description: String?) {
        self.imageItem = imageItem
        self.description = description
    }
}
