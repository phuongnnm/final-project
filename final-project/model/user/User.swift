//
//  User.swift
//  final-project
//
//  Created by phuong nguyen on 5/19/21.
//

import Foundation

class User {
    let id: String
    let email: String
    let role: String
    
    init(id: String, email: String, role: String) {
        self.id = id
        self.email = email
        self.role = role
    }
}

class JobCategory {
    let id: String?
    let categoryName: String?
    let categoryDescription: String?
    
    init (id: String?, categoryName: String?, categoryDescription: String?) {
        self.id = id
        self.categoryName = categoryName
        self.categoryDescription = categoryDescription
    }
}
