//
//  BaseViewController.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit

class App {
    enum RedirectFlow {
        case BecomeSeller
        case Booking
        case Home
    }
}
class BaseViewController: UIViewController, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    let imgPicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view?.backgroundColor = UIColor(red: 0.99, green: 0.80, blue: 0.43, alpha: 1.00)
        configNav()
        self.hideKeyboardWhenTappedAround()
    }
    
    func configNav() {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtn
    }
    
    func showImagePicker() {
        imgPicker.delegate = self
        imgPicker.allowsEditing = true
        imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true, completion: nil)
    }
}

