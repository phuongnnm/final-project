//
//  UserManager.swift
//  final-project
//
//  Created by Phuong Nguyen on 28/08/2021.
//

import Foundation

class UserManager {
    static var shared = UserManager()
    
    func login(email: String, password: String, completionBlock: @escaping (Bool) -> Void) -> Bool {
        var res: Bool = false
        var input = UserPayloadInput(email: email, password: password)
        Network.shared.apollo.perform(mutation: LoginMutation(user: input)) {
            result in
            switch result {
            case .success(let graphQLResult):
//                if let graphQLResult = graphQLResult.data?.login!
//                print(graphQLResult)
                if let id = graphQLResult.data?.login?.user?.id,
                   let role = graphQLResult.data?.login?.user?.role,
                   let email = graphQLResult.data?.login?.user?.email,
                   let token = graphQLResult.data?.login?.token {
                    
                    UserDefaults.standard.set(token, forKey: "accessToken")
                    let token = UserDefaults.standard.string(forKey: "accessToken")
                    SQLiteCommands.insertRowUser(User(id: id, email: email, role: role.rawValue))
                    res = true
                    completionBlock(res)
                } else {
                    completionBlock(false)
                }
            case .failure(let error):
                print("Failure! Error: \(error)")
                print("fail")
                res = false
                completionBlock(res)
            }
        }
        return res
    }
    
    func logout() {
        let user = SQLiteCommands.getCurrentUserInfo()
        if let user = user {
            let res = SQLiteCommands.deleteUser(at: user.id)
            UserDefaults.standard.removeObject(forKey: "accessToken")
        }
        
    }
}
