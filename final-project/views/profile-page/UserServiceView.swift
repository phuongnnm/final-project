//
//  UserServiceView.swift
//  final-project
//
//  Created by phuong nguyen on 7/1/21.
//

import UIKit

class UserServiceView: UIView {
    @IBOutlet weak var serviceTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        serviceTableView.register(UINib(nibName: "ServicesItemCell", bundle: nil), forCellReuseIdentifier: "ServicesItemCell")
    }
    
    func setDelegateAndDataSourceTableView(delegate: UITableViewDelegate, dataSource: UITableViewDataSource) {
        serviceTableView.delegate = delegate
        serviceTableView.dataSource = dataSource
        serviceTableView.reloadData()
    }
}
