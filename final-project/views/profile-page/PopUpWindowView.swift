//
//  PopUpWindowView.swift
//  final-project
//
//  Created by Phuong Nguyen on 23/11/2021.
//

import UIKit

class PopUpWindowView: UIView {
    
    let popupView = UIView(frame: CGRect.zero)
    let popupTitle = UILabel(frame: CGRect.zero)
    let popupText = UILabel(frame: CGRect.zero)
    let popupButton = UIButton(frame: CGRect.zero)
    let descTv = UITextView(frame: CGRect.zero)
    let BorderWidth: CGFloat = 2.0
    
    init() {
        super.init(frame: CGRect.zero)
        // Semi-transparent background
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        // Popup Background
        popupView.backgroundColor = .white
        popupView.layer.cornerRadius = 12.0
        popupView.layer.masksToBounds = true
        popupView.clipsToBounds = true
        
        // Popup Title
        popupTitle.textColor = UIColor.darkGray
        popupTitle.backgroundColor = UIColor.white
        popupTitle.layer.masksToBounds = true
        popupTitle.adjustsFontSizeToFitWidth = true
        popupTitle.clipsToBounds = true
        popupTitle.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        popupTitle.numberOfLines = 1
        popupTitle.textAlignment = .center
        
        // TextView
        descTv.layer.borderWidth = 0.4
        descTv.layer.borderColor = UIColor.black.cgColor
        descTv.layer.cornerRadius = 12.0
        descTv.clipsToBounds = true
        descTv.font = .systemFont(ofSize: 18.5)
        descTv.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(descTv)
        
        
        // Popup Button
        popupButton.setTitleColor(UIColor.white, for: .normal)
        popupButton.titleLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        popupButton.backgroundColor = UIColor.orange
        popupView.addSubview(popupTitle)
        
        popupView.addSubview(popupButton)
        
        // Add the popupView(box) in the PopUpWindowView (semi-transparent background)
        addSubview(popupView)
        
        
        // PopupView constraints
        popupView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupView.widthAnchor.constraint(equalToConstant: 310),
            popupView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            popupView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
        
        // PopupTitle constraints
        popupTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupTitle.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: BorderWidth),
            popupTitle.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -BorderWidth),
            popupTitle.topAnchor.constraint(equalTo: popupView.topAnchor, constant: BorderWidth),
            popupTitle.heightAnchor.constraint(equalToConstant: 55)
        ])
        
        
        // PopupText constraints
        
        NSLayoutConstraint.activate([
            descTv.heightAnchor.constraint(greaterThanOrEqualToConstant: 170),
            descTv.topAnchor.constraint(equalTo: popupTitle.bottomAnchor, constant: 8),
            descTv.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 15),
            descTv.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -15),
            descTv.bottomAnchor.constraint(equalTo: popupButton.topAnchor, constant: -20)
        ])
        
        
        // PopupButton constraints
        popupButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            popupButton.heightAnchor.constraint(equalToConstant: 44),
            popupButton.leadingAnchor.constraint(equalTo: popupView.leadingAnchor),
            popupButton.trailingAnchor.constraint(equalTo: popupView.trailingAnchor),
            popupButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor)
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol DescPopupDelegate {
    func updateDesc(description: String)
}

class PopUpWindow: UIViewController {
    var delegate: DescPopupDelegate?
    var sellerId: String?
    private let popUpWindowView = PopUpWindowView()
    
    init(title: String, buttontext: String, sellerId: String) {
        super.init(nibName: nil, bundle: nil)
        self.sellerId = sellerId
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        popUpWindowView.popupTitle.text = title
        popUpWindowView.popupButton.setTitle(buttontext, for: .normal)
        popUpWindowView.popupButton.addTarget(self, action: #selector(updateDescription), for: .touchUpInside)
        popUpWindowView.addGestureRecognizer(tap)
        popUpWindowView.isUserInteractionEnabled = true
        view = popUpWindowView
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func updateDescription() {
        if let sellerId = sellerId {
            if  !popUpWindowView.descTv.text.isEmpty {
                Network.shared.apollo.perform(mutation: UpdateSellerProfileMutation(id: sellerId, avatarUrl: nil, description:  popUpWindowView.descTv.text, portfolioPhotoUrls: nil)) {
                    [weak self] res in
                    guard let self = self else { return }
                    
                    switch res {
                    case .success (let response):
                        if let data = response.data?.updateSellerProfile{
                            if let desc = data.resultMap["description"] as! String? {
                                self.delegate?.updateDesc(description: desc)
                            }
                        }
                        break;
                    case .failure(let err):
                        print(err.localizedDescription)
                        break;
                    }
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                let alert = UIAlertController(title: "Invalid Input", message: "Description cannot be empty", preferredStyle: .alert)
                let act1 = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(act1)
                self.present(alert, animated: true, completion: nil)
            }
        }

    }
}
