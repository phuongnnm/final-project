//
//  ServicesItemCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/2/21.
//

import UIKit

class ServicesItemCell: UITableViewCell {

    @IBOutlet weak var serviceMinPriceLabel: UILabel!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var providerAva: UIImageView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.cornerRadius = 20.0
        container.layer.shadowColor = UIColor.lightGray.cgColor
        container.layer.shadowOpacity = 0.6
        container.layer.shadowOffset = .zero
        container.layer.shadowRadius = 5
        productImageView.layer.cornerRadius = 20
//        container.layer.shadowPath = UIBezierPath(rect: container.bounds).cgPath
        container.layer.shouldRasterize = true
        container.layer.rasterizationScale = UIScreen.main.scale
        productImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        productImageView.clipsToBounds = true
        providerAva.makeRounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
