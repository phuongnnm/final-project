//
//  UserProfileView.swift
//  final-project
//
//  Created by phuong nguyen on 7/1/21.
//

import UIKit

protocol SellerProfileProtocol {
    func avatarClick()
    func editBtnClick()
    func portfolioClick()
}

class UserProfileView: UIView {
    var delegate: SellerProfileProtocol?
    @IBOutlet weak var editBtnView: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var portfolioCollection: UICollectionView!
    @IBOutlet weak var avatarImgView: UIImageView!
    @IBOutlet weak var contenView: UIView!
    var cellWidth: CGFloat = 0
    @IBAction func editBtnClick(_ sender: UIButton) {
        delegate?.editBtnClick()
    }
    @IBAction func avatarClick(_ sender: UIButton) {
        delegate?.avatarClick()
    }
    
    @IBOutlet weak var avaBtnView: UIButton!
    @IBOutlet weak var portfolioBtnView: UIButton!
    @IBAction func portfolioBtnClick(_ sender: UIButton) {
        delegate?.portfolioClick()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configView()
        
        cellWidth = (portfolioCollection.frame.size.width - 40) / 2
        let height = (portfolioCollection.frame.size.height - 20) / 2
        let layout = portfolioCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: height)
    }
    
    func configView() {
        portfolioCollection.register(UINib(nibName: "PortfolioCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PortfolioCollectionViewCell")
        
        avatarImgView.makeRounded()
    }
    
    func setDelegateAndDataSourceForCollectionView(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        self.portfolioCollection.delegate = delegate
        self.portfolioCollection.dataSource = dataSource
        portfolioCollection.reloadData()
    }

}
