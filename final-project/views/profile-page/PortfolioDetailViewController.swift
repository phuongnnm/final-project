//
//  PortfolioDetailViewController.swift
//  final-project
//
//  Created by phuong nguyen on 7/2/21.
//

import UIKit
import Nuke 
class PortfolioDetailViewController: UIViewController {

    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var descriptionView: UILabel!
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let url = url {
            NukeLoad.loadContent(url: url, imageView: pictureView)
        }
    }
}

class NukeLoad {
    static func loadContent(url: URL, imageView: UIImageView) {
        var resizedImageProcessors: [ImageProcessing] {
          let imageSize = CGSize(width: 350, height: 350)
          return [ImageProcessors.Resize(size: imageSize, contentMode: .aspectFill)]
        }
        
        let options = ImageLoadingOptions(
            placeholder: UIImage(named: "loading2"),
            transition: .fadeIn(duration: 0.5)
        )
        
        let request = ImageRequest(
          url: url,
          processors: resizedImageProcessors)

        Nuke.loadImage(with: request, options: options, into: imageView)
    }
}
