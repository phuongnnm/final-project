//
//  PackageInfoView.swift
//  final-project
//
//  Created by phuong nguyen on 7/19/21.
//

import UIKit

class PackageInfoView: UIView {
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var priceTf: UITextField!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var unitTf: UITextField!
    @IBOutlet weak var quantityTf: UITextField!
    @IBOutlet weak var packageDescTextView: UITextView!
    @IBOutlet weak var desc1Lbl: UILabel!
    @IBOutlet weak var deliverTf: UITextField!
    @IBOutlet weak var deliverTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldConf(tf: unitTf)
        textFieldConf(tf: quantityTf)
        textFieldConf(tf: deliverTf)
        textFieldConf(tf: priceTf)
        quantityTf.translatesAutoresizingMaskIntoConstraints = false
        priceTf.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            quantityTf.heightAnchor.constraint(equalTo: unitTf.heightAnchor),
            quantityTf.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
            quantityTf.centerYAnchor.constraint(equalTo: quantityLbl.centerYAnchor),
            quantityTf.widthAnchor.constraint(equalTo: unitTf.widthAnchor),
            
            separatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
        ])
        
        packageDescTextView.layer.borderWidth = 0.3
        packageDescTextView.layer.cornerRadius = 12.0
        packageDescTextView.clipsToBounds = true
        deliverTf.translatesAutoresizingMaskIntoConstraints = false
        deliverTableView.translatesAutoresizingMaskIntoConstraints = false
        deliverTableView.register(UINib(nibName: "DeliberablesTableViewCell", bundle: nil), forCellReuseIdentifier: "DeliberablesTableViewCell")
    }
    
    func setDelegateDataSourceDeleverTableView(delegate: UITableViewDelegate, dataSource: UITableViewDataSource) {
        deliverTableView.delegate = delegate
        deliverTableView.dataSource = dataSource
        deliverTableView.reloadData()
    }
    
    func textFieldConf(tf: UITextField) {
        tf.layer.borderWidth = 0.3
        tf.layer.cornerRadius = 12.0
        tf.clipsToBounds = true
    }
}
