//
//  ServicePackageView.swift
//  final-project
//
//  Created by phuong nguyen on 7/19/21.
//

import UIKit

class ServicePackageView: UIView {
    let regularCheckBox = CheckBoxImgView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    let standardCheckBox = CheckBoxImgView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    let premiumCheckBox = CheckBoxImgView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    var labels: [UILabel] = []
    
    var standardView: UIView!
    var premiumView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        generateViews()
    }
    
    func generateViews() {
        standardView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width - 30, height: 30))
        premiumView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width - 30, height: 30))
        standardView.translatesAutoresizingMaskIntoConstraints = false
        premiumView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(standardView)
        self.addSubview(premiumView)
        regularCheckBox.translatesAutoresizingMaskIntoConstraints = false
        standardCheckBox.translatesAutoresizingMaskIntoConstraints = false
        premiumCheckBox.translatesAutoresizingMaskIntoConstraints = false
        regularCheckBox.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        premiumCheckBox.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        let config = UIImage.SymbolConfiguration(scale: .large)
        regularCheckBox.image = UIImage(systemName: "checkmark", withConfiguration: config)
        
        self.addSubview(regularCheckBox)
        standardView.addSubview(standardCheckBox)
        premiumView.addSubview(premiumCheckBox)
        
        NSLayoutConstraint.activate([
            regularCheckBox.heightAnchor.constraint(equalToConstant: regularCheckBox.frame.width),
            regularCheckBox.widthAnchor.constraint(equalToConstant: regularCheckBox.frame.width),
            regularCheckBox.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            regularCheckBox.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
        ])
        
        for i in 0..<3 {
            let titles = ["Regular package (required)", "Standard package (optional)", "Premium package (optional)"]
            let label = UILabel(frame: CGRect.zero)
            label.text = titles[i]
            label.textColor = .darkGray
            label.translatesAutoresizingMaskIntoConstraints = false
            labels.append(label)
        }
        self.addSubview(labels[0])
        standardView.addSubview(labels[1])
        premiumView.addSubview(labels[2])
        
        NSLayoutConstraint.activate([
            labels[0].leadingAnchor.constraint(equalTo: regularCheckBox.trailingAnchor, constant: 10),
            labels[0].centerYAnchor.constraint(equalTo: regularCheckBox.centerYAnchor),
            labels[0].trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
            labels[0].heightAnchor.constraint(equalToConstant: 35)
        ])
        
        NSLayoutConstraint.activate([
            standardCheckBox.leadingAnchor.constraint(equalTo: standardView.leadingAnchor),
            standardCheckBox.topAnchor.constraint(equalTo: standardView.topAnchor),
            standardCheckBox.heightAnchor.constraint(equalToConstant: standardCheckBox.frame.height),
            standardCheckBox.widthAnchor.constraint(equalToConstant: standardCheckBox.frame.height),
            
            labels[1].leadingAnchor.constraint(equalTo: standardCheckBox.trailingAnchor, constant: 12),
            labels[1].trailingAnchor.constraint(equalTo: standardView.trailingAnchor),
            labels[1].heightAnchor.constraint(equalToConstant: standardView.frame.height)
        ])
        
        NSLayoutConstraint.activate([
            premiumCheckBox.leadingAnchor.constraint(equalTo: premiumView.leadingAnchor),
            premiumCheckBox.topAnchor.constraint(equalTo: premiumView.topAnchor),
            premiumCheckBox.heightAnchor.constraint(equalToConstant: premiumCheckBox.frame.height),
            premiumCheckBox.widthAnchor.constraint(equalToConstant: premiumCheckBox.frame.height),
            
            labels[2].leadingAnchor.constraint(equalTo: premiumCheckBox.trailingAnchor, constant: 12),
            labels[2].trailingAnchor.constraint(equalTo: premiumView.trailingAnchor),
            labels[2].heightAnchor.constraint(equalToConstant: premiumView.frame.height)
        ])
        
    }
    
}
