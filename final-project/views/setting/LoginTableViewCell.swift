//
//  LoginTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 5/6/21.
//

import UIKit

protocol LoginCellDelegate {
    func loginBtnTap(cell: LoginTableViewCell)
    func resetForm(cell: LoginTableViewCell)
    func signInTap()
}

class LoginTableViewCell: UITableViewCell, UITextFieldDelegate {
    var delegate: LoginCellDelegate?
    @IBAction func signInBtnTap(_ sender: UIButton) {
        delegate?.signInTap()
    }
    @IBOutlet weak var errorMessLbl: UILabel!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    
    
    @IBAction func loginBtnClick(_ sender: UIButton) {
        delegate?.loginBtnTap(cell: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.emailTf.layer.cornerRadius = 12.0
        self.emailTf.layer.masksToBounds = true
        self.passwordTf.layer.cornerRadius = 12.0
        self.passwordTf.layer.masksToBounds = true
        
        self.passwordTf.layer.borderWidth = 0.5
        self.emailTf.layer.borderWidth = 0.5
        self.cleanUpView()
    }
    
    func cleanUpView () {
        self.errorMessLbl?.text = ""
        self.errorMessLbl?.textColor = UIColor.red
        self.passwordTf?.text = ""
        self.emailTf?.text = ""
    }
    
    func displayError() {
        self.errorMessLbl.text = "Incorrect email or password, please try again"
        self.passwordTf.layer.borderWidth = 1.2
        self.emailTf.layer.borderWidth = 1.2
        self.passwordTf.layer.borderColor = UIColor.red.cgColor
        self.emailTf.layer.borderColor = UIColor.red.cgColor
    }
}
