//
//  FooterTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit

class FooterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellView?.backgroundColor = UIColor(red: 0.99, green: 0.80, blue: 0.43, alpha: 1.00)
    }
    
    
}
