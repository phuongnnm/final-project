//
//  BookingHistoryTableViewCell.swift
//  final-project
//
//  Created by Phuong Nguyen on 30/09/2021.
//

import UIKit

class BookingHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var sellerAvaImgView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var serviceDescLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func displayDate(dateString: String) {
        let date = Utilitites.formateDateFromString(dateString: dateString)
        dateLabel.text = date
    }
    
    func displayStatus(status: Status) {
        switch status {
        case .inProgress:
            statusLabel.textColor = UIColor(red: 0.07, green: 0.45, blue: 0.87, alpha: 1.00)
        case .cancelled:
            statusLabel.textColor = .red
        case .finished:
            statusLabel.textColor = UIColor(red: 0.00, green: 0.55, blue: 0.01, alpha: 1.00)
        default:
            break;
        }
        statusLabel.text = status.rawValue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
