//
//  BookingConfirmViewController.swift
//  final-project
//
//  Created by Phuong Nguyen on 29/09/2021.
//

import UIKit
class ConfirmViewModel {
    let categoryName: String
    var avaUrl: String?
    let sellerName: String
    let packageType: PackageType
    let totalPrice: Double
    var quantity: Double?
    let description: String
    var unit: String?
    
    init(categoryName: String, sellerName: String, sellerAva: String?, totalPrice: Double, quantity: Double?, packageType: PackageType, description: String, unit: String?) {
        self.description = description
        self.categoryName = categoryName
        self.avaUrl = sellerAva
        self.quantity = quantity
        self.totalPrice = totalPrice
        self.packageType = packageType
        self.sellerName = sellerName
        self.unit = unit
    }
}
class BookingConfirmViewController: BaseViewController {

    @IBOutlet weak var finishBtnView: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sellerAvaImgView: UIImageView!
    @IBOutlet weak var noteTf: UITextView!
    
    var userId: String?
    var packageId: String?
    var viewModel: ConfirmViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        configView()
        generateView()
    }
    
    @IBAction func finishBtnClick(_ sender: UIButton) {
        let uniqueCode = Utilitites.generateCode()
        let bookingInput = BookingInput(note: noteTf.text, code: uniqueCode, customerQuantity: viewModel?.quantity)
        guard let id = userId, let packageId = self.packageId else {return}
        
        Network.shared.apollo.perform(mutation: CreateBookingMutation(booking: bookingInput, packageId: packageId, userId: id)) {
            res in
            switch(res) {
            case .success(let response):
                if let data = response.data?.createBooking {
                    let status = data.resultMap["status"] as? Int
                    let message = data.resultMap["message"] as? String
                    self.handleStatus(statusCode: status!, message: message!)
                }
                break;
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    @IBAction func cancelBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func configView() {
        view.backgroundColor = .white
        noteTf.layer.borderWidth = 0.7
        noteTf.layer.borderColor = UIColor.black.cgColor
        noteTf.layer.cornerRadius = 12.0
        noteTf.clipsToBounds = true
        sellerAvaImgView.makeRounded()
    }
    
    func generateView() {
        if let viewModel = self.viewModel {
            let dprice = Utilitites.formatPrice(price: viewModel.totalPrice)
            totalPriceLabel.text = "\(dprice) VND"
            finishBtnView.setTitle("Finish (\(dprice) VND)", for: .normal)
            descriptionLabel.text = viewModel.description
            packageLabel.text = "\(viewModel.packageType)".uppercased()
            sellerNameLabel.text = viewModel.sellerName
            categoryLabel.text = viewModel.categoryName
            if let quantity = viewModel.quantity {
                quantityLabel.text = "\(String(describing: quantity)) \(viewModel.unit ?? "")"
            } else {
                quantityLabel.text = "none"
            }
            if let url = URL(string: viewModel.avaUrl ?? "") {
                NukeLoad.loadContent(url: url, imageView: self.sellerAvaImgView)
            }
        }
    }
    
    func handleStatus(statusCode: Int, message: String) {
        var alert: UIAlertController?
        switch statusCode {
        case 0:
            alert = UIAlertController(title: "Order Success", message: message, preferredStyle: .alert)
            
        default:
            alert = UIAlertController(title: "Something's wrong", message: message, preferredStyle: .alert)
        }
        
        self.present(alert!, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 2.1
        DispatchQueue.main.asyncAfter(deadline: when){
            alert!.dismiss(animated: true, completion: nil)
        }
    }

}
