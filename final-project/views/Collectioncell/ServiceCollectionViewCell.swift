//
//  ServiceCollectionViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/8/21.
//

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        jobImageView.layer.cornerRadius = 15.0
        jobImageView.clipsToBounds = true
        jobTitleLabel.layer.cornerRadius = 13.0
        jobTitleLabel.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        jobTitleLabel.clipsToBounds = true
        
    }
}
