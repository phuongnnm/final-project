//
//  PortfolioUploadView.swift
//  final-project
//
//  Created by phuong nguyen on 7/15/21.
//

import UIKit

protocol PortfolioViewDelegate {
    func addImage()
}

class PortfolioUploadView: UIView {
    var delegate: PortfolioViewDelegate?
    
    @IBOutlet weak var imageTableView: UITableView!
    
    @IBAction func addImgClick(_ sender: UIButton) {
        delegate?.addImage()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        imageTableView.register(UINib(nibName: "PortfolioImageTableViewCell", bundle: nil), forCellReuseIdentifier: "PortfolioImageTableViewCell")
    }
    
    func setDelegateAndDataSouce(delegate: UITableViewDelegate, dataSource: UITableViewDataSource) {
        imageTableView.delegate = delegate
        imageTableView.dataSource = dataSource
        imageTableView.reloadData()
    }
    
}
