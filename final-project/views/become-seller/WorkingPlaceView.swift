//
//  WorkingPlaceView.swift
//  final-project
//
//  Created by phuong nguyen on 7/17/21.
//

import UIKit

protocol WorkingPlaceDelegate {
    func animatePage()
}

class WorkingPlaceView: UIView {
    var delegate: WorkingPlaceDelegate?
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var descLabel: UILabel!
    var collectionViewHeight: NSLayoutConstraint!
    var contentViewBottomAnChor: NSLayoutConstraint!
    var placeCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    var cityLabel = UILabel()
    var isShow = false
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let btn = CheckBoxImgView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btn.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(btn)
        NSLayoutConstraint.activate([
            btn.topAnchor.constraint(equalTo: descLabel.bottomAnchor, constant: 40),
            btn.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            btn.heightAnchor.constraint(equalToConstant: btn.frame.height),
            btn.widthAnchor.constraint(equalToConstant: btn.frame.width)
        ])
        
        let anyPlaceLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 90, height: btn.frame.height))
        anyPlaceLabel.text = "Any places"
        anyPlaceLabel.font = UIFont(name: "HelveticaNeue", size: 16)
        anyPlaceLabel.textColor = .darkGray
        anyPlaceLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(anyPlaceLabel)
        
        NSLayoutConstraint.activate([
            anyPlaceLabel.leadingAnchor.constraint(equalTo: btn.trailingAnchor, constant: 10),
            anyPlaceLabel.centerYAnchor.constraint(equalTo: btn.centerYAnchor),
            anyPlaceLabel.heightAnchor.constraint(equalToConstant: anyPlaceLabel.frame.height),
            anyPlaceLabel.widthAnchor.constraint(equalToConstant: anyPlaceLabel.frame.width)
        ])
        
        let descLabel2 = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 30, height: 45))
        
        descLabel2.text = "Note: Please choose \"Any Places\" if your product is shippable anywhere."
        descLabel2.font = UIFont(name: "HelveticaNeue", size: 14)
        descLabel2.textColor = .gray
        descLabel2.translatesAutoresizingMaskIntoConstraints = false
        descLabel2.numberOfLines = 2
        contentView.addSubview(descLabel2)
        NSLayoutConstraint.activate([
            descLabel2.topAnchor.constraint(equalTo: btn.bottomAnchor, constant: 3),
            descLabel2.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            descLabel2.heightAnchor.constraint(equalToConstant: descLabel2.frame.height),
            descLabel2.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
        
        cityLabel.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width - 30, height: 30)
        cityLabel.text = "+ Ho Chi Minh city"
        cityLabel.textColor = .darkGray
        cityLabel.font = UIFont(name: "HelveticaNeue", size: 25)
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.isUserInteractionEnabled = true
        cityLabel.addGestureRecognizer(tap)
        contentView.addSubview(cityLabel)
        
        NSLayoutConstraint.activate([
            cityLabel.topAnchor.constraint(equalTo: descLabel2.bottomAnchor, constant: 20),
            cityLabel.leadingAnchor.constraint(equalTo: descLabel2.leadingAnchor),
            cityLabel.heightAnchor.constraint(equalToConstant: cityLabel.frame.height),
            cityLabel.trailingAnchor.constraint(equalTo: descLabel2.trailingAnchor)
        ])
        
        placeCollectionView.register(UINib(nibName: "PlacesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlacesCollectionViewCell")
        let cellWidth = contentView.frame.size.width/2 - 40
        placeCollectionView.backgroundColor = .white
        let layout = placeCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: 65)
        layout.minimumLineSpacing = 0
        contentView.addSubview(placeCollectionView)
        placeCollectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionViewHeight = placeCollectionView.heightAnchor.constraint(equalToConstant: 0)
        collectionViewHeight.isActive = true
        
        contentViewBottomAnChor = placeCollectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -260)
        contentViewBottomAnChor.isActive = true
        NSLayoutConstraint.activate([
            placeCollectionView.topAnchor.constraint(equalTo: cityLabel.bottomAnchor, constant: 10),
            placeCollectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 15),
            placeCollectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
        ])
    }
    
    func setDelegateAndDatasource(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        placeCollectionView.delegate = delegate
        placeCollectionView.dataSource = dataSource
        placeCollectionView.reloadData()
    }
    
    @objc func tapView() {
        isShow = !isShow
        if (isShow == true) {
            self.cityLabel.text = "- Ho Chi Minh city"
            collectionViewHeight.constant = 500
            contentViewBottomAnChor.constant = -10
            self.contentView.layoutIfNeeded()
        } else {
            self.cityLabel.text = "+ Ho Chi Minh city"
            collectionViewHeight.constant = 0
            contentViewBottomAnChor.constant = -260
            self.contentView.layoutIfNeeded()
        }
       
    }
}

class CheckBoxImgView: UIImageView {

    var isChecked = false {
        didSet {
            if isChecked == true {
                let config = UIImage.SymbolConfiguration(scale: .large)
                self.image = UIImage(systemName: "checkmark", withConfiguration: config)
            } else {
                self.image = nil
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.orange.cgColor
        self.tintColor = .orange
        self.contentMode = .center
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
