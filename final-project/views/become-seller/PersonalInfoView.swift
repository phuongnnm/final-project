//
//  PersonalInfoView.swift
//  final-project
//
//  Created by phuong nguyen on 7/14/21.
//

import UIKit

protocol PersonalInfoDelegate {
    func avatarClick()
}

class PersonalInfoView: UIView {
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var lastNameTf: UITextField!
    @IBOutlet weak var avaImgView: UIImageView!
    @IBOutlet weak var firstNameTf: UITextField!
    var delegate: PersonalInfoDelegate?
    @IBAction func avaBtnClick(_ sender: UIButton) {
        delegate?.avatarClick()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avaImgView.makeRounded()
        firstNameTf.layer.cornerRadius = 15.0
        firstNameTf.layer.borderWidth = 0.5
        firstNameTf.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        lastNameTf.layer.cornerRadius = 15.0
        lastNameTf.layer.borderWidth = 0.5
        lastNameTf.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        firstNameTf.clipsToBounds = true
        lastNameTf.clipsToBounds = true
        descTextView.isEditable = true
        descTextView.layer.cornerRadius = 15.0
        descTextView.layer.borderWidth = 0.7
        descTextView.clipsToBounds = true
    }
}
