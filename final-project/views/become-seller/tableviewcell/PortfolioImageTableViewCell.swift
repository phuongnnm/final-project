//
//  PortfolioImageTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/15/21.
//

import UIKit

protocol ImageCellDelegate {
    func removeBtnClick(index: Int)
}

class PortfolioImageTableViewCell: UITableViewCell {
    var delegate: ImageCellDelegate?
    var index = -1
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var portfolioImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        portfolioImageView.contentMode = .scaleAspectFill
        descTextView.isEditable = true
    }
    
    @IBAction func removeImgClick(_ sender: UIButton) {
        delegate?.removeBtnClick(index: index)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
