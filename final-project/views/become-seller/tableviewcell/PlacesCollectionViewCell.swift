//
//  PlacesCollectionViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/17/21.
//

import UIKit

class PlacesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    let checkBox = CheckBoxImgView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    let placeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 90, height: 30))
    override func awakeFromNib() {
        super.awakeFromNib()
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        placeLabel.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(checkBox)
        NSLayoutConstraint.activate([
            checkBox.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            checkBox.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            checkBox.heightAnchor.constraint(equalToConstant: checkBox.frame.height),
            checkBox.widthAnchor.constraint(equalToConstant: checkBox.frame.width)
        ])
        placeLabel.font = UIFont(name: "HelveticaNeue", size: 16)
        placeLabel.textColor = .darkGray
        placeLabel.numberOfLines = 2
        containerView.addSubview(placeLabel)
        NSLayoutConstraint.activate([
            placeLabel.centerYAnchor.constraint(equalTo: checkBox.centerYAnchor),
            placeLabel.leadingAnchor.constraint(equalTo: checkBox.trailingAnchor, constant: 10),
            placeLabel.heightAnchor.constraint(equalToConstant: contentView.frame.height),
            placeLabel.widthAnchor.constraint(equalToConstant: placeLabel.frame.width)
        ])
    }
}
