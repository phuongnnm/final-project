//
//  DeliberablesTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/25/21.
//

import UIKit

protocol DeliverableCellDeletgate {
    func deleteBtnClick(index: Int, tableView: UITableView)
}

class DeliberablesTableViewCell: UITableViewCell {
    var index = 0
    var tableView: UITableView!
    var delegate: DeliverableCellDeletgate?
    @IBAction func deleteBtnClick(_ sender: UIButton) {
        delegate?.deleteBtnClick(index: index, tableView: tableView)
    }
    @IBOutlet weak var deliverLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        deliverLabel.font = UIFont(name: "HelveticaNeue", size: 13.5)
        deliverLabel.textColor = #colorLiteral(red: 0.25, green: 0.25, blue: 0.25, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
