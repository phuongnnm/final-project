//
//  HomeTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceCollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpCollectionView() {
        serviceCollection.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
    }
    
    func setDelegateAndDataSourceCollectionView(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource, forRow row: Int) {
        serviceCollection.delegate = delegate
        serviceCollection.dataSource = dataSource
        serviceCollection.tag = row
        serviceCollection.reloadData()
    }
    
}
