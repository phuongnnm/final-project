//
//  DeliverableTableViewCell.swift
//  final-project
//
//  Created by Phuong Nguyen on 17/09/2021.
//

import UIKit

class DeliverableTableViewCell: UITableViewCell {

    @IBOutlet weak var deliverableLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        deliverableLabel.textColor = .darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
