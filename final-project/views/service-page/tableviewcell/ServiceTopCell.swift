//
//  ServiceTopCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/3/21.
//

import UIKit

class ServiceTopCell: UITableViewCell {

    @IBOutlet weak var serviceDescription: UILabel!
    
    @IBOutlet weak var providerDisplayName: UILabel!
    @IBOutlet weak var providerAva: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        providerAva.makeRounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
