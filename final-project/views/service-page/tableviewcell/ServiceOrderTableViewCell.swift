//
//  ServiceOrderTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/3/21.
//

import UIKit

protocol ServiceOrderProtocol {
    func goToConFirm()
}

class ServiceOrderTableViewCell: UITableViewCell, ServiceControllerDelegate {

    var delegate: ServiceOrderProtocol?
    
    @IBOutlet weak var quantityStack: UIStackView!
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBOutlet weak var quantityTf: UITextField!
    
    
    @IBOutlet weak var unitTf: UILabel!
    var tabbarCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    var deliverables: [String] = [] {
        didSet {
            deliverablesTableView.reloadData()
        }
    }
    var packages: [PackageModel] = []
    @IBOutlet weak var deliverablesTableView: UITableView!
    var viewTopAnchor: NSLayoutConstraint!
    var nextButton = UIButton(frame: CGRect.zero)
    var deliverablesHeightAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var contentContainerView: UIView!
    
    @IBOutlet weak var serviceIntro: UILabel!
    
    @IBOutlet weak var servicePriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        tabbarCollection.register(TabBarCollectionViewCell.self, forCellWithReuseIdentifier: "tabbarcell")
        setConstraints()
        setUpViews()
        quantityStack.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setConstraints() {
        deliverablesTableView.translatesAutoresizingMaskIntoConstraints = false
        deliverablesTableView.rowHeight = 23
        serviceIntro.translatesAutoresizingMaskIntoConstraints = false
        servicePriceLabel.translatesAutoresizingMaskIntoConstraints = false
        serviceTypeLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setUpViews() {
        var tabBarHeight = CGFloat(0);
        tabbarCollection.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: 50)
        tabBarHeight = tabbarCollection.frame.height
        
        tabbarCollection.tag = 10
        tabbarCollection.backgroundColor = .white
        tabbarCollection.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(tabbarCollection)
        NSLayoutConstraint.activate([
            tabbarCollection.topAnchor.constraint(equalTo: contentView.topAnchor),
            tabbarCollection.heightAnchor.constraint(equalToConstant: tabBarHeight),
            tabbarCollection.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tabbarCollection.leadingAnchor.constraint(equalTo: contentView.leadingAnchor)
        ])
        
        let width = UIScreen.main.bounds.width / 3
        let layout = tabbarCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: 50)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        contentContainerView.translatesAutoresizingMaskIntoConstraints = false
        viewTopAnchor = contentContainerView.topAnchor.constraint(equalTo: tabbarCollection.bottomAnchor, constant: CGFloat(1))
        viewTopAnchor.isActive = true
        
        NSLayoutConstraint.activate([
            contentContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            contentContainerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentContainerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            
            serviceTypeLabel.topAnchor.constraint(equalTo: contentContainerView.topAnchor, constant: CGFloat(15)),
            serviceTypeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: CGFloat(20)),
            serviceTypeLabel.heightAnchor.constraint(equalToConstant: 33),
            serviceTypeLabel.widthAnchor.constraint(equalToConstant: CGFloat(contentContainerView.frame.width / 2)),
            
            servicePriceLabel.trailingAnchor.constraint(equalTo: contentContainerView.trailingAnchor, constant: CGFloat(-20)),
            servicePriceLabel.topAnchor.constraint(equalTo: contentContainerView.topAnchor, constant: CGFloat(15)),
            
            serviceIntro.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: CGFloat(20)),
            serviceIntro.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: CGFloat(-20)),
            serviceIntro.topAnchor.constraint(equalTo: serviceTypeLabel.bottomAnchor, constant: CGFloat(8)),
        ])
        
        let label1 = UILabel()
        
        label1.text = "This package includes"
        label1.font = UIFont.systemFont(ofSize: CGFloat(16))
        label1.translatesAutoresizingMaskIntoConstraints = false
        contentContainerView.addSubview(label1)
        
        NSLayoutConstraint.activate([
            label1.topAnchor.constraint(equalTo: serviceIntro.bottomAnchor, constant: 15),
            label1.trailingAnchor.constraint(equalTo: contentContainerView.trailingAnchor, constant: CGFloat(20)),
            label1.leadingAnchor.constraint(equalTo: contentContainerView.leadingAnchor, constant: CGFloat(20)),
            label1.heightAnchor.constraint(equalToConstant: 32)
        ])
        
        contentContainerView.addSubview(deliverablesTableView)
        deliverablesHeightAnchor = deliverablesTableView.heightAnchor.constraint(equalToConstant: 96)
        deliverablesHeightAnchor.isActive = true
        NSLayoutConstraint.activate([
            deliverablesTableView.topAnchor.constraint(equalTo: label1.bottomAnchor, constant: CGFloat(8)),
            deliverablesTableView.leadingAnchor.constraint(equalTo: contentContainerView.leadingAnchor, constant: CGFloat(20)),
            deliverablesTableView.trailingAnchor.constraint(equalTo: contentContainerView.trailingAnchor)
        ])
        
        quantityStack.translatesAutoresizingMaskIntoConstraints = false
        quantityStack.spacing = 10
        
        
        quantityStack.trailingAnchor.constraint(equalTo: contentContainerView.trailingAnchor, constant: -10).isActive = true
        
        quantityStack.topAnchor.constraint(equalTo: deliverablesTableView.bottomAnchor, constant: 10).isActive = true
        quantityStack.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        nextButton = UIButton(frame: CGRect(x: 0, y: 0, width: contentContainerView.bounds.width - 40, height: CGFloat(38)))
        nextButton.setTitle("Continue (\(String(describing: servicePriceLabel.text!)))", for: .normal)
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.layer.cornerRadius = 12.0
        nextButton.clipsToBounds = true
        contentContainerView.addSubview(nextButton)
        nextButton.addTarget(self, action: #selector(goToNextStep), for: .touchUpInside)
        nextButton.backgroundColor = .orange
        nextButton.setTitleColor(.white, for: .normal)

        NSLayoutConstraint.activate([
            nextButton.heightAnchor.constraint(equalToConstant: CGFloat(50)),
            nextButton.trailingAnchor.constraint(equalTo: contentContainerView.trailingAnchor, constant: CGFloat(-20)),
            nextButton.bottomAnchor.constraint(equalTo: contentContainerView.bottomAnchor, constant: CGFloat(-20)),
            nextButton.leadingAnchor.constraint(equalTo: contentContainerView.leadingAnchor, constant: CGFloat(20)),
            nextButton.topAnchor.constraint(equalTo: quantityStack.bottomAnchor, constant: CGFloat(15)),
        ])
    }
    
    @objc func goToNextStep() {
        delegate?.goToConFirm()
    }

    func setDelegationForCollectionView(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        tabbarCollection.delegate = delegate
        tabbarCollection.dataSource = dataSource
        tabbarCollection.reloadData()
        tabbarCollection.selectItem(at: [0, 0], animated: true, scrollPosition: [])
        
    }
    
    func displayInfoWhenTapped(row: Int, packages: [PackageModel]) {
        switch  row {
        case 0:
            serviceTypeLabel.text = "Regular Service"
            displayPackageInfo(package: packages[0])
        case 1:
            serviceTypeLabel.text = "Standard Service"
            displayPackageInfo(package: packages[1])
        case 2:
            serviceTypeLabel.text = "Premium Service"
            displayPackageInfo(package: packages[2])
        default:
            serviceTypeLabel.text = "Regular Service"
            displayPackageInfo(package: packages[0])
        }
    }
    
    func displayPackageInfo(package: PackageModel) {
        if let deliverables = package.deliverables {
            self.deliverables = deliverables
        }
        if let price = package.price {
            let dPrice = Utilitites.formatPrice(price: Double(price))
            
            servicePriceLabel.text = "\(dPrice) VND"
            nextButton.setTitle("Continue (\(String(describing: servicePriceLabel.text!)))", for: .normal)
        }
        
        if let quantity = package.quantity {
            quantityStack.isHidden = false
            quantityTf.text = String(quantity)
        } else {
            quantityStack.isHidden = true
        }
        
        if let description = package.description {
            serviceIntro.text = description
        }
    }
    
    func setDelegateAndDataSourceForDeliTableView(delegate: UITableViewDelegate, dataSource: UITableViewDataSource, tfDelegate: UITextFieldDelegate) {
        self.quantityTf.delegate = tfDelegate
        self.deliverablesTableView.delegate = delegate
        self.deliverablesTableView.dataSource = dataSource
        self.deliverablesTableView.reloadData()
    }
    
    func calculatePrice(quantity: Double, row: Int) -> Double {
        if let defaultQuantity = packages[row].quantity,
           let price = packages[row].price{
            if quantity <= defaultQuantity {
                    return Double(price)
            }
            return  Double(price) / defaultQuantity * quantity
        }
        return 0.0
    }
}
