//
//  ProductImgSlideTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 7/3/21.
//

import UIKit

class ProductImgSlideTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageCollection.register(UINib(nibName: "ProductImgCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductImgCollectionViewCell")
        imageCollection.frame(forAlignmentRect:  CGRect(x: 0, y: 0, width: contentView.frame.width - 40, height: contentView.frame.height - 20))
        
        let cellWidth = (contentView.frame.size.width + 40) / 3
        let layout = imageCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellWidth)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setDelegateAndDataSource(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        imageCollection.delegate = delegate
        imageCollection.dataSource = dataSource
        imageCollection.reloadData()
    }
    
}
