//
//  LabourTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/18/21.
//

import UIKit

class LabourTableViewCell: UITableViewCell {
    @IBOutlet weak var serviceName: UILabel!
    
    @IBOutlet weak var serviceDesc: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
