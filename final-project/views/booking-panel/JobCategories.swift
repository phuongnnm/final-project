//
//  JobCategories.swift
//  final-project
//
//  Created by phuong nguyen on 6/16/21.
//

import UIKit

class JobCategoriesView: UIView {
    fileprivate var view2HeightAnchor: NSLayoutConstraint!
    fileprivate var view4HeightAnchor: NSLayoutConstraint!
    fileprivate var tableView1Height: NSLayoutConstraint!
    fileprivate var tableView2Height: NSLayoutConstraint!
    
    var view2ToggleOn = false
    var view4ToggleOn = false

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView:UIView!
    var view1 = UIView()
    var view2 = UIView()
    var view3 = UIView()
    var view4 = UIView()
    var labourImgView = UIImageView()
    var boxImgView = UIImageView()
    var labourServiceTableView = UITableView()
    var productServiceTableView = UITableView()
   
    override func awakeFromNib() {
        super.awakeFromNib()
        createView()
        createTableViews()
        labourServiceTableView.backgroundColor = .yellow
        let touchInsideView1 = UITapGestureRecognizer(target: self, action: #selector(tapView1))
            let touchInsideView3 = UITapGestureRecognizer(target: self, action: #selector(tapView3))
        view1.addGestureRecognizer(touchInsideView1)
        view3.addGestureRecognizer(touchInsideView3)
        
    }
    
    @objc func tapView1() {
        view2ToggleOn = !view2ToggleOn
        if (view2ToggleOn == true) {
            labourImgView.tintColor = GlobalDesign.backgroundColor
            view2HeightAnchor.constant = 64*3
            tableView1Height.constant = 64*3
        } else {
            labourImgView.tintColor = .darkGray
            view2HeightAnchor.constant = 0
            tableView1Height.constant = 0
        }
        
        UIView.animate(withDuration: 0.7) {
                self.contentView.layoutIfNeeded()
        }
    }
    
    @objc func tapView3() {
        view4ToggleOn = !view4ToggleOn
        if (view4ToggleOn == true) {
            boxImgView.tintColor = GlobalDesign.backgroundColor
            view4HeightAnchor.constant = 64*6
            tableView2Height.constant = 64*6
        } else {
            boxImgView.tintColor = .darkGray
            tableView2Height.constant = 0
            view4HeightAnchor.constant = 0
        }
        
        UIView.animate(withDuration: 0.7) {
                self.contentView.layoutIfNeeded()
        }
    }
    
    func createTableViews() {
        labourServiceTableView = UITableView(frame: CGRect(x: 0, y: 0, width: view1.frame.width, height: view2.frame.height))
        labourServiceTableView.register(UINib(nibName: "LabourTableViewCell", bundle: nil), forCellReuseIdentifier: "LabourTableViewCell")
        view2.addSubview(labourServiceTableView)
        labourServiceTableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView1Height = labourServiceTableView.heightAnchor.constraint(equalToConstant: labourServiceTableView.frame.height)
        tableView1Height.isActive = true
        NSLayoutConstraint.activate([
            labourServiceTableView.topAnchor.constraint(equalTo: view2.topAnchor),
            labourServiceTableView.trailingAnchor.constraint(equalTo: view2.trailingAnchor),
            labourServiceTableView.leadingAnchor.constraint(equalTo: view2.leadingAnchor)
        ])
        //view 2
        
        productServiceTableView = UITableView(frame: CGRect(x: 0, y: 0, width: view1.frame.width, height: view2.frame.height))
        productServiceTableView.tag = 1
        productServiceTableView.backgroundColor = .blue
        productServiceTableView.register(UINib(nibName: "LabourTableViewCell", bundle: nil), forCellReuseIdentifier: "LabourTableViewCell")
        view4.addSubview(productServiceTableView)
        productServiceTableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView2Height = productServiceTableView.heightAnchor.constraint(equalToConstant: productServiceTableView.frame.height)
        tableView2Height.isActive = true
        NSLayoutConstraint.activate([
            productServiceTableView.topAnchor.constraint(equalTo: view4.topAnchor),
            productServiceTableView.trailingAnchor.constraint(equalTo: view4.trailingAnchor),
            productServiceTableView.leadingAnchor.constraint(equalTo: view4.leadingAnchor)
        ])
    }
    
    func createView() {
        view1 = UIView(frame: CGRect(x: 0, y: 0, width: contentView.frame.width - 70, height: 66))
        view2 = UIView(frame: CGRect(x: 0, y: 0, width: contentView.frame.width - 70 - 40, height: 0.5))
        view3 = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width - 30, height: 66))
        view4 = UIView(frame: CGRect(x: 0, y: 0, width: contentView.frame.width - 70 - 40, height: 0.5))
        
        let view5 = UIView(frame: CGRect(x: 0, y: 0, width: contentView.frame.width - 70 - 40, height: 0.5))
        
        view1.translatesAutoresizingMaskIntoConstraints = false
        view2.translatesAutoresizingMaskIntoConstraints = false
        view3.translatesAutoresizingMaskIntoConstraints = false
        view4.translatesAutoresizingMaskIntoConstraints = false
        view5.translatesAutoresizingMaskIntoConstraints = false
        
        view1.layer.borderWidth = 0.5
        view1.layer.borderColor = UIColor.darkGray.cgColor
        view1.layer.cornerRadius = 10.0
        view1.clipsToBounds = true
        
        view3.layer.cornerRadius = 10.0
        view3.layer.borderWidth = 0.5
        view3.layer.borderColor = UIColor.darkGray.cgColor
        view3.clipsToBounds = true
        
        view2.backgroundColor = .systemPink
        view4.backgroundColor = .systemTeal
        
        self.contentView.addSubview(view5)
        self.contentView.addSubview(view1)
        self.contentView.addSubview(view2)
        self.contentView.addSubview(view3)
        self.contentView.addSubview(view4)
        
        NSLayoutConstraint.activate([
            view1.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            view1.heightAnchor.constraint(equalToConstant: view1.frame.height),
            view1.widthAnchor.constraint(equalToConstant: CGFloat(self.contentView.frame.width - 70)),
            view1.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])

        self.view2HeightAnchor = view2.heightAnchor.constraint(equalToConstant: 0)
        view2HeightAnchor.isActive = true
        NSLayoutConstraint.activate([
            view2.topAnchor.constraint(equalTo: view1.bottomAnchor, constant: 5),
            view2.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            view2.widthAnchor.constraint(equalToConstant: CGFloat(self.contentView.frame.width - 80))
        ])
        
        NSLayoutConstraint.activate([
            view3.topAnchor.constraint(equalTo: view2.bottomAnchor, constant: 8),
            view3.heightAnchor.constraint(equalToConstant: view3.frame.height),
            view3.widthAnchor.constraint(equalToConstant: CGFloat(self.contentView.frame.width - 70)),
            view3.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
        view4HeightAnchor = view4.heightAnchor.constraint(equalToConstant: 0)
        view4HeightAnchor.isActive = true
        NSLayoutConstraint.activate([
            view4.topAnchor.constraint(equalTo: view3.bottomAnchor, constant: 5),
            view4.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            view4.widthAnchor.constraint(equalToConstant: CGFloat(self.contentView.frame.width - 80))
        ])
        
        NSLayoutConstraint.activate([
            view5.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 20),
            view5.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            view5.heightAnchor.constraint(equalToConstant: 0.5),
            view5.widthAnchor.constraint(equalToConstant: CGFloat(self.contentView.frame.width - 80))
        ])
        let labourLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view1.frame.width - 60, height: 22))
        labourLabel.text = "Labour Services"
        labourLabel.textColor = UIColor.black
        labourLabel.font = labourLabel.font.withSize(14.8)
        labourLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let description1 = UILabel(frame: CGRect(x: 0, y: 0, width: view1.frame.width - 20, height: 40))
        description1.numberOfLines = 0
        description1.text = "We will come to your place and help you!\nEx: House cleaning, Electronics repair, ..."
        description1.textColor = UIColor.darkGray
        description1.font = labourLabel.font.withSize(11.6)
        description1.translatesAutoresizingMaskIntoConstraints = false
        
        let labourImage = UIImage(systemName: "person.2")
        labourImage?.withTintColor(.darkGray)
        labourImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        labourImgView.contentMode = .scaleAspectFit
        labourImgView.image = labourImage
        labourImgView.tintColor = .darkGray
        labourImgView.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(labourImgView)
        view1.addSubview(labourLabel)
        view1.addSubview(description1)
        
        NSLayoutConstraint.activate([
            labourLabel.topAnchor.constraint(equalTo: view1.topAnchor, constant: 4),
            labourLabel.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 12),
            labourLabel.heightAnchor.constraint(equalToConstant: labourLabel.frame.height),
            labourLabel.widthAnchor.constraint(equalToConstant: labourLabel.frame.width)
        ])
        
        NSLayoutConstraint.activate([
            labourImgView.centerYAnchor.constraint(equalTo: view1.centerYAnchor),
            labourImgView.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: labourLabel.frame.width),
            labourImgView.heightAnchor.constraint(equalToConstant: labourImgView.frame.height),
            labourImgView.widthAnchor.constraint(equalToConstant: labourImgView.frame.width)
        ])
        
        NSLayoutConstraint.activate([
            description1.topAnchor.constraint(equalTo: labourLabel.bottomAnchor, constant: -5),
            description1.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 12),
            description1.heightAnchor.constraint(equalToConstant: description1.frame.height),
            description1.widthAnchor.constraint(equalToConstant: description1.frame.width)
        ])
        
        let productLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view1.frame.width - 60, height: 22))
        productLabel.text = "Product-based Services"
        productLabel.textColor = UIColor.black
        productLabel.font = labourLabel.font.withSize(14.8)
        productLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let description2 = UILabel(frame: CGRect(x: 0, y: 0, width: view1.frame.width - 75, height: 40))
        description2.numberOfLines = 0
        description2.text = "We will create and deliver products to you!\nEx: Graphic Designing, Contents creating,..."
        description2.textColor = UIColor.darkGray
        description2.font = labourLabel.font.withSize(11.6)
        description2.translatesAutoresizingMaskIntoConstraints = false
        
        let boxImg = UIImage(systemName: "cube.box")
        boxImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        boxImgView.contentMode = .scaleAspectFit
        boxImgView.image = boxImg
        boxImgView.tintColor = .darkGray
        boxImgView.translatesAutoresizingMaskIntoConstraints = false
        
        view3.addSubview(boxImgView)
        view3.addSubview(productLabel)
        view3.addSubview(description2)
        
        NSLayoutConstraint.activate([
            boxImgView.centerYAnchor.constraint(equalTo: view3.centerYAnchor),
            boxImgView.leadingAnchor.constraint(equalTo: view3.leadingAnchor, constant: productLabel.frame.width),
            boxImgView.heightAnchor.constraint(equalToConstant: boxImgView.frame.height),
            boxImgView.widthAnchor.constraint(equalToConstant: boxImgView.frame.width)
        ])
        
        NSLayoutConstraint.activate([
            productLabel.topAnchor.constraint(equalTo: view3.topAnchor, constant: 4),
            productLabel.leadingAnchor.constraint(equalTo: view3.leadingAnchor, constant: 12),
            productLabel.heightAnchor.constraint(equalToConstant: productLabel.frame.height),
            productLabel.widthAnchor.constraint(equalToConstant: productLabel.frame.width)
        ])
        
        NSLayoutConstraint.activate([
            description2.topAnchor.constraint(equalTo: productLabel.bottomAnchor, constant: -5),
            description2.leadingAnchor.constraint(equalTo: view3.leadingAnchor, constant: 12),
            description2.heightAnchor.constraint(equalToConstant: description2.frame.height),
            description2.widthAnchor.constraint(equalToConstant: description2.frame.width)
        ])
    }
    
    func addDelegateAndDataSource(delegate: UITableViewDelegate, dataSource: UITableViewDataSource, scrollViewDelegate: UIScrollViewDelegate) {
        self.labourServiceTableView.delegate = delegate
        self.labourServiceTableView.dataSource = dataSource
        self.productServiceTableView.delegate = delegate
        self.productServiceTableView.dataSource = dataSource
        self.scrollView.delegate = scrollViewDelegate
        productServiceTableView.reloadData()
        labourServiceTableView.reloadData()
    }
}
