//
//  CalendarVIew.swift
//  final-project
//
//  Created by phuong nguyen on 6/13/21.
//

import UIKit
import FSCalendar

protocol BookingCalendarDelegate : class {
    func selectedDate(Value : Date)
}

class CalendarView: UIView {

    @IBOutlet weak var cal: FSCalendar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configCalendar()
    }
    
    func setDelegateAndDataSourceForCalendar(delegate: FSCalendarDelegate, dataSource: FSCalendarDataSource) {
        cal.delegate = delegate
        cal.dataSource = dataSource
        cal.reloadData()
        
    }
    
    func configCalendar() {
        cal.appearance.todayColor = nil
        cal.appearance.titleTodayColor = cal.appearance.titleDefaultColor
        cal.appearance.headerMinimumDissolvedAlpha = 0
        cal.appearance.selectionColor = .orange
        cal.appearance.headerTitleColor = .darkGray
        cal.appearance.weekdayTextColor = UIColor(red: 230/255, green: 174/255, blue: 70/255, alpha: 1.0)
    }
}
