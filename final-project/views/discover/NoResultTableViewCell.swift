//
//  NoResultTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/8/21.
//

import UIKit

class NoResultTableViewCell: TableViewCell {
    
    @IBOutlet weak var noResultLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        noResultLabel.text = "Please Enter a keyword to discover"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        noResultLabel.text = "Search results"
    }
    
}
