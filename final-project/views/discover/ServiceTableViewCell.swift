//
//  ServiceTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/5/21.
//

import UIKit

class ServiceTableViewCell: TableViewCell {
    
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var itemContainerView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAvaImgView: UIImageView!
    @IBOutlet weak var serviceMinPrice: UILabel!
    @IBOutlet weak var avaImage: UIImageView!
    
    let bannerView2 = UIView()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bannerView2.removeFromSuperview()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemContainerView.layer.cornerRadius = 20.0
        itemContainerView.layer.shadowColor = UIColor.lightGray.cgColor
        itemContainerView.layer.shadowOpacity = 0.6
        itemContainerView.layer.shadowOffset = .zero
        itemContainerView.layer.shadowRadius = 5
        userAvaImgView.makeRounded()
        avaImage.layer.cornerRadius = 20.0
        avaImage.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        avaImage.layer.masksToBounds = true
        
    }
    
    func displayBanner () {
        bannerView2.backgroundColor = .white
        bannerView2.translatesAutoresizingMaskIntoConstraints = false
        bannerView2.frame = CGRect(x: 50, y: 50, width: 100, height: 20)
        itemContainerView.addSubview(bannerView2)
        bannerView2.makeViewRounded()

        NSLayoutConstraint.activate([
            bannerView2.leadingAnchor.constraint(equalTo: itemContainerView.leadingAnchor, constant: 20),
            bannerView2.widthAnchor.constraint(equalToConstant: 100),
            bannerView2.topAnchor.constraint(equalTo: itemContainerView.topAnchor),
            bannerView2.heightAnchor.constraint(equalToConstant: 20),
        ])
        
        //Auto layout banner Image
        let bannerImage = UIImage(systemName: "checkmark.seal.fill")!
        let bannerImageView = UIImageView(image: bannerImage)
        bannerImageView.translatesAutoresizingMaskIntoConstraints = false
        bannerImageView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        bannerImageView.tintColor = GlobalDesign.backgroundColor
        bannerView2.addSubview(bannerImageView)
        
        NSLayoutConstraint.activate([
            bannerImageView.trailingAnchor.constraint(equalTo: bannerView2.trailingAnchor),
            bannerImageView.widthAnchor.constraint(equalToConstant: 20),
            bannerImageView.topAnchor.constraint(equalTo: bannerView2.topAnchor),
            bannerImageView.heightAnchor.constraint(equalToConstant: 20),
        ])
        
        // Auto layout label
        let bannerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 75, height: 20))
        bannerLabel.text = "Recommended"
        bannerLabel.font = UIFont.systemFont(ofSize: 10.4)
        bannerLabel.textColor = UIColor(red: 0.84, green: 0.4, blue: 0.49, alpha: 1.0)
        bannerLabel.translatesAutoresizingMaskIntoConstraints = false
        bannerView2.addSubview(bannerLabel)
        NSLayoutConstraint.activate([
            bannerLabel.trailingAnchor.constraint(equalTo: bannerImageView.leadingAnchor, constant: -1),
            bannerLabel.widthAnchor.constraint(equalToConstant: 75),
            bannerLabel.topAnchor.constraint(equalTo: bannerView2.topAnchor),
            bannerLabel.heightAnchor.constraint(equalToConstant: 20),
        ])
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
