//
//  ProviderTableViewCell.swift
//  final-project
//
//  Created by phuong nguyen on 6/5/21.
//

import UIKit

class ProviderTableViewCell: TableViewCell {

    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var avaImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        avaImage.makeRounded()
        containerView.layer.cornerRadius = 20.0
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 0.6
        containerView.layer.shadowOffset = .zero
        containerView.layer.shadowRadius = 5
//        containerView.layer.shouldRasterize = true
        avaImage.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
