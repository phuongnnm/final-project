//
//  UITabBarExtension.swift
//  final-project
//
//  Created by phuong nguyen on 5/5/21.
//

import Foundation
import UIKit

extension UITabBarController {
    func setUpImageOntabbar(_ selectedImage : [UIImage], _ image : [UIImage], _ title : [String]?){
        for (index,_) in image.enumerated(){
            if let tab = self.tabBar.items?[index]{
                tab.image = image[index]
                tab.image = selectedImage[index]
                if (title?[index]) != nil{
                    tab.title = title![index]
                }
            }
        }
    }
}

class GlobalDesign {
    static let backgroundColor = UIColor(red: 0.99, green: 0.80, blue: 0.43, alpha: 1.00)
}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}



